package net.styleru.i_komarov.medforum.view.main.topic;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 10.12.16.
 */

public class CommentViewEntity implements Parcelable {

    private String id;
    private String content;
    private String time;
    private String authorImage;
    private String authorFirstName;
    private String authorLastName;
    private Boolean isExpert;

    public CommentViewEntity(Parcel in) {
        this.authorFirstName = in.readString();
        this.authorImage = in.readString();
        this.authorLastName = in.readString();
        this.content = in.readString();
        this.id = in.readString();
        this.isExpert = in.readByte() == (byte) 0;
        this.time = in.readString();
    }

    public CommentViewEntity(String id, String content, String time, String authorImage, String authorFirstName, String authorLastName, Boolean isExpert) {
        this.id = id;
        this.content = content;
        this.time = time;
        this.authorImage = authorImage;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.isExpert = isExpert;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public Boolean getExpert() {
        return isExpert;
    }

    public void setExpert(Boolean expert) {
        isExpert = expert;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authorFirstName);
        dest.writeString(authorImage);
        dest.writeString(authorLastName);
        dest.writeString(content);
        dest.writeString(id);
        dest.writeByte(isExpert ? (byte)1 : (byte)0);
        dest.writeString(time);
    }

    public static Parcelable.Creator<CommentViewEntity> CREATOR = new Creator<CommentViewEntity>() {
        @Override
        public CommentViewEntity createFromParcel(Parcel in) {
            return new CommentViewEntity(in);
        }

        @Override
        public CommentViewEntity[] newArray(int size) {
            return new CommentViewEntity[0];
        }
    };
}
