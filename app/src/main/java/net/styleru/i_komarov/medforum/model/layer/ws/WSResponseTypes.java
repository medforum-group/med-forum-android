package net.styleru.i_komarov.medforum.model.layer.ws;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSResponseTypes {

    public static final String TYPE_CHANGE = "change";
    public static final String TYPE_REQUEST = "request";
    public static final String TYPE_RESPONSE = "response";
    public static final String TYPE_SIGNAL = "signal";
}
