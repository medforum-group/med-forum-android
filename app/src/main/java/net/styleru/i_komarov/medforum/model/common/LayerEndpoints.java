package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerEndpoints {

    public static final String ENDPOINT_NONCE = "nonces";
    public static final String ENDPOINT_SESSIONS = "sessions";
    public static final String ENDPOINT_CONVERSATIONS = "conversations";
    public static final String ENDPOINT_MESSAGES = "messages";
}
