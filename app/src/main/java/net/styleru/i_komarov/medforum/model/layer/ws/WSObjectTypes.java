package net.styleru.i_komarov.medforum.model.layer.ws;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSObjectTypes {

    public static final String TYPE_CONVERSATION = "Conversation";
    public static final String TYPE_MESSAGE = "Message";
}
