package net.styleru.i_komarov.medforum.view.main.categories;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.model.datetime.DateUtils;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

/**
 * Created by i_komarov on 09.12.16.
 */

public class CategoryViewHolder extends BindableViewHolder<CategoryViewEntity, BindableViewHolder.ActionListener<CategoryViewEntity>> {

    private String topicsCountFormatString;
    private String lastUpdateFormatString;
    private String dateTimeFormat;

    private AppCompatTextView titleHolder;
    private AppCompatTextView topicsCountHolder;
    private AppCompatTextView lastUpdateHolder;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_title);
        topicsCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_themes);
        lastUpdateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_category_last_update);
        lastUpdateFormatString = itemView.getResources().getString(R.string.format_last_update);
        dateTimeFormat = itemView.getResources().getString(R.string.format_date_time);
    }

    @Override
    public void bind(int position, CategoryViewEntity item, ActionListener<CategoryViewEntity> listener) {
        super.bind(position, item, listener);

        topicsCountFormatString = itemView.getResources().getQuantityString(R.plurals.format_topics_count, item.getTopicsCount(), item.getTopicsCount());

        titleHolder.setText(item.getTitle());
        topicsCountHolder.setText(topicsCountFormatString);
        try {
            DateUtils.DisplayDate data = DateUtils.DisplayDate.fromServerDateTime(item.getLastUpdate());
            lastUpdateHolder.setText(String.format(dateTimeFormat, data.getDisplayDate(), data.getDisplayTime()));
        } catch(Exception e) {

        }
    }
}
