package net.styleru.i_komarov.medforum.presenter.authorization.verification;

import android.content.Intent;
import android.util.Log;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.UsersProxy;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.view.authorization.main.AuthorizationActivity;
import net.styleru.i_komarov.medforum.view.authorization.verification.IVerificationView;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by i_komarov on 19.12.16.
 */

public class VerificationPresenter extends Presenter<IVerificationView> {

    private AtomicLong timersCount = new AtomicLong(0L);

    private ConnectableObservable<Long> timer = createResendButtonDisableStateTimer();

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IVerificationView view) {
        super.bindView(view);
        subscribeToWrongPhoneNumberButtonClickObservable(view.getWrongPhoneNumberButtonClickObservable());
        subscribeToVerifyCodeButtonClickObservable(view.getVerifyCodeButtonClickObservable());
        subscribeToResendCodeButtonClickObservable(view.getResendCodeButtonClickObservable());
        timer.observeOn(AndroidSchedulers.mainThread()).subscribe(createResendButtonDisableStateTimerObserver());
        timer.connect();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    private void subscribeToWrongPhoneNumberButtonClickObservable(Observable<Integer> wrongPhoneNumberButtonClickObservable) {
        wrongPhoneNumberButtonClickObservable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createWrongPhoneNumberButtonClickObserver());
    }

    private void subscribeToVerifyCodeButtonClickObservable(Observable<PhoneAndCode> verifyCodeButtonClickObservable) {
        verifyCodeButtonClickObservable
                .doOnNext(value -> {
                    timer.subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(createResendButtonDisableStateTimerObserver());
                    timer.connect();
                })
                .flatMap(new Function<PhoneAndCode, ObservableSource<Response<UserServerEntity>>>() {
                    @Override
                    public ObservableSource<Response<UserServerEntity>> apply(PhoneAndCode data) throws Exception {
                        return UsersProxy.getInstance().auth(data.getPhone(), data.getCode())
                                .subscribeOn(Schedulers.io())
                                .retry(3);
                    }
                })
                .filter(response -> hookHttpCode(response.code()))
                .filter(response -> response != null && response.body() != null)
                .map(Response::body)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createVerifyCodeButtonClickObserver());
    }

    private void subscribeToResendCodeButtonClickObservable(Observable<String> resendCodeButtonClickObservable) {
        resendCodeButtonClickObservable
                .doOnNext(value -> {
                    timer.subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(createResendButtonDisableStateTimerObserver());
                    timer.connect();
                })
                .flatMap(new Function<String, ObservableSource<Response<Object>>>() {
                    @Override
                    public ObservableSource<Response<Object>> apply(String phone) throws Exception {
                        return UsersProxy.getInstance().code(phone)
                                .subscribeOn(Schedulers.io())
                                .retry(3);
                    }
                })
                .filter(response -> hookHttpCode(response.code()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createResendCodeButtonClickObserver());
    }

    private DisposableObserver<Integer> createWrongPhoneNumberButtonClickObserver() {
        DisposableObserver<Integer> wrongPhoneNumberButtonClickObserver = new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                view.onUserChangePhoneAction();
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(wrongPhoneNumberButtonClickObserver);

        return wrongPhoneNumberButtonClickObserver;
    }

    private DisposableObserver<UserServerEntity> createVerifyCodeButtonClickObserver() {
        DisposableObserver<UserServerEntity> verifyCodeButtonClickObserver = new DisposableObserver<UserServerEntity>() {
            @Override
            public void onNext(UserServerEntity user) {
                Intent data = new Intent();
                data.putExtra(AuthorizationActivity.INTENT_KEY_AUTHORIZATION_RESULT, user);

                view.onCodeVerificationSucceeded(data);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(verifyCodeButtonClickObserver);

        return verifyCodeButtonClickObserver;
    }

    private DisposableObserver<Response<Object>> createResendCodeButtonClickObserver() {
        DisposableObserver<Response<Object>> resendCodeButtonClickObserver = new DisposableObserver<Response<Object>>() {
            @Override
            public void onNext(Response<Object> value) {
                //Something might be needed here maybe ?
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(resendCodeButtonClickObserver);

        return resendCodeButtonClickObserver;
    }

    private DisposableObserver<Long> createResendButtonDisableStateTimerObserver() {
        DisposableObserver<Long> resendButtonDisableStateTimeObserver = new DisposableObserver<Long>() {

            private Long selfID = timersCount.incrementAndGet();

            @Override
            public void onNext(Long delay) {
                if(selfID != timersCount.get()) {
                    if(!isDisposed()) {
                        dispose();
                    }
                } else {
                    try {
                        view.updateResendButtonDisabledDelay(delay);
                    } catch (NullPointerException e) {
                        if (!isDisposed()) {
                            dispose();
                        }
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(resendButtonDisableStateTimeObserver);

        return resendButtonDisableStateTimeObserver;
    }

    private ConnectableObservable<Long> createResendButtonDisableStateTimer() {
        return Observable.intervalRange(0, 11, 0, 1, TimeUnit.SECONDS)
                .map(ticks -> 10 - ticks)
                .replay();
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode != 200) {
            Log.d("VerificationPresenter", "hookHttpCode: " + httpCode);
            view.onCodeVerificationFailed();
            //TODO: add view.showHttpError(httpCode);
            return false;
        } else {
            return true;
        }
    }

    public static final class PhoneAndCode {

        private String phone;
        private String code;

        public PhoneAndCode(String phone, String code) {
            this.phone = phone;
            this.code = code;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }
}
