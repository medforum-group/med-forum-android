package net.styleru.i_komarov.medforum.view.main.participants;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 29.12.16.
 */

public class ParticipantViewEntity implements Parcelable {

    private String id;
    private String avatarUrl;
    private String firstName;
    private String lastName;

    public ParticipantViewEntity(Parcel in)  {
        this.id = in.readString();
        this.avatarUrl = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
    }

    public ParticipantViewEntity(String id, String avatarUrl, String firstName, String lastName) {
        this.id = id;
        this.avatarUrl = avatarUrl;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(avatarUrl);
        dest.writeString(firstName);
        dest.writeString(lastName);
    }

    public static final Parcelable.Creator<ParticipantViewEntity> CREATOR = new Creator<ParticipantViewEntity>() {
        @Override
        public ParticipantViewEntity createFromParcel(Parcel in) {
            return new ParticipantViewEntity(in);
        }

        @Override
        public ParticipantViewEntity[] newArray(int size) {
            return new ParticipantViewEntity[0];
        }
    };
}
