package net.styleru.i_komarov.medforum.view.main.conversation;

import java.util.List;

/**
 * Created by i_komarov on 22.01.17.
 */

public interface IMessagesManager {

    void clear();

    void add(MessageViewEntity item);

    void add(List<MessageViewEntity> items);

    void addTop(MessageViewEntity item);

    void addTop(List<MessageViewEntity> items);

    String lastUri();
}
