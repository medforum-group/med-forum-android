package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;
import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerMessagePart {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_MIME_TYPE)
    private String mimeType;
    @SerializedName(FIELD_BODY)
    private String body;
    @SerializedName(FIELD_ENCODING)
    private String encoding;
    @SerializedName(FIELD_CONTENT)
    private LayerRichContentEntity content;

    public LayerMessagePart(String id, String mimeType, String body, String encoding, LayerRichContentEntity content) {
        this.id = id;
        this.mimeType = mimeType;
        this.body = body;
        this.encoding = encoding;
        this.content = content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public LayerRichContentEntity getContent() {
        return content;
    }

    public void setContent(LayerRichContentEntity content) {
        this.content = content;
    }
}
