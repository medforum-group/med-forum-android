package net.styleru.i_komarov.medforum.view.authorization.verification;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.authorization.verification.VerificationPresenter;
import net.styleru.i_komarov.medforum.view.authorization.main.AuthorizationActivity;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.12.16.
 */

public class VerificationFragment extends RetainPresenterFragment<IVerificationView, VerificationPresenter> implements IVerificationView {

    private static String KEY_STATE_PHONE = VerificationFragment.class.getCanonicalName() + ".STATE_PHONE";

    private AppCompatTextView wrongPhoneButton;
    private AppCompatTextView codeSentTitleHolder;
    private TextInputEditText codeInputHolder;
    private AppCompatButton applyCodeButton;
    private AppCompatTextView resendCodeButton;
    private AppCompatTextView resendCodeButtonDelayLabel;

    private String phone;

    private String resendButtonDisabledDelayLabelFormat;
    private String resendButtonEnabledLabel;

    public static Fragment newInstance(String phone) {
        Bundle args = new Bundle();
        args.putString(KEY_STATE_PHONE, phone);
        Fragment fragment = new VerificationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(KEY_STATE_PHONE)) {
            phone = savedInstanceState.getString(KEY_STATE_PHONE);
        } else if(getArguments() != null && getArguments().containsKey(KEY_STATE_PHONE)) {
            phone = getArguments().getString(KEY_STATE_PHONE);
        } else {
            throw new RuntimeException("Phone was not found neither in savedInstanceState nor in arguments!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_authorization_verification, parent, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        codeSentTitleHolder.setText(String.format(getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_verification_format_input_phone), phone));
        codeInputHolder.clearFocus();

        resendButtonDisabledDelayLabelFormat = getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_verification_resend_code_enabled_after_format);
        resendButtonEnabledLabel = getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_verification_resend_code_enabled);
    }

    @Override
    public Observable<Integer> getWrongPhoneNumberButtonClickObservable() {
        return RxView.clicks(wrongPhoneButton);
    }

    @Override
    public Observable<VerificationPresenter.PhoneAndCode> getVerifyCodeButtonClickObservable() {
        return RxView.clicks(applyCodeButton)
                .doOnNext(value -> {
                    codeInputHolder.setEnabled(false);
                    codeInputHolder.clearFocus();
                    resendCodeButton.setEnabled(false);
                })
                .map(value -> new VerificationPresenter.PhoneAndCode(phone, codeInputHolder.getText().toString()));
    }

    @Override
    public Observable<String> getResendCodeButtonClickObservable() {
        return RxView.clicks(resendCodeButton)
                .doOnNext(value -> {
                    resendCodeButton.setEnabled(false);
                    codeInputHolder.setText("");
                    codeInputHolder.setEnabled(true);
                    codeInputHolder.clearFocus();
                })
                .map(value -> phone);
    }

    @Override
    public void updateResendButtonDisabledDelay(long secondsLast) {
        resendCodeButtonDelayLabel.setText(secondsLast == 0 ? resendButtonEnabledLabel : String.format(resendButtonDisabledDelayLabelFormat, String.valueOf(secondsLast)));
        if(secondsLast == 0) {
            resendCodeButton.setEnabled(true);
        }
    }

    @Override
    public void onCodeVerificationSucceeded(Intent data) {
        ((AuthorizationActivity) getActivity()).onAuthorizationSuccess(data);
    }

    @Override
    public void onCodeVerificationFailed() {
        codeInputHolder.setError(getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_verification_error_code_verification_failed));
        codeInputHolder.setText("");
        codeInputHolder.setEnabled(true);
    }

    @Override
    public void onUserChangePhoneAction() {
        ((AuthorizationActivity) getActivity()).onUserChangePhoneAction();
    }

    @Override
    protected void bindViewComponents(View view) {
        wrongPhoneButton = (AppCompatTextView) view.findViewById(R.id.fragment_authorization_verification_wrong_phone);
        codeSentTitleHolder = (AppCompatTextView) view.findViewById(R.id.fragment_authorization_verification_enter_phone_label);
        codeInputHolder = (TextInputEditText) view.findViewById(R.id.fragment_authorization_verification_enter_code_field);
        applyCodeButton = (AppCompatButton) view.findViewById(R.id.fragment_authorization_verification_continue_button);
        resendCodeButton = (AppCompatTextView) view.findViewById(R.id.fragment_authorization_verification_resend_code_button);
        resendCodeButtonDelayLabel = (AppCompatTextView) view.findViewById(R.id.fragment_authorization_verification_resend_code_time_limit_label);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_STATE_PHONE, phone);
    }

    @Override
    protected PresenterLoader.Info<IVerificationView> getLoaderInfo() {
        return new PresenterLoader.Info<IVerificationView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_AUTHORIZATION_ENTRY;
            }

            @Override
            public Presenter.Factory<IVerificationView> getFactory() {
                return new Presenter.Factory<IVerificationView>() {
                    @Override
                    public <P extends Presenter<IVerificationView>> P create() {
                        return (P) new VerificationPresenter();
                    }
                };
            }

            @Override
            public IVerificationView getView() {
                return VerificationFragment.this;
            }
        };
    }
}
