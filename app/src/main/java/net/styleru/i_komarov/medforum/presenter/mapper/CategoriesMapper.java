package net.styleru.i_komarov.medforum.presenter.mapper;

import net.styleru.i_komarov.medforum.model.api.dto.CategoryServerEntity;
import net.styleru.i_komarov.medforum.view.main.categories.CategoryViewEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 03.01.17.
 */

public class CategoriesMapper implements Function<List<CategoryServerEntity>, List<CategoryViewEntity>> {
    @Override
    public List<CategoryViewEntity> apply(List<CategoryServerEntity> categoryServerEntityList) throws Exception {
        return Flowable.fromIterable(categoryServerEntityList)
                .map(category -> new CategoryViewEntity(category.getId(), category.getTitle(), category.getCount(), category.getLastUpdate()))
                .toList()
                .blockingGet();
    }
}
