package net.styleru.i_komarov.medforum.view.participants_picker;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.activity.PresenterActivity;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.participants_picker.ParticipantsPickerPresenter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewHolder;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;
import net.styleru.i_komarov.rxjava2_binding.search_view.main.RxSearchView;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.01.17.
 */

public class ParticipantsPickerActivity extends PresenterActivity<IParticipantsPickerView, ParticipantsPickerPresenter> implements IParticipantsPickerView {

    public static final String RESULT_PARTICIPANT_ID = ParticipantsPickerActivity.class.getCanonicalName() + ".PARTICIPANT_ID";

    private static final String KEY_ADAPTER_STATE = ParticipantsPickerActivity.class.getCanonicalName() + ".STATE_ADAPTER";
    private static final String KEY_LIST_STATE = ParticipantsPickerActivity.class.getCanonicalName() + ".STATE_LIST";

    private Parcelable listState;

    private RecyclerView list;
    private BindableAdapter<ParticipantViewEntity> adapter;
    private SearchView searchView;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_participants_picker);

        list = (RecyclerView) findViewById(R.id.activity_participants_picker_recycler_view);
        searchView = (SearchView) findViewById(R.id.activity_participants_picker_search_view);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.activity_participants_picker_swipe_refresh);

        swipeRefresh.setEnabled(false);

        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey(KEY_ADAPTER_STATE)) {
                adapter = savedInstanceState.getParcelable(KEY_ADAPTER_STATE);
            }
            if(savedInstanceState.containsKey(KEY_LIST_STATE)) {
                listState = savedInstanceState.getParcelable(KEY_LIST_STATE);
            }
        }

        if(adapter == null) {
            adapter = new BindableAdapter<>(R.layout.list_item_participant, ParticipantViewHolder.class);
        }

        adapter.setActionListener(new BindableViewHolder.ActionListener<ParticipantViewEntity>() {
            @Override
            public void clicked(int position, ParticipantViewEntity item) {
                adapter.setActionListener(null);
                Intent intent = new Intent();
                intent.putExtra(RESULT_PARTICIPANT_ID, item.getId());
                setResult(RESULT_OK, intent);
                adapter.onStop();
                finish();
            }

            @Override
            public void longClicked(int position, ParticipantViewEntity item) {

            }

            @Override
            public void swiped(int position, int direction) {

            }
        });

        list.setLayoutManager(new LinearLayoutManager(this));
        list.setAdapter(adapter);
    }

    @Override
    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable() {
        return RxSearchView.queryChanges(searchView);
    }

    @Override
    public void refreshAdapter() {
        adapter.refreshDataset();
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.onStart();
        adapter.subscribeToSourceChannel(presenter.provideDataChannel(adapter.getStateObservable(list)));
        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
            listState = null;
        }
    }

    @Override
    public void onPause() {
        if(list != null) {
            listState = list.getLayoutManager().onSaveInstanceState();
        }
        adapter.onStop();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(adapter != null) {
            outState.putParcelable(KEY_ADAPTER_STATE, adapter);
        }
        if(listState != null) {
            outState.putParcelable(KEY_LIST_STATE, listState);
        }
    }

    @Override
    protected PresenterLoader.Info<IParticipantsPickerView> getLoaderInfo() {
        return new PresenterLoader.Info<IParticipantsPickerView>() {
            @Override
            public int getID() {
                return LOADER_PARTICIPANTS_PICKER;
            }

            @Override
            public Presenter.Factory<IParticipantsPickerView> getFactory() {
                return new Presenter.Factory<IParticipantsPickerView>() {
                    @Override
                    public <P extends Presenter<IParticipantsPickerView>> P create() {
                        return (P) new ParticipantsPickerPresenter();
                    }
                };
            }

            @Override
            public IParticipantsPickerView getView() {
                return ParticipantsPickerActivity.this;
            }
        };
    }
}
