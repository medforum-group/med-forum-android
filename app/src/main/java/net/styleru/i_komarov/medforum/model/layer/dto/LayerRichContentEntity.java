package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerRichContentEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_DOWNLOAD_URL)
    private String downloadUrl;
    @SerializedName(FIELD_EXPIRATION)
    private String expiration;
    @SerializedName(FIELD_REFRESH_URl)
    private String refreshUrl;
    @SerializedName(FIELD_SIZE)
    private Long size;

    public LayerRichContentEntity(String id, String downloadUrl, String expiration, String refreshUrl, Long size) {
        this.id = id;
        this.downloadUrl = downloadUrl;
        this.expiration = expiration;
        this.refreshUrl = refreshUrl;
        this.size = size;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getRefreshUrl() {
        return refreshUrl;
    }

    public void setRefreshUrl(String refreshUrl) {
        this.refreshUrl = refreshUrl;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
