package net.styleru.i_komarov.medforum.model.database.entities.questions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.i_komarov.medforum.model.database.mapping.questions.QuestionsTableConfig;

/**
 * Created by i_komarov on 14.01.17.
 */

@StorIOSQLiteType(table = QuestionsTableConfig.TABLE)
public class QuestionDatabaseEntity {

    @Nullable
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_SERVER_ID)
    String serverID;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_USER_ID)
    String userID;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_FIRST_NAME)
    String firstName;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_LAST_NAME)
    String lastName;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_IMAGE_URL)
    String imageUrl;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_ROLE)
    String role;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_CATEGORY_ID)
    String categoryID;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_TITLE)
    String title;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_CONTENT)
    String content;

    @NonNull
    @StorIOSQLiteColumn(name = QuestionsTableConfig.COLUMN_LAST_UPDATE)
    String lastUpdate;

    QuestionDatabaseEntity() {

    }

    @NonNull
    public static QuestionDatabaseEntity newInstance(String serverID, String userID, String firstName, String lastName, String imageUrl, String role,
                                                     String categoryID, String title, String content, String lastUpdate) {
        QuestionDatabaseEntity entity = new QuestionDatabaseEntity();
        entity.serverID = serverID;
        entity.userID = userID;
        entity.firstName = firstName;
        entity.lastName = lastName;
        entity.imageUrl = imageUrl;
        entity.role = role;
        entity.categoryID = categoryID;
        entity.title = title;
        entity.content = content;
        entity.lastUpdate = lastUpdate;

        return entity;
    }

    @NonNull
    public static QuestionDatabaseEntity newInstance(Long id, String serverID, String userID, String firstName, String lastName, String imageUrl, String role,
                                                     String categoryID, String title, String content, String lastUpdate) {
        QuestionDatabaseEntity entity = new QuestionDatabaseEntity();
        entity.id = id;
        entity.serverID = serverID;
        entity.userID = userID;
        entity.firstName = firstName;
        entity.lastName = lastName;
        entity.imageUrl = imageUrl;
        entity.role = role;
        entity.categoryID = categoryID;
        entity.title = title;
        entity.content = content;
        entity.lastUpdate = lastUpdate;

        return entity;
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @NonNull
    public String getServerID() {
        return serverID;
    }

    public void setServerID(@NonNull String serverID) {
        this.serverID = serverID;
    }

    @NonNull
    public String getUserID() {
        return userID;
    }

    public void setUserID(@NonNull String userID) {
        this.userID = userID;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NonNull String lastName) {
        this.lastName = lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@NonNull String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    public void setRole(@NonNull String role) {
        this.role = role;
    }

    @NonNull
    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(@NonNull String categoryID) {
        this.categoryID = categoryID;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(@NonNull String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionDatabaseEntity that = (QuestionDatabaseEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!serverID.equals(that.serverID)) return false;
        if (!userID.equals(that.userID)) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (!imageUrl.equals(that.imageUrl)) return false;
        if (!role.equals(that.role)) return false;
        if (!categoryID.equals(that.categoryID)) return false;
        if (!title.equals(that.title)) return false;
        if (!content.equals(that.content)) return false;
        return lastUpdate.equals(that.lastUpdate);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + serverID.hashCode();
        result = 31 * result + userID.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + imageUrl.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + categoryID.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + content.hashCode();
        result = 31 * result + lastUpdate.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "QuestionDatabaseEntity{" +
                "id=" + id +
                ", serverID='" + serverID + '\'' +
                ", userID='" + userID + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", role='" + role + '\'' +
                ", categoryID='" + categoryID + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                '}';
    }
}
