package net.styleru.i_komarov.medforum.view.main.categories;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 09.12.16.
 */

public interface ICategoriesView {

    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable();

    public void refreshAdapter();
}
