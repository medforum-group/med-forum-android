package net.styleru.i_komarov.medforum.view.main.topic;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.model.datetime.DateUtils;
import net.styleru.i_komarov.medforum.presenter.main.discussion.DiscussionPresenter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableAdapter;
import net.styleru.i_komarov.medforum.view.main.drawer.DrawerActivity;
import net.styleru.i_komarov.rxjava2_binding.text_input.main.RxEditText;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import java.text.ParseException;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static net.styleru.i_komarov.medforum.view.main.topic.DiscussionFragment.State.*;

/**
 * Created by i_komarov on 10.12.16.
 */

public class DiscussionFragment extends RetainPresenterFragment<IDiscussionView, DiscussionPresenter> implements IDiscussionView {

    private BindableAdapter<CommentViewEntity> adapter;

    private AppCompatTextView answersCountHolder;
    private AppCompatTextView authorNameHolder;
    private AppCompatTextView topicTitleHolder;
    private AppCompatTextView topicContentHolder;

    private AppCompatEditText commentInputHolder;
    private AppCompatImageButton sendCommentButton;

    private CircleImageView authorImageHolder;

    private DiscussionViewEntity headerContent = null;

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView list;

    private String categoryID;
    private String questionID;

    public static DiscussionFragment newInstance(String categoryID, String questionID) {
        Bundle args = new Bundle();
        args.putString(ARGUMENTS_CATEGORY, categoryID);
        args.putString(ARGUMENTS_QUESTION, questionID);
        DiscussionFragment fragment = new DiscussionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(ARGUMENTS_CATEGORY) && savedInstanceState.containsKey(ARGUMENTS_QUESTION)) {
            categoryID = savedInstanceState.getString(ARGUMENTS_CATEGORY);
            questionID = savedInstanceState.getString(ARGUMENTS_QUESTION);
        } else {
            Bundle args = getArguments();
            if(args != null && args.containsKey(ARGUMENTS_CATEGORY) && args.containsKey(ARGUMENTS_QUESTION)) {
                categoryID = args.getString(ARGUMENTS_CATEGORY);
                questionID = args.getString(ARGUMENTS_QUESTION);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topic, parent, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(adapter != null) {
            outState.putParcelable(STATE_ADAPTER, adapter);
        }
        if(list != null && listState != null) {
            outState.putParcelable(STATE_LIST, listState);
        }
        if(headerContent != null) {
            outState.putParcelable(STATE_HEADER, headerContent);
        }
        outState.putString(ARGUMENTS_CATEGORY, categoryID);
        outState.putString(ARGUMENTS_QUESTION, questionID);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_ADAPTER)) {
            adapter = savedInstanceState.getParcelable(STATE_ADAPTER);
        }
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_LIST)) {
            listState = savedInstanceState.getParcelable(STATE_LIST);
        }
        if(adapter == null) {
            adapter = new BindableAdapter<>(R.layout.list_item_comment, CommentViewHolder.class);
        }
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_HEADER)) {
            this.headerContent = savedInstanceState.getParcelable(STATE_HEADER);
        }
        list.setAdapter(adapter);
    }

    @Override
    protected void bindViewComponents(View view) {
        answersCountHolder = (AppCompatTextView) view.findViewById(R.id.list_header_topic_answers);
        authorNameHolder   = (AppCompatTextView) view.findViewById(R.id.list_header_topic_author_name);
        topicTitleHolder   = (AppCompatTextView) view.findViewById(R.id.list_header_topic_title);
        topicContentHolder = (AppCompatTextView) view.findViewById(R.id.list_header_topic_content);

        commentInputHolder = (AppCompatEditText) view.findViewById(R.id.fragment_discussion_content_input_holder);
        sendCommentButton  = (AppCompatImageButton) view.findViewById(R.id.fragment_discussion_send_content_button);

        authorImageHolder  = (CircleImageView) view.findViewById(R.id.list_header_topic_author_image);
        swipeRefresh       = (SwipeRefreshLayout) view.findViewById(R.id.fragment_discussion_swipe_refresh);
        list               = (RecyclerView) view.findViewById(R.id.fragment_discussion_comments_list);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.refreshDataset();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.onStart();
        presenter.init(categoryID, questionID);
        adapter.subscribeToSourceChannel(presenter.provideDataChannel(adapter.getStateObservable(list)));
        if(State.listState != null) {
            list.getLayoutManager().onRestoreInstanceState(State.listState);
            State.listState = null;
        }
        if(headerContent == null) {
            presenter.loadDiscussionHeader();
        } else {
            displayDiscussionHeader(headerContent);
        }
    }

    @Override
    public void onPause() {
        ((DrawerActivity) getActivity()).setTitle("");
        listState = list.getLayoutManager().onSaveInstanceState();
        adapter.onStop();
        super.onPause();
    }

    @Override
    public void displayDiscussionHeader(DiscussionViewEntity entity) {
        this.headerContent = entity;

        ((DrawerActivity) getActivity()).setTitle(headerContent.getTitle());

        String answersCount = getActivity().getApplicationContext().getResources().getQuantityString(R.plurals.format_answers_count, entity.getAnswersCount(), entity.getAnswersCount());
        answersCountHolder.setText(answersCount);

        String authorName = getActivity().getApplicationContext().getResources().getString(R.string.format_space_delimiter_2);
        authorName = String.format(authorName, entity.getAuthorFirstName(), entity.getAuthorLastName());
        authorNameHolder.setText(authorName);

        topicTitleHolder.setText(entity.getTitle());
        topicContentHolder.setText(entity.getContent());

        if(authorImageHolder.getDrawable() != null) {
            authorImageHolder.setImageDrawable(null);
        }

        if(entity.getAuthorImage() != null && !entity.getAuthorImage().equals("") && !entity.getAuthorImage().equals("null")) {
            Glide.with(getActivity().getApplicationContext())
                    .load(entity.getAuthorImage())
                    .asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(authorImageHolder);
        }
    }

    @Override
    public void onCommentAdded(CommentViewEntity comment) {
        try {
            comment.setTime(DateUtils.getCurrentDateTime(getActivity().getApplication()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        adapter.addItem(comment);
    }

    @Override
    public Observable<String> getSendButtonClicksObservable() {
        return RxView.clicks(sendCommentButton)
                .map(value -> commentInputHolder.getText().toString())
                .doOnNext(value -> {
                    commentInputHolder.setText("");
                    commentInputHolder.clearFocus();
                    ((InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(commentInputHolder.getWindowToken(), 0);
                });
    }

    @Override
    public Observable<String> getCommentInputChangeEventObservable() {
        return RxEditText.textChanges(commentInputHolder)
                .debounce(250, TimeUnit.MILLISECONDS)
                .map(CharSequence::toString);
    }

    @Override
    protected PresenterLoader.Info<IDiscussionView> getLoaderInfo() {
        return new PresenterLoader.Info<IDiscussionView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_DISCUSSION;
            }

            @Override
            public Presenter.Factory<IDiscussionView> getFactory() {
                return new Presenter.Factory<IDiscussionView>() {
                    @Override
                    public <P extends Presenter<IDiscussionView>> P create() {
                        return (P) new DiscussionPresenter();
                    }
                };
            }

            @Override
            public IDiscussionView getView() {
                return DiscussionFragment.this;
            }
        };
    }

    static class State {

        static final String ARGUMENTS_CATEGORY = DiscussionFragment.class.getCanonicalName() + ".CATEGORY";
        static final String ARGUMENTS_QUESTION = DiscussionFragment.class.getCanonicalName() + ".QUESTION";

        static final String STATE_LIST = DiscussionFragment.class.getCanonicalName() + ".LIST_STATE";
        static final String STATE_ADAPTER = DiscussionFragment.class.getCanonicalName() + ".ADAPTER_STATE";
        static final String STATE_HEADER = DiscussionFragment.class.getCanonicalName() + ".HEADER_STATE";

        static Parcelable listState = null;
    }
}
