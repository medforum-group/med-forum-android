package net.styleru.i_komarov.medforum.common.view;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 29.11.16.
 */

public interface IMessageInteract {

    void showMessage(String title, String message);

    void showMessage(@StringRes int title, String message);

    void showMessage(@StringRes int title, @StringRes int message);
}
