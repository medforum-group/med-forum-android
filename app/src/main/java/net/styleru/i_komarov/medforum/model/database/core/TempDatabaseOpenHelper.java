package net.styleru.i_komarov.medforum.model.database.core;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.styleru.i_komarov.medforum.model.database.mapping.answers.AnswersTableConfig;
import net.styleru.i_komarov.medforum.model.database.mapping.categories.CategoriesTableConfig;
import net.styleru.i_komarov.medforum.model.database.mapping.questions.QuestionsTableConfig;

/**
 * Created by i_komarov on 14.01.17.
 */

class TempDatabaseOpenHelper extends SQLiteOpenHelper {

    TempDatabaseOpenHelper(Context context) {
        super(context, DatabaseConfig.DATABASE_NAME, null, DatabaseConfig.SCHEMA_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CategoriesTableConfig.getCreateTableQuery());
        db.execSQL(QuestionsTableConfig.getCreateTableQuery());
        db.execSQL(AnswersTableConfig.getCreateTableQuery());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //no implementation
    }
}
