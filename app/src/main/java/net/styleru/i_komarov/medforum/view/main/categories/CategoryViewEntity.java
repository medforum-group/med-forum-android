package net.styleru.i_komarov.medforum.view.main.categories;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 09.12.16.
 */

public class CategoryViewEntity implements Parcelable {

    private String id;
    private String title;
    private int topicsCount;
    private String lastUpdate;

    public CategoryViewEntity(Parcel in) {
        id = in.readString();
        title = in.readString();
        topicsCount = in.readInt();
        lastUpdate = in.readString();
    }

    public CategoryViewEntity(String id, String title, int topicsCount, String lastUpdate) {
        this.id = id;
        this.title = title;
        this.topicsCount = topicsCount;
        this.lastUpdate = lastUpdate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTopicsCount() {
        return topicsCount;
    }

    public void setTopicsCount(int topicsCount) {
        this.topicsCount = topicsCount;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "CategoryViewEntity{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", topicsCount='" + topicsCount + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeInt(topicsCount);
        dest.writeString(lastUpdate);
    }

    public static final Parcelable.Creator<CategoryViewEntity> CREATOR = new Creator<CategoryViewEntity>() {
        @Override
        public CategoryViewEntity createFromParcel(Parcel in) {
            return new CategoryViewEntity(in);
        }

        @Override
        public CategoryViewEntity[] newArray(int size) {
            return new CategoryViewEntity[0];
        }
    };
}
