package net.styleru.i_komarov.medforum.presenter.registration.registration1;

import android.util.Log;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.UsersProxy;
import net.styleru.i_komarov.medforum.model.common.ResultResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.view.registration.step_1.IRegistrationStep1View;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

/**
 * Created by i_komarov on 06.01.17.
 */

public class RegistrationStep1Presenter extends Presenter<IRegistrationStep1View> {

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IRegistrationStep1View view) {
        super.bindView(view);
        subscribeToAttachPhotoButtonClicksObservable(view.getAttachPhotoButtonClickObservable());
        subscribeToRegisterButtonClicksObservable(view.getRegisterButtonClicksObservable());
    }

    @Override
    public void unbindView() {
        super.unbindView();
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onDestroy() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    private void subscribeToAttachPhotoButtonClicksObservable(Observable<Integer> attachPhotoButtonClicksObservable) {
        //TODO: uncomment and complete below!
        attachPhotoButtonClicksObservable.subscribe(createAttachPhotoButtonClicksObserver());
    }

    private void subscribeToRegisterButtonClicksObservable(Observable<UserServerEntity> registerButtonClicksObservable) {
        registerButtonClicksObservable
                .flatMap(new Function<UserServerEntity, ObservableSource<UserServerEntity>>() {
                    @Override
                    public ObservableSource<UserServerEntity> apply(UserServerEntity user) throws Exception {
                        return UsersProxy.getInstance().updatePersonalData(user.getId(), user.getToken(), user.getFirstName(), user.getLastName())
                                .subscribeOn(Schedulers.io())
                                .filter(RegistrationStep1Presenter.this::checkResponse)
                                .map(response -> user);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createRegisterButtonClicksObserver());
    }

    private DisposableObserver<Integer> createAttachPhotoButtonClicksObserver() {
        DisposableObserver<Integer> attachPhotoButtonClicksObserver = new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                //TODO: make below work with rxPermissions
                //view.onPhotoPickerStartRequest();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(attachPhotoButtonClicksObserver);

        return attachPhotoButtonClicksObserver;
    }

    private DisposableObserver<UserServerEntity> createRegisterButtonClicksObserver() {
        DisposableObserver<UserServerEntity> registerButtonClicksObserver = new DisposableObserver<UserServerEntity>() {
            @Override
            public void onNext(UserServerEntity user) {
                Log.d("RegistrationStep1Presenter", "user: " + user);
                view.onRegistrationSucceeded(user);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(registerButtonClicksObserver);

        return registerButtonClicksObserver;
    }

    private boolean checkResponse(Response<ResultResponseWrapper> response) {
        if(response.code() != 200 || response.body() == null || !response.body().status()) {
            Log.d("VerificationPresenter", "checkResponse: " + response.code());
            //view.showHttpError(httpCode);
            //TODO: add view.showHttpError(httpCode);
            return false;
        } else {
            return true;
        }
    }
}
