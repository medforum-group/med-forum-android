package net.styleru.i_komarov.medforum.view.main.topics;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.model.datetime.DateUtils;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 09.12.16.
 */

public class TopicViewHolder extends BindableViewHolder<TopicViewEntity, BindableViewHolder.ActionListener<TopicViewEntity>> {

    private String answersCountFormatString;
    private String lastUpdateFormatString;
    private String twoParamsSpaceDelimeterFormatString;
    private String dateTimeFormat;

    private AppCompatTextView titleHolder;
    private AppCompatTextView answersCountHolder;
    private AppCompatTextView lastUpdateHolder;
    private AppCompatTextView authorNameHolder;

    private ImageView completedToggle;

    private CircleImageView authorImageHolder;

    public TopicViewHolder(View itemView) {
        super(itemView);

        titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_title);
        answersCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_answers_count);
        lastUpdateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_last_update);
        authorNameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_topic_author_name);

        completedToggle = (ImageView) itemView.findViewById(R.id.list_item_topic_ic_toggle);

        completedToggle.setColorFilter(itemView.getResources().getColor(R.color.colorAccentExtra));

        authorImageHolder = (CircleImageView) itemView.findViewById(R.id.list_item_topic_author_image);

        lastUpdateFormatString = itemView.getResources().getString(R.string.format_last_update);
        answersCountFormatString = itemView.getResources().getQuantityString(R.plurals.format_answers_count, 0, 0);
        twoParamsSpaceDelimeterFormatString = itemView.getResources().getString(R.string.format_space_delimiter_2);
        dateTimeFormat = itemView.getResources().getString(R.string.format_date_time);
    }

    @Override
    public void bind(int position, TopicViewEntity item, ActionListener<TopicViewEntity> listener) {
        super.bind(position, item, listener);

        titleHolder.setText("");
        answersCountHolder.setText("");
        lastUpdateHolder.setText("");
        authorNameHolder.setText("");

        answersCountFormatString = itemView.getResources().getQuantityString(R.plurals.format_answers_count, Integer.valueOf(item.getAnswersCount()), Integer.valueOf(item.getAnswersCount()));

        titleHolder.setText(item.getTitle());
        answersCountHolder.setText(answersCountFormatString);
        try {
            DateUtils.DisplayDate data = DateUtils.DisplayDate.fromServerDateTime(item.getLastUpdate());
            lastUpdateHolder.setText(String.format(dateTimeFormat, data.getDisplayDate(), data.getDisplayTime()));
        } catch(Exception e) {

        }
        authorNameHolder.setText(String.format(twoParamsSpaceDelimeterFormatString, item.getAuthorFirstName(), item.getAuthorLastName()));

        if(authorImageHolder.getDrawable() != null) {
            authorImageHolder.setImageDrawable(null);
        }

        completedToggle.setVisibility(item.getCompleted() ? View.VISIBLE : View.GONE);

        if(item.getAuthorPhoto() != null && !item.getAuthorPhoto().equals("") && !item.getAuthorPhoto().equals("null")) {
            Glide.with(itemView.getContext())
                    .fromString()
                    .asBitmap()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .load(item.getAuthorPhoto())
                    .into(authorImageHolder);
        }
    }
}
