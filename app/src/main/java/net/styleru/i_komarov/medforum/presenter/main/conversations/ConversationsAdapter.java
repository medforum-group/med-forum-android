package net.styleru.i_komarov.medforum.presenter.main.conversations;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.conversations.ConversationViewEntity;
import net.styleru.i_komarov.medforum.view.main.conversations.ConversationViewHolder;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.BiPredicate;

/**
 * Created by i_komarov on 23.01.17.
 */

public class ConversationsAdapter extends RecyclerView.Adapter<BindableViewHolder<ConversationViewEntity, BindableViewHolder.ActionListener<ConversationViewEntity>>> implements Parcelable, IConversationsManager {

    private BindableViewHolder.ActionListener<ConversationViewEntity> listener;

    private List<ConversationViewEntity> items;

    private int limit = 30;

    public ConversationsAdapter() {
        this.items = new ArrayList<>();
    }

    public ConversationsAdapter(Parcel in) {
        in.readList(this.items, List.class.getClassLoader());
    }

    @Override
    public BindableViewHolder<ConversationViewEntity, BindableViewHolder.ActionListener<ConversationViewEntity>> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ConversationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_conversation, parent, false));
    }

    @Override
    public void onBindViewHolder(BindableViewHolder<ConversationViewEntity, BindableViewHolder.ActionListener<ConversationViewEntity>> holder, int position) {
        holder.bind(position, items.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setListener(BindableViewHolder.ActionListener<ConversationViewEntity> listener) {
        this.listener = listener;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void add(ConversationViewEntity item) {
        final int insertPosition = this.items.size();
        this.items.add(item);
        notifyItemInserted(insertPosition);
    }

    @Override
    public void add(List<ConversationViewEntity> items) {
        final int insertPosition = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(insertPosition, items.size());
    }

    @Override
    public void addTop(ConversationViewEntity item) {
        final int insertPosition = 0;
        this.items.add(insertPosition, item);
        notifyItemInserted(insertPosition);
    }

    @Override
    public void addTop(List<ConversationViewEntity> items) {
        final int insertPosition = 0;
        this.items.addAll(insertPosition, items);
        notifyItemRangeInserted(insertPosition, items.size());
    }

    @Override
    public void pushTop(ConversationViewEntity item) {
        if(this.items.size() > 0) {
            ConversationViewEntity itemToRemove = null;
            for (ConversationViewEntity conversation : items) {
                if (conversation.getUri().equals(item.getUri())) {
                    itemToRemove = conversation;
                    break;
                }
            }
            if(itemToRemove != null) {
                final int removePosition = this.items.indexOf(itemToRemove);
                this.items.remove(itemToRemove);
                notifyItemRemoved(removePosition);
            }
        }

        this.items.add(0, item);
        notifyItemInserted(0);
    }

    @Override
    public void pushTop(List<ConversationViewEntity> items) {
        for(int i = items.size() - 1; i >= 0; i--) {
            pushTop(items.get(i));
        }
    }

    @Override
    public String lastUri() {
        if(this.items.size() > 0) {
            Log.d("ConversationsAdapter", "last uri: " + this.items.get(this.items.size() - 1).getUri());
            return this.items.get(this.items.size() - 1).getUri();
        } else {
            return null;
        }
    }

    @Override
    public String firstUri() {
        if(this.items.size() > 0) {
            Log.d("ConversationsAdapter", "firstUri: " + this.items.get(0).getUri());
            return this.items.get(0).getUri();
        } else {
            return null;
        }
    }

    public Observable<LayerListState> createListStateObservable(RecyclerView list) {
        return Observable.create(
                new ObservableOnSubscribe<LayerListState>() {
                    @Override
                    public void subscribe(ObservableEmitter<LayerListState> stateEmitter) throws Exception {
                        list.setOnScrollListener(buildPaginationListener(stateEmitter));
                    }
                })
                .distinctUntilChanged(
                        new BiPredicate<LayerListState, LayerListState>() {
                            @Override
                            public boolean test(LayerListState state, LayerListState state2) throws Exception {
                                return state.getLastUri().equals(state2.getLastUri());
                            }
                        }
                );
    }

    private LayerListState getState() {
        String lastUri = null;
        if(this.items.size() > 0) {
            lastUri = this.items.get(this.items.size() - 1).getUri();
        }

        return new LayerListState(lastUri, limit);
    }

    private RecyclerView.OnScrollListener buildPaginationListener(final ObservableEmitter<LayerListState> stateEmitter) {
        stateEmitter.onNext(new LayerListState(lastUri(), limit));

        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LayerListState state = getState();

                int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                int updatePosition = getItemCount() - 1 - (state.getLimit() / 2);

                if(updatePosition <= position) {
                    stateEmitter.onNext(state);
                }
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(items);
    }

    public static final Parcelable.Creator<ConversationsAdapter> CREATOR = new Creator<ConversationsAdapter>() {
        @Override
        public ConversationsAdapter createFromParcel(Parcel in) {
            return new ConversationsAdapter(in);
        }

        @Override
        public ConversationsAdapter[] newArray(int size) {
            return new ConversationsAdapter[0];
        }
    };
}
