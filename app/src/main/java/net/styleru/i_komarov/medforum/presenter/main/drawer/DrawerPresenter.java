package net.styleru.i_komarov.medforum.presenter.main.drawer;

import android.util.Log;
import android.view.MenuItem;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.layer.dao.AuthenticationProxy;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerSessionToken;
import net.styleru.i_komarov.medforum.view.main.drawer.IDrawerView;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Response;

/**
 * Created by i_komarov on 07.12.16.
 */

public class DrawerPresenter extends Presenter<IDrawerView> {

    private static final String TAG = "DrawerPresenter";

    public DrawerPresenter() {
        super();
    }

    @Override
    public void onStart() {

    }

    public void onUserAuthenticated() {
        AuthenticationProxy.getInstance().authenticate(PreferencesManager.getInstance().loadUserID())
                .subscribe(new DisposableObserver<Response<LayerSessionToken>>() {
                    @Override
                    public void onNext(Response<LayerSessionToken> value) {
                        PreferencesManager.getInstance().saveSessionToken(value.body().getSessionToken());
                        Log.d("DrawerPresenter", "layer authentication result: " + value.body());
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void bindView(IDrawerView view) {
        super.bindView(view);

        setToolbarEventChannels(view.getToolbarNavigationClickEventChannel());
        setNavigationViewEventChannels(view.getNavigationItemClickEventChannel());
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
    }

    @Override
    public void onDestroy() {
        disposeAllUIObservers();
    }

    private void setToolbarEventChannels(Observable<Integer> navigationClicks) {
        DisposableObserver<Integer> navigationClicksObserver = createToolbarNavigationClicksObserver();
        addObserver(navigationClicksObserver);
        navigationClicks.subscribe(navigationClicksObserver);
    }

    private void setNavigationViewEventChannels(Observable<MenuItem> itemSelections) {
        DisposableObserver<MenuItem> navigationViewItemSelectsObserver = createNavigationViewItemSelectsObserver();
        addObserver(navigationViewItemSelectsObserver);
        itemSelections.subscribe(navigationViewItemSelectsObserver);
    }

    private DisposableObserver<Integer> createToolbarNavigationClicksObserver() {
        return new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                view.onNavigationClicked();
            }

            @Override
            public void onError(Throwable e) {
                view.showError(R.string.dialog_error_content_unknown_error);
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private DisposableObserver<MenuItem> createNavigationViewItemSelectsObserver() {
        return new DisposableObserver<MenuItem>() {
            @Override
            public void onNext(MenuItem item) {
                Log.d(TAG, "navigationViewItemSelectionObserver.onNext");
                view.onNavigationViewItemSelected(item);
            }

            @Override
            public void onError(Throwable e) {
                view.showError(R.string.dialog_error_content_unknown_error);
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
