package net.styleru.i_komarov.medforum.model.layer.repository;

import net.styleru.i_komarov.medforum.model.layer.dto.LayerIdentityEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerNonceEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerSessionToken;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

import static net.styleru.i_komarov.medforum.model.common.LayerEndpoints.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public interface AuthenticationRepository {

    @Headers("Accept: application/vnd.layer+json; version=2.0")
    @POST(ENDPOINT_NONCE)
    Observable<Response<LayerNonceEntity>> getNonce(

    );

    @Headers("Accept: application/vnd.layer+json; version=2.0")
    @POST(ENDPOINT_SESSIONS)
    Observable<Response<LayerSessionToken>> getToken(
            @Body LayerIdentityEntity identity
    );
}
