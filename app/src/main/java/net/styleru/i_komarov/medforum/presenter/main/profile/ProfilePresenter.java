package net.styleru.i_komarov.medforum.presenter.main.profile;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.view.main.profile.IProfileView;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 08.01.17.
 */

public class ProfilePresenter extends Presenter<IProfileView> {

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IProfileView view) {
        super.bindView(view);
        subscribeOnSignOutButtonClicksObservable(view.getSignOutButtonClicksObservable());
    }

    @Override
    public void unbindView() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
        super.unbindView();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    private void subscribeOnSignOutButtonClicksObservable(Observable<Integer> signOutButtonClicksObservable) {
        signOutButtonClicksObservable.subscribe(createSignOutButtonClicksObserver());
    }

    private DisposableObserver<Integer> createSignOutButtonClicksObserver() {
        DisposableObserver<Integer> signOutButtonClicksObserver = new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                //TODO: perfom sign out
                view.onSignOutSucceeded();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(signOutButtonClicksObserver);

        return signOutButtonClicksObserver;
    }
}
