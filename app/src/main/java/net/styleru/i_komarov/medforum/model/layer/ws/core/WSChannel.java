package net.styleru.i_komarov.medforum.model.layer.ws.core;

import android.util.Log;

import com.google.gson.Gson;

import net.styleru.i_komarov.medforum.model.layer.ws.dto.WSResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.functions.Action;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okio.ByteString;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSChannel {

    private WSChannel() {
        throw new IllegalStateException("No instances please");
    }

    public static class Factory {

        private static final String LAYER_URI = "wss://websockets.layer.com/?session_token=%1$s";
        private static final String LAYER_PROTOCOL = "layer-2.0";

        private static volatile Factory instance;

        private Map<String, IWSListener> registeredListeners;

        private OkHttpClient client;
        private WebSocket socket;

        private Scheduler defaultScheduler;

        private Factory(String sessionToken) {
            this.defaultScheduler = Schedulers.io();

            this.registeredListeners = new HashMap<>();

            this.client = new OkHttpClient.Builder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(15, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();

            this.socket = client.newWebSocket(
                    new Request.Builder()
                            .url(String.format(LAYER_URI, sessionToken))
                            .addHeader("Sec-WebSocket-Protocol", LAYER_PROTOCOL)
                            .build(),
                    new WebSocketListener() {
                        @Override
                        public void onOpen(WebSocket webSocket, Response response) {
                            super.onOpen(webSocket, response);
                        }

                        @Override
                        public void onMessage(WebSocket webSocket, String text) {
                            super.onMessage(webSocket, text);
                            notifyAllListeners(text);
                        }

                        @Override
                        public void onMessage(WebSocket webSocket, ByteString bytes) {
                            super.onMessage(webSocket, bytes);
                        }

                        @Override
                        public void onClosing(WebSocket webSocket, int code, String reason) {
                            super.onClosing(webSocket, code, reason);
                        }

                        @Override
                        public void onClosed(WebSocket webSocket, int code, String reason) {
                            super.onClosed(webSocket, code, reason);
                        }

                        @Override
                        public void onFailure(WebSocket webSocket, Throwable t, Response response) {
                            super.onFailure(webSocket, t, response);
                        }
                    }
            );
        }

        public static Factory getInstance(String sessionToken) {
            Factory localInstance = instance;
            if(localInstance == null) {
                synchronized (WSChannel.Factory.class) {
                    localInstance = instance;
                    if(localInstance == null) {
                        localInstance = instance = new Factory(sessionToken);
                    }
                }
            }

            return localInstance;
        }

        public Observable<WSResponse> newWSChannelObservable(String recipientKey) {
            return Observable.create(
                    new ObservableOnSubscribe<WSResponse>() {
                        @Override
                        public void subscribe(final ObservableEmitter<WSResponse> emitter) throws Exception {
                            registerListener(
                                    recipientKey,
                                    new IWSListener() {
                                        @Override
                                        public void onMessage(String message) {
                                            try {
                                                if(!emitter.isDisposed()) {
                                                    emitter.onNext(new Gson().fromJson(message, WSResponse.class));
                                                }
                                            } catch(Exception e) {
                                                if(!emitter.isDisposed()) {
                                                    emitter.onError(e);
                                                }
                                            }
                                        }
                                    }
                            );
                        }
                    })
                    .doOnDispose(new Action() {
                        @Override
                        public void run() throws Exception {
                            unregisterListener(recipientKey);
                        }
                    })
                    .subscribeOn(defaultScheduler);
        }

        private void notifyAllListeners(String message) {
            String[] keySet = registeredListeners.keySet().toArray(new String[registeredListeners.keySet().size()]);
            for(int i = 0; i < keySet.length; i++) {
                synchronized (WSChannel.Factory.class) {
                    if(registeredListeners.containsKey(keySet[i])) {
                        registeredListeners.get(keySet[i]).onMessage(message);
                        Log.d("WSChannel", "listener: " + keySet[i] + ", notified: " + message);
                    }
                }
            }
        }

        private void registerListener(String recipientKey, IWSListener listener) {
            synchronized (WSChannel.Factory.class) {
                this.registeredListeners.put(recipientKey, listener);
                Log.d("WSChannel", "listener: " + recipientKey + ", registered");
            }
        }

        private void unregisterListener(String recipientKey) {
            synchronized (WSChannel.Factory.class) {
                if(this.registeredListeners.containsKey(recipientKey)) {
                    this.registeredListeners.remove(recipientKey);
                    Log.d("WSChannel", "listener: " + recipientKey + ", unregistered");
                }
            }
        }
    }
}
