package net.styleru.i_komarov.medforum.presenter.main.conversations;

/**
 * Created by i_komarov on 23.01.17.
 */

public class LayerListState {

    private String lastUri;
    private int limit;

    public LayerListState(String lastUri, int limit) {
        this.lastUri = lastUri;
        this.limit = limit;
    }

    public String getLastUri() {
        return lastUri;
    }

    public void setLastUri(String lastUri) {
        this.lastUri = lastUri;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
