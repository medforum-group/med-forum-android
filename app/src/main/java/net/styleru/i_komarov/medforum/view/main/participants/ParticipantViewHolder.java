package net.styleru.i_komarov.medforum.view.main.participants;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import com.bumptech.glide.Glide;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 29.12.16.
 */

public class ParticipantViewHolder extends BindableViewHolder<ParticipantViewEntity, BindableViewHolder.ActionListener<ParticipantViewEntity>> {

    private CircleImageView avatarHolder;
    private AppCompatTextView avatarAbbreviatureHolder;
    private AppCompatTextView fullnameHolder;

    private int colorBackgroundPrimary;

    public ParticipantViewHolder(View itemView) {
        super(itemView);

        avatarHolder = (CircleImageView) itemView.findViewById(R.id.list_item_participant_avatar);
        avatarAbbreviatureHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_participant_avatar_abbreviature);
        fullnameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_participant_full_name);

        colorBackgroundPrimary = itemView.getContext().getApplicationContext().getResources().getColor(R.color.colorBackgroundPrimary);
    }

    @Override
    public void bind(int position, ParticipantViewEntity item, ActionListener<ParticipantViewEntity> listener) {
        super.bind(position, item, listener);
        avatarHolder.setImageDrawable(null);
        avatarAbbreviatureHolder.setText("");

        if(item.getAvatarUrl() != null && !item.getAvatarUrl().equals("") && !item.getAvatarUrl().equals("null")) {
            Glide.with(itemView.getContext())
                    .load(item.getAvatarUrl())
                    .into(avatarHolder);
        } else {
            avatarHolder.setImageDrawable(new ColorDrawable(colorBackgroundPrimary));

            try {
                avatarAbbreviatureHolder.setText(String.valueOf(item.getFirstName().charAt(0)) + String.valueOf(item.getLastName().charAt(0)));
            } catch(Exception e) {
                try {
                    avatarAbbreviatureHolder.setText(String.valueOf(item.getFirstName().charAt(0)));
                } catch(Exception e1) {
                    avatarAbbreviatureHolder.setText("");
                }
            }
        }

        fullnameHolder.setText(String.format(itemView.getResources().getString(R.string.format_space_delimiter_2), item.getFirstName(), item.getLastName()));
    }
}
