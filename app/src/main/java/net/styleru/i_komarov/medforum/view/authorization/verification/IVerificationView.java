package net.styleru.i_komarov.medforum.view.authorization.verification;

import android.content.Intent;

import net.styleru.i_komarov.medforum.presenter.authorization.verification.VerificationPresenter;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.12.16.
 */

public interface IVerificationView {

    public Observable<Integer> getWrongPhoneNumberButtonClickObservable();

    public Observable<VerificationPresenter.PhoneAndCode> getVerifyCodeButtonClickObservable();

    public Observable<String> getResendCodeButtonClickObservable();

    public void updateResendButtonDisabledDelay(long secondsLast);

    public void onCodeVerificationSucceeded(Intent data);

    public void onCodeVerificationFailed();

    public void onUserChangePhoneAction();
}
