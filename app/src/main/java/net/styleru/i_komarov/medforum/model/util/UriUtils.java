package net.styleru.i_komarov.medforum.model.util;

/**
 * Created by i_komarov on 22.01.17.
 */

public class UriUtils {

    public static String extractEndpoint(String uri) {
        return uri.replaceFirst(".*/([^/?]+).*", "$1");
    }
}
