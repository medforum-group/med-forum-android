package net.styleru.i_komarov.medforum.model.api.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.*;

/**
 * Created by i_komarov on 03.01.17.
 */

public class QuestionServerEntity {

    @SerializedName(FIELD_QUESTION_QUESTION_ID)
    private String id;
    @SerializedName(FIELD_QUESTION_USER_ID)
    private String userID;
    @SerializedName(FIELD_QUESTION_RESPONSE_ID)
    private String categoryID;
    @SerializedName(FIELD_TITLE)
    private String title;
    @SerializedName(FIELD_TEXT)
    private String content;
    @SerializedName(FIELD_LAST_UPDATE)
    private String lastUpdate;
    @SerializedName(FIELD_NAME)
    private String firstName;
    @SerializedName(FIELD_SURNAME)
    private String lastName;
    @SerializedName(FIELD_IMAGE)
    private String imageUrl;
    @SerializedName(FIELD_ROLE)
    private String role;
    @SerializedName(FIELD_QUESTION_COUNT)
    private int count;

    public QuestionServerEntity(String id, String userID, String categoryID, String title, String content, String lastUpdate, String firstName, String lastName, String imageUrl, String role, int count) {
        this.id = id;
        this.userID = userID;
        this.categoryID = categoryID;
        this.title = title;
        this.content = content;
        this.lastUpdate = lastUpdate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "QuestionServerEntity{" +
                "id='" + id + '\'' +
                ", userID='" + userID + '\'' +
                ", categoryID='" + categoryID + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", role='" + role + '\'' +
                ", count=" + count +
                '}';
    }
}
