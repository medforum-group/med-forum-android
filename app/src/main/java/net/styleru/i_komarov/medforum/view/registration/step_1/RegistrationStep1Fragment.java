package net.styleru.i_komarov.medforum.view.registration.step_1;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.tbruyelle.rxpermissions2.RxPermissions;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.presenter.registration.registration1.RegistrationStep1Presenter;
import net.styleru.i_komarov.medforum.view.registration.main.RegistrationActivity;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;

import static android.app.Activity.RESULT_OK;

/**
 * Created by i_komarov on 06.01.17.
 */

public class RegistrationStep1Fragment extends RetainPresenterFragment<IRegistrationStep1View, RegistrationStep1Presenter> implements IRegistrationStep1View {

    private static final String KEY_USER_CURRENT = RegistrationStep1Fragment.class.getCanonicalName() + ".STATE_USER_CURRENT";

    private static final int REQUEST_CODE_IMAGE = 9987;

    private RxPermissions permissions;

    private AppCompatEditText firstNameHolder;
    private AppCompatEditText lastNameHolder;
    private CircleImageView   photoHolder;
    private RelativeLayout    attachPhotoButton;
    private AppCompatButton   registerButton;

    private UserServerEntity currentUser;

    public static Fragment newInstance(UserServerEntity currentUser) {
        Fragment fragment = new RegistrationStep1Fragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_USER_CURRENT, currentUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null && args.containsKey(KEY_USER_CURRENT)) {
            currentUser = args.getParcelable(KEY_USER_CURRENT);
        } else if(savedInstanceState != null && savedInstanceState.containsKey(KEY_USER_CURRENT)) {
            currentUser = savedInstanceState.getParcelable(KEY_USER_CURRENT);
        } else {
            throw new RuntimeException("User was not found neither in arguments nor in saved state!");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        permissions = new RxPermissions(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registration_1, parent, false);
    }

    @Override
    public Observable<UserServerEntity> getRegisterButtonClicksObservable() {
        return RxView.clicks(registerButton)
                .doOnNext(value -> {
                    firstNameHolder.setEnabled(false);
                    lastNameHolder.setEnabled(false);
                    registerButton.setEnabled(false);
                })
                .map(value -> {
                    currentUser.setFirstName(firstNameHolder.getText().toString());
                    currentUser.setLastName(lastNameHolder.getText().toString());
                    return currentUser;
                });
    }

    @Override
    public Observable<Integer> getAttachPhotoButtonClickObservable() {
        return RxView.clicks(attachPhotoButton);
                //.compose(permissions.ensure(Manifest.permission.READ_EXTERNAL_STORAGE))
                //.filter(isGranted -> isGranted)
                //.map(status -> 0);
    }

    @Override
    public void onPhotoPickerStartRequest() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setData(Uri.parse("image/*"));
        getActivity().startActivityForResult(intent, REQUEST_CODE_IMAGE);
    }

    @Override
    public void onRegistrationSucceeded(UserServerEntity user) {
        ((RegistrationActivity) getActivity()).onRegistrationSucceeded(user);
    }

    @Override
    public void onRegistrationFailed() {
        firstNameHolder.setEnabled(true);
        lastNameHolder.setEnabled(true);
        registerButton.setEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_IMAGE && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            //Log.d("RegistrationStep1Fragment", "image uri received: " + imageUri);
        }
    }

    @Override
    protected void bindViewComponents(View view) {
        firstNameHolder   = (AppCompatEditText) view.findViewById(R.id.fragment_registration_1_first_name_holder);
        lastNameHolder    = (AppCompatEditText) view.findViewById(R.id.fragment_registration_1_last_name_holder);
        photoHolder       = (CircleImageView) view.findViewById(R.id.fragment_registration_1_photo_holder);
        attachPhotoButton = (RelativeLayout) view.findViewById(R.id.fragment_registration_1_attach_photo_button);
        registerButton    = (AppCompatButton) view.findViewById(R.id.fragment_registration_1_register_button);
    }

    @Override
    protected PresenterLoader.Info<IRegistrationStep1View> getLoaderInfo() {
        return new PresenterLoader.Info<IRegistrationStep1View>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_REGISTRATION_1;
            }

            @Override
            public Presenter.Factory<IRegistrationStep1View> getFactory() {
                return new Presenter.Factory<IRegistrationStep1View>() {
                    @Override
                    public <P extends Presenter<IRegistrationStep1View>> P create() {
                        return (P) new RegistrationStep1Presenter();
                    }
                };
            }

            @Override
            public IRegistrationStep1View getView() {
                return RegistrationStep1Fragment.this;
            }
        };
    }
}
