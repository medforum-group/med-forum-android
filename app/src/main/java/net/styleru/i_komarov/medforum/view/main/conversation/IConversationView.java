package net.styleru.i_komarov.medforum.view.main.conversation;

import net.styleru.i_komarov.medforum.presenter.main.conversation.MessagesAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.01.17.
 */

public interface IConversationView {

    public IMessagesManager getMessagesManager();

    public Observable<String> getSendContentButtonClicksObservable();

    public Observable<State> getListStateObservable();

    public Observable<String> getContentTypingEventsObservable();
}
