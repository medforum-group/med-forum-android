package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerIdentifierUrlPair {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_URL)
    private String url;

    public LayerIdentifierUrlPair(String id, String url) {
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
