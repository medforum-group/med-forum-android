package net.styleru.i_komarov.medforum.model.database.entities.answers;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.i_komarov.medforum.model.database.mapping.answers.AnswersTableConfig;

/**
 * Created by i_komarov on 15.01.17.
 */

@StorIOSQLiteType(table = AnswersTableConfig.TABLE)
public class AnswerDatabaseEntity {

    @Nullable
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_SERVER_ID)
    String serverID;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_QUESTION_ID)
    String questionID;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_USER_ID)
    String userID;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_FIRST_NAME)
    String firstName;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_LAST_NAME)
    String lastName;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_IMAGE_URL)
    String imageUrl;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_ROLE)
    String role;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_DATE)
    String date;

    @NonNull
    @StorIOSQLiteColumn(name = AnswersTableConfig.COLUMN_CONTENT)
    String content;

    AnswerDatabaseEntity() {

    }

    public static AnswerDatabaseEntity newInstance(String serverID, String questionID, String userID, String firstName, String lastName,
                                                   String imageUrl, String role, String date, String content) {
        AnswerDatabaseEntity entity = new AnswerDatabaseEntity();
        entity.serverID = serverID;
        entity.questionID = questionID;
        entity.userID = userID;
        entity.firstName = firstName;
        entity.lastName = lastName;
        entity.imageUrl = imageUrl;
        entity.role = role;
        entity.date = date;
        entity.content = content;

        return entity;
    }

    public static AnswerDatabaseEntity newInstance(Long id, String serverID, String questionID, String userID, String firstName, String lastName,
                                                   String imageUrl, String role, String date, String content) {
        AnswerDatabaseEntity entity = new AnswerDatabaseEntity();
        entity.id = id;
        entity.serverID = serverID;
        entity.questionID = questionID;
        entity.userID = userID;
        entity.firstName = firstName;
        entity.lastName = lastName;
        entity.imageUrl = imageUrl;
        entity.role = role;
        entity.date = date;
        entity.content = content;

        return entity;
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @NonNull
    public String getServerID() {
        return serverID;
    }

    public void setServerID(@NonNull String serverID) {
        this.serverID = serverID;
    }

    @NonNull
    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(@NonNull String questionID) {
        this.questionID = questionID;
    }

    @NonNull
    public String getUserID() {
        return userID;
    }

    public void setUserID(@NonNull String userID) {
        this.userID = userID;
    }

    @NonNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NonNull String firstName) {
        this.firstName = firstName;
    }

    @NonNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NonNull String lastName) {
        this.lastName = lastName;
    }

    @NonNull
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@NonNull String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @NonNull
    public String getRole() {
        return role;
    }

    public void setRole(@NonNull String role) {
        this.role = role;
    }

    @NonNull
    public String getDate() {
        return date;
    }

    public void setDate(@NonNull String date) {
        this.date = date;
    }

    @NonNull
    public String getContent() {
        return content;
    }

    public void setContent(@NonNull String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnswerDatabaseEntity that = (AnswerDatabaseEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!serverID.equals(that.serverID)) return false;
        if (!questionID.equals(that.questionID)) return false;
        if (!userID.equals(that.userID)) return false;
        if (!firstName.equals(that.firstName)) return false;
        if (!lastName.equals(that.lastName)) return false;
        if (!imageUrl.equals(that.imageUrl)) return false;
        if (!role.equals(that.role)) return false;
        if (!date.equals(that.date)) return false;
        return content.equals(that.content);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + serverID.hashCode();
        result = 31 * result + questionID.hashCode();
        result = 31 * result + userID.hashCode();
        result = 31 * result + firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + imageUrl.hashCode();
        result = 31 * result + role.hashCode();
        result = 31 * result + date.hashCode();
        result = 31 * result + content.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "AnswerDatabaseEntity{" +
                "id=" + id +
                ", serverID='" + serverID + '\'' +
                ", questionID='" + questionID + '\'' +
                ", userID='" + userID + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", role='" + role + '\'' +
                ", date='" + date + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
