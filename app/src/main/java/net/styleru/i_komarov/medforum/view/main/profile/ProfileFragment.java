package net.styleru.i_komarov.medforum.view.main.profile;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.profile.ProfilePresenter;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Observable;

/**
 * Created by i_komarov on 06.01.17.
 */

public class ProfileFragment extends RetainPresenterFragment<IProfileView, ProfilePresenter> implements IProfileView {

    private CircleImageView avatarHolder;
    private AppCompatTextView fullNameHolder;
    private AppCompatButton signOutButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        avatarHolder   = (CircleImageView) view.findViewById(R.id.fragment_profile_avatar_holder);
        fullNameHolder = (AppCompatTextView) view.findViewById(R.id.fragment_profile_fullname_holder);
        signOutButton  = (AppCompatButton) view.findViewById(R.id.fragment_profile_sign_out_button);
    }

    @Override
    public void onSignOutSucceeded() {
        //TODO: on sign out success
    }

    @Override
    public void onSignOutFailed() {
        //TODO: show some error maybe ?
        signOutButton.setEnabled(true);
    }

    @Override
    public void displayUserProfile(ParticipantViewEntity participant) {
        if(participant.getAvatarUrl() != null && !participant.getAvatarUrl().equals("") && !participant.getAvatarUrl().equals("null")) {
            Glide.with(getActivity())
                    .load(participant.getAvatarUrl())
                    .into(avatarHolder);
        }

        fullNameHolder.setText(String.format(getActivity().getApplicationContext().getString(R.string.format_space_delimiter_2), participant.getFirstName(), participant.getLastName()));
    }

    @Override
    public Observable<Integer> getSignOutButtonClicksObservable() {
        return RxView.clicks(signOutButton)
                .doOnNext(value -> signOutButton.setEnabled(false));
    }

    @Override
    protected PresenterLoader.Info<IProfileView> getLoaderInfo() {
        return new PresenterLoader.Info<IProfileView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_PROFILE;
            }

            @Override
            public Presenter.Factory<IProfileView> getFactory() {
                return new Presenter.Factory<IProfileView>() {
                    @Override
                    public <P extends Presenter<IProfileView>> P create() {
                        return (P) new ProfilePresenter();
                    }
                };
            }

            @Override
            public IProfileView getView() {
                return ProfileFragment.this;
            }
        };
    }
}
