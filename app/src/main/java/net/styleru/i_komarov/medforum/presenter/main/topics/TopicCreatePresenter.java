package net.styleru.i_komarov.medforum.presenter.main.topics;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.QuestionsProxy;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.view.main.topic.ITopicCreateView;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 09.01.17.
 */

public class TopicCreatePresenter extends Presenter<ITopicCreateView> {

    private String categoryID;

    @Override
    public void onStart() {

    }

    public void init(String categoryID) {
        this.categoryID = categoryID;
    }

    @Override
    public void bindView(ITopicCreateView view) {
        super.bindView(view);
        subscribeOnCreateTopicButtonClicksObservable(view.getCreateTopicButtonClicksObservable());
    }

    @Override
    public void unbindView() {
        disposeAllUISubscribers();
        disposeAllUIObservers();
        super.unbindView();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    private void subscribeOnCreateTopicButtonClicksObservable(Observable<String[]> createTopicButtonClicksObservable) {
        createTopicButtonClicksObservable
                .flatMap(new Function<String[], ObservableSource<String[]>>() {
                    @Override
                    public ObservableSource<String[]> apply(String[] content) throws Exception {
                        return QuestionsProxy.getInstance().create(PreferencesManager.getInstance().loadUserID(), categoryID, content[0], content[1])
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .filter(response -> hookHttpCode(response.code()))
                                .map(value -> content);
                    }
                })
                .subscribe(createCreateTopicButtonClicksObserver());
    }

    private DisposableObserver<String[]> createCreateTopicButtonClicksObserver() {
        DisposableObserver<String[]> createTopicButtonClicksObserver = new DisposableObserver<String[]>() {
            @Override
            public void onNext(String[] data) {
                view.onTopicCreationSucceeded();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(createTopicButtonClicksObserver);

        return createTopicButtonClicksObserver;
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode == 200) {
            return true;
        } else {
            view.onTopicCreationFailed();
            return false;
        }
    }
}
