package net.styleru.i_komarov.medforum.view.authorization.main;

import android.content.Intent;

import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;

/**
 * Created by i_komarov on 19.12.16.
 */

public interface IAuthorizationView {

    void onAuthorizationSuccess(Intent data);

    void onAuthorizationFailure(Intent data);

    void onRegistrationNeeded(UserServerEntity data);

    void onPhoneVerified(String phone);

    void onUserChangePhoneAction();
}
