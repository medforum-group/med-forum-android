package net.styleru.i_komarov.medforum.view.main.topic;

import net.styleru.i_komarov.rxjava2_binding.text_input.on_subscribe.EditTextTextChangeOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 10.12.16.
 */

public interface IDiscussionView {

    public void displayDiscussionHeader(DiscussionViewEntity entity);

    public void onCommentAdded(CommentViewEntity comment);

    public Observable<String> getSendButtonClicksObservable();

    public Observable<String> getCommentInputChangeEventObservable();
}
