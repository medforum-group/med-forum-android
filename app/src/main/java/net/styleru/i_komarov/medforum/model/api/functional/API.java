package net.styleru.i_komarov.medforum.model.api.functional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.i_komarov.medforum.model.api.common.OkHttpClientPreferences;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.common.ResultResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.AnswerServerEntity;
import net.styleru.i_komarov.medforum.model.api.dto.CategoryServerEntity;
import net.styleru.i_komarov.medforum.model.api.dto.QuestionServerEntity;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerIdentityToken;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import static net.styleru.i_komarov.medforum.model.common.BaseUrls.*;
import static net.styleru.i_komarov.medforum.model.common.Endpoints.*;
import static net.styleru.i_komarov.medforum.model.common.Fields.*;

/**
 * Created by i_komarov on 03.01.17.
 */

public interface API {

    interface Users {

        @GET(ENDPOINT_USERS)
        Observable<Response<UserServerEntity>> get(
                @Query(FIELD_ID) String userID
        );

        @GET(ENDPOINT_USERS)
        Flowable<Response<List<UserServerEntity>>> getList(
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit
        );

        @GET(ENDPOINT_USERS)
        Flowable<Response<List<UserServerEntity>>> getList(
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit,
                @Query(FIELD_QUERY) String query
        );

        @FormUrlEncoded
        @POST(ENDPOINT_CODE)
        Observable<Response<Object>> code(
                @Field(FIELD_PHONE) String phone
        );

        @FormUrlEncoded
        @POST(ENDPOINT_AUTHORIZE)
        Observable<Response<UserServerEntity>> auth(
                @Field(FIELD_PHONE) String phone,
                @Field(FIELD_CODE) String code
        );

        @FormUrlEncoded
        @POST(ENDPOINT_REGISTRATION)
        Observable<Response<ResultResponseWrapper>> updatePersonalData(
                @Field(FIELD_USER_ID) String userID,
                @Field(FIELD_TOKEN) String token,
                @Field(FIELD_NAME) String firstName,
                @Field(FIELD_SURNAME) String lastName
        );

        @FormUrlEncoded
        @POST(ENDPOINT_TOKEN)
        Observable<Response<LayerIdentityToken>> getToken(
                @Field(FIELD_ID) String userID,
                @Field(FIELD_NONCE) String nonce
        );
    }

    interface Categories {

        @GET(ENDPOINT_CATEGORIES)
        Flowable<Response<ResponseWrapper<List<CategoryServerEntity>>>> getList(
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit
        );

        @GET(ENDPOINT_CATEGORIES)
        Flowable<Response<ResponseWrapper<List<CategoryServerEntity>>>> getList(
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit,
                @Query(FIELD_QUERY) String query
        );
    }

    interface Questions {

        @GET(ENDPOINT_QUESTION)
        Observable<Response<ResponseWrapper<List<QuestionServerEntity>>>> get(
                @Query(FIELD_QUESTION_ID) String questionID
        );

        @GET(ENDPOINT_QUESTIONS)
        Flowable<Response<ResponseWrapper<List<QuestionServerEntity>>>> getList(
                @Query(FIELD_CATEGORY_ID) String categoryID,
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit
        );

        @GET(ENDPOINT_QUESTIONS)
        Flowable<Response<ResponseWrapper<List<QuestionServerEntity>>>> getList(
                @Query(FIELD_CATEGORY_ID) String categoryID,
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit,
                @Query(FIELD_QUERY) String query
        );

        @FormUrlEncoded
        @POST(ENDPOINT_QUESTION)
        Observable<Response<ResultResponseWrapper>> create(
                @Field(FIELD_USER_ID) String userID,
                @Field(FIELD_CATEGORY_ID) String categoryID,
                @Field(FIELD_TITLE) String title,
                @Field(FIELD_TEXT) String content
        );
    }

    interface Answers {

        @GET(ENDPOINT_ANSWERS)
        Flowable<Response<ResponseWrapper<List<AnswerServerEntity>>>> getList(
                @Query(FIELD_QUESTION_ID) String questionID,
                @Query(FIELD_OFFSET) int offset,
                @Query(FIELD_LIMIT) int limit
        );

        @FormUrlEncoded
        @POST(ENDPOINT_ANSWER)
        Observable<Response<ResultResponseWrapper>> reply(
                @Field(FIELD_USER_ID) String userID,
                @Field(FIELD_QUESTION_ID) String questionID,
                @Field(FIELD_TEXT) String content
        );
    }

    class Factory {

        private static OkHttpClientPreferences preferences = new OkHttpClientPreferences();

        private static Retrofit retrofit = buildRetrofit(BASE_URL_MEDFORUM);

        public static Users buildUsersAPI() {
            return retrofit.create(Users.class);
        }

        public static Categories buildCategoriesAPI() {
            return retrofit.create(Categories.class);
        }

        public static Questions buildQuestionsAPI() {
            return retrofit.create(Questions.class);
        }

        public static Answers buildAnswersAPI() {
            return retrofit.create(Answers.class);
        }

        private static Retrofit buildRetrofit(String baseUrl) {
            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();

            return new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(buildClient())
                    .build();
        }

        private static OkHttpClient buildClient() {
            return new OkHttpClient.Builder()
                    .connectTimeout(preferences.getConnectTimeout(), TimeUnit.SECONDS)
                    .writeTimeout(preferences.getWriteTimeout(), TimeUnit.SECONDS)
                    .readTimeout(preferences.getConnectTimeout(), TimeUnit.SECONDS)
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build();
        }
    }
}
