package net.styleru.i_komarov.medforum.view.main.conversation;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 22.01.17.
 */

public class TheirMessageTextViewHolder extends BindableViewHolder<MessageViewEntity, BindableViewHolder.ActionListener<MessageViewEntity>> {

    private CircleImageView senderPhotoHolder;
    private AppCompatTextView senderNameSymbolsHolder;
    private AppCompatTextView contentHolder;

    private String nameSymbolsPattern = "%1$s%2$s";

    public TheirMessageTextViewHolder(View itemView) {
        super(itemView);

        senderPhotoHolder = (CircleImageView) itemView.findViewById(R.id.list_item_message_their_sender_photo);
        senderNameSymbolsHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_message_their_sender_name_symbols);
        contentHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_message_their_content_holder);
    }

    @Override
    public void bind(int position, MessageViewEntity item, ActionListener<MessageViewEntity> listener) {
        super.bind(position, item, listener);

        senderPhotoHolder.setImageDrawable(null);
        if(item.getFirstName() != null && item.getLastName() != null && item.getFirstName().length() > 0 && item.getLastName().length() > 0) {
            senderNameSymbolsHolder.setText(String.format(nameSymbolsPattern, item.getFirstName().substring(0, 1), item.getLastName().substring(0, 1)));
        }

        contentHolder.setText(item.getContent());
    }
}
