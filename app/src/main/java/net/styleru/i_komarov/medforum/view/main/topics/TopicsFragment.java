package net.styleru.i_komarov.medforum.view.main.topics;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.topics.TopicsPresenter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.drawer.DrawerActivity;
import net.styleru.i_komarov.medforum.view.main.drawer.IDrawerView;
import net.styleru.i_komarov.medforum.view.main.topic.DiscussionFragment;
import net.styleru.i_komarov.medforum.view.main.topic.TopicCreateFragment;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;
import net.styleru.i_komarov.rxjava2_binding.search_view.main.RxSearchView;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import io.reactivex.Observable;

import static net.styleru.i_komarov.medforum.view.main.topics.TopicsFragment.State.ARGUMENTS_CATEGORY;
import static net.styleru.i_komarov.medforum.view.main.topics.TopicsFragment.State.STATE_ADAPTER;
import static net.styleru.i_komarov.medforum.view.main.topics.TopicsFragment.State.STATE_LIST;
import static net.styleru.i_komarov.medforum.view.main.topics.TopicsFragment.State.listState;

/**
 * Created by i_komarov on 09.12.16.
 */

public class TopicsFragment extends RetainPresenterFragment<ITopicsView, TopicsPresenter> implements ITopicsView {

    private BindableAdapter<TopicViewEntity> adapter;

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView list;
    private SearchView searchView;
    private FloatingActionButton createTopicButton;

    private String categoryID;

    public static TopicsFragment newInstance(String categoryID) {
        Bundle args = new Bundle();
        args.putString(ARGUMENTS_CATEGORY, categoryID);
        TopicsFragment fragment = new TopicsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(ARGUMENTS_CATEGORY)) {
            categoryID = savedInstanceState.getString(ARGUMENTS_CATEGORY);
            Log.d("TopicsFragment", "found category in saved state");
        } else {
            Bundle args = getArguments();
            if(args != null && args.containsKey(ARGUMENTS_CATEGORY)) {
                Log.d("TopicsFragment", "found category in argumnets");
                categoryID = args.getString(ARGUMENTS_CATEGORY);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topics, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        swipeRefresh      = (SwipeRefreshLayout) view.findViewById(R.id.fragment_topics_swipe_refresh);
        list              = (RecyclerView) view.findViewById(R.id.fragment_topics_recycler_view);
        searchView        = (SearchView) view.findViewById(R.id.fragment_topics_search_view);
        createTopicButton = (FloatingActionButton) view.findViewById(R.id.fragment_topics_create_topic_button);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.refreshDataset();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable() {
        return RxSearchView.queryChanges(searchView);
    }

    @Override
    public Observable<Integer> getCreateTopicButtonClicksObservable() {
        return RxView.clicks(createTopicButton)
                .doOnNext(value -> {
                    createTopicButton.setEnabled(false);
                });
    }

    @Override
    public void onTopicCreated(TopicViewEntity topic) {
        createTopicButton.setEnabled(true);
        adapter.addItemToTop(topic);
    }

    @Override
    public void refreshAdapter() {
        adapter.refreshDataset();
    }

    @Override
    public void onCreateTopicButtonClicked() {
        ((DrawerActivity) getActivity()).onAddFragmentInitiated(true, TopicCreateFragment.newInstance(categoryID));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(adapter != null) {
            outState.putParcelable(STATE_ADAPTER, adapter);
        }
        if(list != null && listState != null) {
            outState.putParcelable(STATE_LIST, listState);
        }
        if(categoryID != null) {
            outState.putString(ARGUMENTS_CATEGORY, categoryID);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_ADAPTER)) {
            adapter = savedInstanceState.getParcelable(STATE_ADAPTER);
        }
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_LIST)) {
            listState = savedInstanceState.getParcelable(STATE_LIST);
        }
        if(adapter == null) {
            adapter = new BindableAdapter<>(R.layout.list_item_topic, TopicViewHolder.class);
        }
        list.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DrawerActivity) getActivity()).setTitle(getActivity().getApplicationContext().getResources().getString(R.string.title_questions));
        adapter.onStart();
        presenter.init(categoryID);
        adapter.subscribeToSourceChannel(presenter.provideDataChannel(adapter.getStateObservable(list)));
        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
            listState = null;
        }
        adapter.setActionListener(new BindableViewHolder.ActionListener<TopicViewEntity>() {
            @Override
            public void clicked(int position, TopicViewEntity item) {
                ((IDrawerView) getActivity()).onAddFragmentInitiated(true, DiscussionFragment.newInstance(categoryID, item.getId()));
            }

            @Override
            public void longClicked(int position, TopicViewEntity item) {

            }

            @Override
            public void swiped(int position, int direction) {

            }
        });
    }

    @Override
    public void onPause() {
        ((DrawerActivity) getActivity()).setTitle("");
        listState = list.getLayoutManager().onSaveInstanceState();
        adapter.onStop();
        super.onPause();
    }

    @Override
    protected PresenterLoader.Info<ITopicsView> getLoaderInfo() {
        return new PresenterLoader.Info<ITopicsView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_TOPICS;
            }

            @Override
            public Presenter.Factory<ITopicsView> getFactory() {
                return new Presenter.Factory<ITopicsView>() {
                    @Override
                    public <P extends Presenter<ITopicsView>> P create() {
                        return (P) new TopicsPresenter();
                    }
                };
            }

            @Override
            public ITopicsView getView() {
                return TopicsFragment.this;
            }
        };
    }

    static class State {

        static final String ARGUMENTS_CATEGORY = TopicsFragment.class.getCanonicalName() + ".CATEGORY";

        static final String STATE_LIST = TopicsFragment.class.getCanonicalName() + ".LIST_STATE";
        static final String STATE_ADAPTER = TopicsFragment.class.getCanonicalName() + ".ADAPTER_STATE";

        static Parcelable listState = null;
    }
}
