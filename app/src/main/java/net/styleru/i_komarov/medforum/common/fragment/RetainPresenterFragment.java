package net.styleru.i_komarov.medforum.common.fragment;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;

/**
 * Created by i_komarov on 29.11.16.
 */

public abstract class RetainPresenterFragment<V, P extends Presenter<V>> extends BaseFragment implements LoaderManager.LoaderCallbacks<P> {

    private PresenterLoader.Info<V> info = getLoaderInfo();
    protected P presenter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(info.getID(), null, this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onStart();
        presenter.bindView(info.getView());
    }

    @Override
    public void onPause() {
        presenter.unbindView();
        presenter.onStop();
        super.onPause();
    }

    @Override
    public Loader<P> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(getActivity().getApplicationContext(), info.getFactory());
    }

    @Override
    public void onLoadFinished(Loader<P> loader, P presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onLoaderReset(Loader<P> loader) {
        this.presenter = null;
    }

    protected abstract PresenterLoader.Info<V> getLoaderInfo();
}
