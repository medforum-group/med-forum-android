package net.styleru.i_komarov.medforum.view.main.topics;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 09.12.16.
 */

public class TopicViewEntity implements Parcelable {

    private String id;
    private String title;
    private int answersCount;
    private String lastUpdate;
    private String authorPhoto;
    private String authorFirstName;
    private String authorLastName;
    private Boolean isCompleted;

    public TopicViewEntity(Parcel in) {
        id = in.readString();
        title = in.readString();
        answersCount = in.readInt();
        lastUpdate = in.readString();
        authorPhoto = in.readString();
        authorFirstName = in.readString();
        authorLastName = in.readString();
        isCompleted = in.readByte() == 1;
    }

    public TopicViewEntity(String id, String title, int answersCount, String lastUpdate, String authorPhoto, String authorFirstName, String authorLastName, Boolean isCompleted) {
        this.id = id;
        this.answersCount = answersCount;
        this.lastUpdate = lastUpdate;
        this.title = title;
        this.authorPhoto = authorPhoto;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.isCompleted = isCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(int answersCount) {
        this.answersCount = answersCount;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorPhoto() {
        return authorPhoto;
    }

    public void setAuthorPhoto(String authorPhoto) {
        this.authorPhoto = authorPhoto;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeInt(answersCount);
        dest.writeString(lastUpdate);
        dest.writeString(authorPhoto);
        dest.writeString(authorFirstName);
        dest.writeString(authorLastName);
        dest.writeByte(isCompleted ? (byte) 1 : (byte) 0);
    }

    public static final Parcelable.Creator<TopicViewEntity> CREATOR = new Creator<TopicViewEntity>() {
        @Override
        public TopicViewEntity createFromParcel(Parcel in) {
            return new TopicViewEntity(in);
        }

        @Override
        public TopicViewEntity[] newArray(int size) {
            return new TopicViewEntity[0];
        }
    };
}
