package net.styleru.i_komarov.medforum.common.view;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 29.11.16.
 */

public interface IErrorInteract {

    void showError(@StringRes int erorr);

    void showError(String error);
}
