package net.styleru.i_komarov.medforum.view.main.topic;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.model.datetime.DateUtils;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

import java.text.ParseException;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 10.12.16.
 */

public class CommentViewHolder extends BindableViewHolder<CommentViewEntity, BindableViewHolder.ActionListener<CommentViewEntity>> {

    private AppCompatTextView answerTimeHolder;
    private AppCompatTextView contentHolder;
    private AppCompatTextView authorNameHolder;
    private AppCompatTextView authorStatusHolder;
    private CircleImageView authorPhotoHolder;
    private ImageView authorBadgeHolder;

    private Drawable expertToggle;
    private String authorNameFormat;
    private String dateTimeFormat;
    private String expertBadge;

    public CommentViewHolder(View itemView) {
        super(itemView);

        answerTimeHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_time);
        contentHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_content);
        authorNameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_author_name);
        authorStatusHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_comment_author_badge);
        authorPhotoHolder = (CircleImageView) itemView.findViewById(R.id.list_item_comment_author_image);
        authorBadgeHolder = (ImageView) itemView.findViewById(R.id.list_item_comment_ic_toggle);

        authorBadgeHolder.setColorFilter(itemView.getResources().getColor(R.color.colorAccentExtra));

        authorNameFormat = itemView.getResources().getString(R.string.format_space_delimiter_2);
        dateTimeFormat = itemView.getResources().getString(R.string.format_date_time);
        expertToggle = itemView.getResources().getDrawable(R.drawable.ic_star_black_24dp);
        expertBadge = itemView.getResources().getString(R.string.author_expert);
    }

    @Override
    public void bind(int position, CommentViewEntity item, ActionListener<CommentViewEntity> listener) {
        super.bind(position, item, listener);
        answerTimeHolder.setText(item.getTime());
        contentHolder.setText(item.getContent());
        authorNameHolder.setText(String.format(authorNameFormat, item.getAuthorFirstName(), item.getAuthorLastName()));
        authorStatusHolder.setText(item != null && item.getExpert() != null && item.getExpert() ? expertBadge : "");

        authorPhotoHolder.setImageDrawable(null);
        authorBadgeHolder.setImageDrawable(null);

        try {
            DateUtils.DisplayDate data = DateUtils.DisplayDate.fromServerDateTime(item.getTime());
            answerTimeHolder.setText(String.format(dateTimeFormat, data.getDisplayDate(), data.getDisplayTime()));
        } catch(Exception e) {

        }

        if(item != null && item.getExpert() != null && item.getExpert()) {
            authorBadgeHolder.setImageDrawable(expertToggle);
            authorStatusHolder.setText(expertBadge);
        } else {
            authorStatusHolder.setText("");
        }

        if(item.getAuthorImage() != null) {
            Glide.with(itemView.getContext())
                    .load(item.getAuthorImage())
                    .fitCenter()
                    .into(authorPhotoHolder);
        }
    }
}
