package net.styleru.i_komarov.medforum.presenter.mapper;

import net.styleru.i_komarov.medforum.model.common.Values;
import net.styleru.i_komarov.medforum.model.api.dto.AnswerServerEntity;
import net.styleru.i_komarov.medforum.view.main.topic.CommentViewEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 03.01.17.
 */

public class AnswersMapper implements Function<List<AnswerServerEntity>, List<CommentViewEntity>> {
    @Override
    public List<CommentViewEntity> apply(List<AnswerServerEntity> answerServerEntities) throws Exception {
        return Flowable.fromIterable(answerServerEntities)
                //TODO: add imageUrl as it comes in API
                .map(answer -> new CommentViewEntity(answer.getId(), answer.getContent(), answer.getDate(), answer.getImageUrl(), answer.getFirstName(), answer.getLastName(), answer.getRole() != null && answer.getRole().equals(Values.VALUE_EXPERT)))
                .toList()
                .blockingGet();
    }
}
