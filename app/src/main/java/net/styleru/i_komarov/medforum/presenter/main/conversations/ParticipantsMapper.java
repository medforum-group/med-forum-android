package net.styleru.i_komarov.medforum.presenter.main.conversations;

import net.styleru.i_komarov.medforum.model.layer.dto.LayerParticipantEntity;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ParticipantsMapper implements Function<List<LayerParticipantEntity>, List<ParticipantViewEntity>> {

    @Override
    public List<ParticipantViewEntity> apply(List<LayerParticipantEntity> list) throws Exception {
        return Observable.fromIterable(list)
                .map(new Function<LayerParticipantEntity, ParticipantViewEntity>() {
                    @Override
                    public ParticipantViewEntity apply(LayerParticipantEntity entity) throws Exception {
                        String[] name = entity.getDisplayName().split(" ");
                        return new ParticipantViewEntity(entity.getId(), entity.getPhotoUrl(), name[0], name[1]);
                    }
                })
                .toList()
                .blockingGet();
    }
}
