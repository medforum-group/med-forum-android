package net.styleru.i_komarov.medforum.common.presenter;

/**
 * Created by i_komarov on 29.11.16.
 */

public interface IPresenter<V> {

    public void onStart();

    public void bindView(V view);

    public void unbindView();

    public void onStop();

    public void onDestroy();
}
