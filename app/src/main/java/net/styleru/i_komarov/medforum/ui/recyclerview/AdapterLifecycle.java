package net.styleru.i_komarov.medforum.ui.recyclerview;

/**
 * Created by i_komarov on 29.11.16.
 */

public interface AdapterLifecycle {

    public void onStart();

    public void onStop();
}
