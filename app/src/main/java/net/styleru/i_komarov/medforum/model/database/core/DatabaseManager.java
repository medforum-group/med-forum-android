package net.styleru.i_komarov.medforum.model.database.core;

import android.app.Application;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResults;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.i_komarov.medforum.model.database.entities.answers.AnswerDatabaseEntity;
import net.styleru.i_komarov.medforum.model.database.entities.answers.AnswerDatabaseEntityStorIOSQLiteDeleteResolver;
import net.styleru.i_komarov.medforum.model.database.entities.answers.AnswerDatabaseEntityStorIOSQLiteGetResolver;
import net.styleru.i_komarov.medforum.model.database.entities.answers.AnswerDatabaseEntityStorIOSQLitePutResolver;
import net.styleru.i_komarov.medforum.model.database.entities.categories.CategoryDatabaseEntity;
import net.styleru.i_komarov.medforum.model.database.entities.categories.CategoryDatabaseEntityStorIOSQLiteDeleteResolver;
import net.styleru.i_komarov.medforum.model.database.entities.categories.CategoryDatabaseEntityStorIOSQLiteGetResolver;
import net.styleru.i_komarov.medforum.model.database.entities.categories.CategoryDatabaseEntityStorIOSQLitePutResolver;
import net.styleru.i_komarov.medforum.model.database.entities.questions.QuestionDatabaseEntity;
import net.styleru.i_komarov.medforum.model.database.entities.questions.QuestionDatabaseEntityStorIOSQLiteDeleteResolver;
import net.styleru.i_komarov.medforum.model.database.entities.questions.QuestionDatabaseEntityStorIOSQLiteGetResolver;
import net.styleru.i_komarov.medforum.model.database.entities.questions.QuestionDatabaseEntityStorIOSQLitePutResolver;
import net.styleru.i_komarov.medforum.model.database.mapping.answers.AnswersTableConfig;
import net.styleru.i_komarov.medforum.model.database.mapping.categories.CategoriesTableConfig;
import net.styleru.i_komarov.medforum.model.database.mapping.questions.QuestionsTableConfig;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 14.01.17.
 */

public class DatabaseManager {

    private static DatabaseManager instance;

    private StorIOSQLite storIOSQLite;

    private DatabaseManager(Application app) {
        storIOSQLite = DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(new TempDatabaseOpenHelper(app))
                .addTypeMapping(
                        CategoryDatabaseEntity.class,
                        SQLiteTypeMapping.<CategoryDatabaseEntity>builder()
                            .putResolver(new CategoryDatabaseEntityStorIOSQLitePutResolver())
                            .getResolver(new CategoryDatabaseEntityStorIOSQLiteGetResolver())
                            .deleteResolver(new CategoryDatabaseEntityStorIOSQLiteDeleteResolver())
                        .build()
                )
                .addTypeMapping(
                        QuestionDatabaseEntity.class,
                        SQLiteTypeMapping.<QuestionDatabaseEntity>builder()
                            .putResolver(new QuestionDatabaseEntityStorIOSQLitePutResolver())
                            .getResolver(new QuestionDatabaseEntityStorIOSQLiteGetResolver())
                            .deleteResolver(new QuestionDatabaseEntityStorIOSQLiteDeleteResolver())
                        .build()
                )
                .addTypeMapping(
                        AnswerDatabaseEntity.class,
                        SQLiteTypeMapping.<AnswerDatabaseEntity>builder()
                            .putResolver(new AnswerDatabaseEntityStorIOSQLitePutResolver())
                            .getResolver(new AnswerDatabaseEntityStorIOSQLiteGetResolver())
                            .deleteResolver(new AnswerDatabaseEntityStorIOSQLiteDeleteResolver())
                        .build()
                )
                .build();
    }

    public static void onApplicationCreated(Application app) {
        instance = new DatabaseManager(app);
    }

    public static DatabaseManager getInstance() {
        if(instance == null) {
            throw new RuntimeException("DatabaseManager.onApplicationCreated(Application) was not called!");
        }

        return instance;
    }

    public Observable<CategoryDatabaseEntity> getCategory(Long id) {
        return wrapGetResult(
                storIOSQLite.get()
                        .listOfObjects(CategoryDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(CategoriesTableConfig.TABLE)
                                        .where(CategoriesTableConfig.COLUMN_ID + " = " + id)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<CategoryDatabaseEntity> getCategory(String categoryID) {
        return wrapGetResult(
                storIOSQLite.get()
                    .listOfObjects(CategoryDatabaseEntity.class)
                    .withQuery(
                            Query.builder()
                                    .table(CategoriesTableConfig.TABLE)
                                    .where(CategoriesTableConfig.COLUMN_SERVER_ID + " = " + categoryID)
                                    .build()
                    )
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<List<CategoryDatabaseEntity>> getCategories(int offset, int limit) {
        return wrapGetResults(
                storIOSQLite.get()
                    .listOfObjects(CategoryDatabaseEntity.class)
                    .withQuery(
                            Query.builder()
                                    .table(CategoriesTableConfig.TABLE)
                                    .limit(offset, limit)
                                    .build()
                    )
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<List<CategoryDatabaseEntity>> getCategories(int offset, int limit, String whereCondition) {
        return wrapGetResults(
                storIOSQLite.get()
                        .listOfObjects(CategoryDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(CategoriesTableConfig.TABLE)
                                        .where(whereCondition)
                                        .limit(offset, limit)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResult> putCategory(CategoryDatabaseEntity entity) {
        return wrapPut(
                storIOSQLite.put()
                    .object(entity)
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<PutResult> putCategory(Observable<CategoryDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<CategoryDatabaseEntity, ObservableSource<PutResult>>() {
            @Override
            public ObservableSource<PutResult> apply(CategoryDatabaseEntity entity) throws Exception {
                return putCategory(entity).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<PutResults<CategoryDatabaseEntity>> putCategories(List<CategoryDatabaseEntity> entities) {
        return wrapPut(
                storIOSQLite.put()
                    .objects(entities)
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<PutResults<CategoryDatabaseEntity>> putCategories(Observable<List<CategoryDatabaseEntity>> entitiesObservable) {
        return entitiesObservable.flatMap(new Function<List<CategoryDatabaseEntity>, ObservableSource<PutResults<CategoryDatabaseEntity>>>() {
            @Override
            public ObservableSource<PutResults<CategoryDatabaseEntity>> apply(List<CategoryDatabaseEntity> entities) throws Exception {
                return putCategories(entities).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<DeleteResult> deleteCategory(CategoryDatabaseEntity entity) {
        return wrapDelete(
                storIOSQLite.delete()
                    .object(entity)
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<DeleteResult> deleteCategory(Observable<CategoryDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<CategoryDatabaseEntity, ObservableSource<DeleteResult>>() {
            @Override
            public ObservableSource<DeleteResult> apply(CategoryDatabaseEntity entity) throws Exception {
                return deleteCategory(entity).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<QuestionDatabaseEntity> getQuestion(Long id) {
        return wrapGetResult(
                storIOSQLite.get()
                    .listOfObjects(QuestionDatabaseEntity.class)
                    .withQuery(
                            Query.builder()
                                    .table(QuestionsTableConfig.TABLE)
                                    .where(QuestionsTableConfig.COLUMN_ID + " = " + id)
                                    .build()
                    )
                    .prepare()
                    .executeAsBlocking()
        );
    }

    public Observable<QuestionDatabaseEntity> getQuestion(String questionID) {
        return wrapGetResult(
                storIOSQLite.get()
                        .listOfObjects(QuestionDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(QuestionsTableConfig.TABLE)
                                        .where(QuestionsTableConfig.COLUMN_SERVER_ID + " = " + questionID)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<List<QuestionDatabaseEntity>> getQuestions(int offset, int limit) {
        return wrapGetResults(
                storIOSQLite.get()
                        .listOfObjects(QuestionDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(QuestionsTableConfig.TABLE)
                                        .limit(offset, limit)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<List<QuestionDatabaseEntity>> getQuestions(int offset, int limit, String whereCondition) {
        return wrapGetResults(
                storIOSQLite.get()
                        .listOfObjects(QuestionDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(QuestionsTableConfig.TABLE)
                                        .where(whereCondition)
                                        .limit(offset, limit)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResult> putQuestion(QuestionDatabaseEntity entity) {
        return wrapPut(
                storIOSQLite.put()
                        .object(entity)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResult> putQuestion(Observable<QuestionDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<QuestionDatabaseEntity, ObservableSource<PutResult>>() {
            @Override
            public ObservableSource<PutResult> apply(QuestionDatabaseEntity entities) throws Exception {
                return putQuestion(entities).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<PutResults<QuestionDatabaseEntity>> putQuestions(List<QuestionDatabaseEntity> entities) {
        return wrapPut(
                storIOSQLite.put()
                        .objects(entities)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResults<QuestionDatabaseEntity>> putQuestions(Observable<List<QuestionDatabaseEntity>> entitiesObservable) {
        return entitiesObservable.flatMap(new Function<List<QuestionDatabaseEntity>, ObservableSource<PutResults<QuestionDatabaseEntity>>>() {
            @Override
            public ObservableSource<PutResults<QuestionDatabaseEntity>> apply(List<QuestionDatabaseEntity> entities) throws Exception {
                return putQuestions(entities).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<DeleteResult> deleteQuestion(QuestionDatabaseEntity entity) {
        return wrapDelete(
                storIOSQLite.delete()
                        .object(entity)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<DeleteResult> deleteQuestion(Observable<QuestionDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<QuestionDatabaseEntity, ObservableSource<DeleteResult>>() {
            @Override
            public ObservableSource<DeleteResult> apply(QuestionDatabaseEntity entity) throws Exception {
                return deleteQuestion(entity).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<AnswerDatabaseEntity> getAnswer(Long id) {
        return wrapGetResult(
                storIOSQLite.get()
                        .listOfObjects(AnswerDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(AnswersTableConfig.TABLE)
                                        .where(AnswersTableConfig.COLUMN_ID + " = " + id)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<AnswerDatabaseEntity> getAnswer(String questionID) {
        return wrapGetResult(
                storIOSQLite.get()
                        .listOfObjects(AnswerDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(AnswersTableConfig.TABLE)
                                        .where(AnswersTableConfig.COLUMN_SERVER_ID + " = " + questionID)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<List<AnswerDatabaseEntity>> getAnswers(int offset, int limit) {
        return wrapGetResults(
                storIOSQLite.get()
                        .listOfObjects(AnswerDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(AnswersTableConfig.TABLE)
                                        .limit(offset, limit)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<List<AnswerDatabaseEntity>> getAnswers(int offset, int limit, String whereCondition) {
        return wrapGetResults(
                storIOSQLite.get()
                        .listOfObjects(AnswerDatabaseEntity.class)
                        .withQuery(
                                Query.builder()
                                        .table(AnswersTableConfig.TABLE)
                                        .where(whereCondition)
                                        .limit(offset, limit)
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResult> putAnswer(AnswerDatabaseEntity entity) {
        return wrapPut(
                storIOSQLite.put()
                        .object(entity)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResult> putAnswer(Observable<AnswerDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<AnswerDatabaseEntity, ObservableSource<PutResult>>() {
            @Override
            public ObservableSource<PutResult> apply(AnswerDatabaseEntity entities) throws Exception {
                return putAnswer(entities).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<PutResults<AnswerDatabaseEntity>> putAnswers(List<AnswerDatabaseEntity> entities) {
        return wrapPut(
                storIOSQLite.put()
                        .objects(entities)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<PutResults<AnswerDatabaseEntity>> putAnswers(Observable<List<AnswerDatabaseEntity>> entitiesObservable) {
        return entitiesObservable.flatMap(new Function<List<AnswerDatabaseEntity>, ObservableSource<PutResults<AnswerDatabaseEntity>>>() {
            @Override
            public ObservableSource<PutResults<AnswerDatabaseEntity>> apply(List<AnswerDatabaseEntity> entities) throws Exception {
                return putAnswers(entities).subscribeOn(Schedulers.computation());
            }
        });
    }

    public Observable<DeleteResult> deleteAnswer(AnswerDatabaseEntity entity) {
        return wrapDelete(
                storIOSQLite.delete()
                        .object(entity)
                        .prepare()
                        .executeAsBlocking()
        );
    }

    public Observable<DeleteResult> deleteAnswer(Observable<AnswerDatabaseEntity> entityObservable) {
        return entityObservable.flatMap(new Function<AnswerDatabaseEntity, ObservableSource<DeleteResult>>() {
            @Override
            public ObservableSource<DeleteResult> apply(AnswerDatabaseEntity entity) throws Exception {
                return deleteAnswer(entity).subscribeOn(Schedulers.computation());
            }
        });
    }

    private <T> Observable<T> wrapGetResult(T result) {
        return Observable.create(
                new ObservableOnSubscribe<T>() {
                    @Override
                    public void subscribe(ObservableEmitter<T> e) throws Exception {
                        e.onNext(result);
                    }
                }
        );
    }

    private <T> Observable<T> wrapGetResult(List<T> results) {
        return Observable.fromIterable(results);
    }

    private <T> Observable<List<T>> wrapGetResults(List<T> results) {
        return Observable.create(
                new ObservableOnSubscribe<List<T>>() {
                    @Override
                    public void subscribe(ObservableEmitter<List<T>> e) throws Exception {
                        e.onNext(results);
                    }
                }
        );
    }

    private Observable<PutResult> wrapPut(PutResult result) {
        return Observable.create(
                new ObservableOnSubscribe<PutResult>() {
                    @Override
                    public void subscribe(ObservableEmitter<PutResult> e) throws Exception {
                        e.onNext(result);
                    }
                }
        );
    }

    private <T> Observable<PutResults<T>> wrapPut(PutResults<T> results) {
        return Observable.create(
                new ObservableOnSubscribe<PutResults<T>>() {
                    @Override
                    public void subscribe(ObservableEmitter<PutResults<T>> e) throws Exception {
                        e.onNext(results);
                    }
                }
        );
    }

    private Observable<DeleteResult> wrapDelete(DeleteResult result) {
        return Observable.create(
                new ObservableOnSubscribe<DeleteResult>() {
                    @Override
                    public void subscribe(ObservableEmitter<DeleteResult> e) throws Exception {
                        e.onNext(result);
                    }
                }
        );
    }

    private <T> Observable<DeleteResults<T>> wrapDelete(DeleteResults<T> results) {
        return Observable.create(
                new ObservableOnSubscribe<DeleteResults<T>>() {
                    @Override
                    public void subscribe(ObservableEmitter<DeleteResults<T>> e) throws Exception {
                        e.onNext(results);
                    }
                }
        );
    }
}
