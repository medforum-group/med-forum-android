package net.styleru.i_komarov.medforum.model.layer.ws.dto;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSResponse {

    @SerializedName(FIELD_TYPE)
    private String type;
    @SerializedName(FIELD_TIMESTAMP)
    private String timeStamp;
    @SerializedName(FIELD_BODY)
    private JsonObject body;
    @SerializedName(FIELD_COUNTER)
    private String counter;

    public WSResponse(String type, String timeStamp, JsonObject body, String counter) {
        this.type = type;
        this.timeStamp = timeStamp;
        this.body = body;
        this.counter = counter;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public JsonObject getBody() {
        return body;
    }

    public void setBody(JsonObject body) {
        this.body = body;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }
}
