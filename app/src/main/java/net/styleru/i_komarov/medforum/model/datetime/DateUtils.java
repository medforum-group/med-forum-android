package net.styleru.i_komarov.medforum.model.datetime;

import android.app.Application;
import android.content.Context;

import net.styleru.i_komarov.medforum.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by i_komarov on 10.01.17.
 */

public class DateUtils {

    private static final String PATTERN_DATE = "yyyy-MM-dd";
    private static final String PATTERN_TIME = "HH:mm:ss.SSS'Z'";

    private static String DATE_TODAY;
    private static String DATE_YESTERDAY;

    private static final String PATTERN_DATE_TIME = PATTERN_DATE + "'T'" + PATTERN_TIME;

    public static void init(Context context) {
        DATE_TODAY = context.getResources().getString(R.string.date_today);
        DATE_YESTERDAY = context.getResources().getString(R.string.date_yesterday);
    }

    private static Calendar parseServerDateTime(String dateTime) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        Date date = new SimpleDateFormat(PATTERN_DATE_TIME, Locale.ENGLISH).parse(dateTime);
        calendar.setTime(date);
        return calendar;
    }

    public static String getCurrentDateTime(Application app) throws ParseException {
        DisplayDate date = DisplayDate.fromServerDateTime(new SimpleDateFormat(PATTERN_DATE_TIME).format(new Date(System.currentTimeMillis())));
        return String.format(app.getResources().getString(R.string.format_date_today), date.getDisplayTime());
    }

    public static final class DisplayDate {

        private Calendar date;

        private DisplayDate(Calendar date) {
            this.date = date;
        }

        public static DisplayDate fromServerDateTime(String dateTime) throws ParseException {
            return new DisplayDate(parseServerDateTime(dateTime));
        }

        public String getDisplayDate() {
            Calendar currentDate = Calendar.getInstance();
            Calendar yesterdayDate = Calendar.getInstance();
            yesterdayDate.add(Calendar.DAY_OF_YEAR, -1);
            if(currentDate.get(Calendar.YEAR) == date.get(Calendar.YEAR) && currentDate.get(Calendar.MONTH) == date.get(Calendar.MONTH) && currentDate.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH)) {
                return DATE_TODAY;
            } else if(yesterdayDate.get(Calendar.YEAR) == date.get(Calendar.YEAR) && yesterdayDate.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)) {
                return DATE_YESTERDAY;
            } else {
                return String.valueOf(completeDatePart(String.valueOf(date.get(Calendar.DAY_OF_MONTH))) + "." + completeDatePart(String.valueOf(date.get(Calendar.MONTH) + 1)) + "." + String.valueOf(date.get(Calendar.YEAR)));
            }
        }

        private String completeDatePart(String part) {
            return part.length() < 2 ? "0" + part : part;
        }

        public String getDisplayTime() {
            return (date.get(Calendar.HOUR_OF_DAY) < 10 ? String.valueOf("0" + date.get(Calendar.HOUR_OF_DAY)) : date.get(Calendar.HOUR_OF_DAY)) + ":" + (date.get(Calendar.MINUTE) < 10 ? String.valueOf("0" + date.get(Calendar.MINUTE)) : date.get(Calendar.MINUTE)) + ":" + (date.get(Calendar.SECOND) < 10 ? String.valueOf("0" + date.get(Calendar.SECOND)) : date.get(Calendar.SECOND));
        }
    }
}
