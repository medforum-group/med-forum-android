package net.styleru.i_komarov.medforum.presenter.mapper;

import net.styleru.i_komarov.medforum.model.common.Values;
import net.styleru.i_komarov.medforum.model.api.dto.QuestionServerEntity;
import net.styleru.i_komarov.medforum.view.main.topics.TopicViewEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 03.01.17.
 */

public class QuestionsMapper implements Function<List<QuestionServerEntity>, List<TopicViewEntity>> {
    @Override
    public List<TopicViewEntity> apply(List<QuestionServerEntity> questionServerEntities) throws Exception {
        return Flowable.fromIterable(questionServerEntities)
                .map(question -> new TopicViewEntity(question.getId(), question.getTitle(), question.getCount(), question.getLastUpdate(), question.getImageUrl(), question.getFirstName(), question.getLastName(), question.getRole() != null && question.getRole().equals(Values.VALUE_EXPERT)))
                .toList()
                .blockingGet();
    }
}
