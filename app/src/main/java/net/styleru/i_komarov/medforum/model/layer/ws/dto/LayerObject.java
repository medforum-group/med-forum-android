package net.styleru.i_komarov.medforum.model.layer.ws.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 23.01.17.
 */

public class LayerObject {

    @SerializedName(FIELD_TYPE)
    private String type;
    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_URL)
    private String url;

    public LayerObject(String type, String id, String url) {
        this.type = type;
        this.id = id;
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "LayerObject{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
