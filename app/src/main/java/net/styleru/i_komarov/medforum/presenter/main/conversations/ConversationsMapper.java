package net.styleru.i_komarov.medforum.presenter.main.conversations;

import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerConversationEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerParticipantEntity;
import net.styleru.i_komarov.medforum.view.main.conversations.ConversationViewEntity;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationsMapper implements Function<List<LayerConversationEntity>, List<ConversationViewEntity>> {

    private String userID;

    public ConversationsMapper(String userID) {
        this.userID = userID;
    }

    @Override
    public List<ConversationViewEntity> apply(List<LayerConversationEntity> list) throws Exception {
        return Observable.fromIterable(list)
                .map(new Function<LayerConversationEntity, ConversationViewEntity>() {
                    @Override
                    public ConversationViewEntity apply(LayerConversationEntity entity) throws Exception {
                        return new ConversationViewEntity(
                                    entity.getId(),
                                    entity.getPreview(),
                                    entity.getLastMessage() != null ? entity.getLastMessage().getSentAt() : entity.getCreatedAt(),
                                    entity.getUnreadCount(),
                                    entity.getParticipants() != null ?
                                            Observable.just(entity.getParticipants()).concatMap(
                                                    new Function<List<LayerParticipantEntity>, ObservableSource<LayerParticipantEntity>>() {
                                                        @Override
                                                        public ObservableSource<LayerParticipantEntity> apply(List<LayerParticipantEntity> layerParticipantEntities) throws Exception {
                                                            return Observable.fromIterable(layerParticipantEntities);
                                                        }
                                                    }).filter(new Predicate<LayerParticipantEntity>() {
                                                        @Override
                                                        public boolean test(LayerParticipantEntity participant) throws Exception {
                                                            return ! participant.getUserID().equals(userID);
                                                        }
                                                    })
                                                     .toList()
                                                    .map(new ParticipantsMapper()).blockingGet() : new ArrayList<>()
                        );
                    }
                })
                .toList()
                .blockingGet();
    }
}
