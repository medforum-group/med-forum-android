package net.styleru.i_komarov.medforum.view.main.drawer;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.activity.PresenterActivity;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.drawer.DrawerPresenter;
import net.styleru.i_komarov.medforum.view.authorization.main.AuthorizationActivity;
import net.styleru.i_komarov.medforum.view.main.categories.CategoriesFragment;
import net.styleru.i_komarov.medforum.view.main.conversations.ConversationsFragment;
import net.styleru.i_komarov.medforum.view.main.dialog.DialogBuilder;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantsFragment;
import net.styleru.i_komarov.rxjava2_binding.navigation_view.main.RxNavigationView;
import net.styleru.i_komarov.rxjava2_binding.toolbar.main.RxToolbar;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class DrawerActivity extends PresenterActivity<IDrawerView, DrawerPresenter> implements IDrawerView {

    private boolean isAuthenticated = false;

    private DialogBuilder dialogBuilder;
    private MaterialDialog errorDialog;
    private MaterialDialog progressDialog;
    private MaterialDialog messageDialog;

    private ActionBarDrawerToggle drawerToggle;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 1) {
            drawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            drawerToggle.setDrawerIndicatorEnabled(true);
        }
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

        if(PreferencesManager.getInstance().loadUserID().equals("") || PreferencesManager.getInstance().loadToken().equals("")) {
            isAuthenticated = false;
            startActivityForResult(new Intent(this, AuthorizationActivity.class), AuthorizationActivity.REQUEST_CODE_AUTHORIZATION);
        } else {
            isAuthenticated = true;
        }

        if(getFragmentManager().findFragmentById(R.id.activity_drawer_screen_primary) == null) {
            getFragmentManager().beginTransaction().replace(R.id.activity_drawer_screen_primary, CategoriesFragment.newInstance()).commit();
        }

        dialogBuilder = new DialogBuilder(this);
        errorDialog = dialogBuilder.buildErrorDialog();
        progressDialog = dialogBuilder.buildConnectingDialog();
        messageDialog = dialogBuilder.buildNotificationDialog();

        bindUI();
    }

    @Override
    public void onResume() {
        super.onResume();
        drawerToggle.setDrawerIndicatorEnabled(getFragmentManager().getBackStackEntryCount() == 0);
        presenter.onUserAuthenticated();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void showConnectingDialog() {
        hideAllDialogs();
        progressDialog.setContent(R.string.dialog_progress_content_connecting);
        progressDialog.show();
    }

    @Override
    public void hideConnectingDialog() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showConnectionError() {
        hideAllDialogs();
        errorDialog.setContent(R.string.dialog_error_content_network_connection_lost);
        errorDialog.show();
    }

    @Override
    public void showConenctionError(@StringRes int message) {
        hideAllDialogs();
        hideAllDialogs();
        errorDialog.setContent(message);
        errorDialog.show();
    }

    @Override
    public void showConnectionError(String message) {
        hideAllDialogs();
        errorDialog.setContent(message);
        errorDialog.show();
    }

    @Override
    public void showError(@StringRes int error) {
        hideAllDialogs();
        errorDialog.setContent(error);
        errorDialog.show();
    }

    @Override
    public void showError(String error) {
        hideAllDialogs();
        errorDialog.setContent(error);
        errorDialog.show();
    }

    @Override
    public void showMessage(String title, String message) {
        hideAllDialogs();
        messageDialog.setTitle(title);
        messageDialog.setContent(message);
        messageDialog.show();
    }

    @Override
    public void showMessage(@StringRes int title, String message) {
        hideAllDialogs();
        messageDialog.setTitle(title);
        messageDialog.setContent(message);
        messageDialog.show();
    }

    @Override
    public void showMessage(@StringRes int title, @StringRes int message) {
        hideAllDialogs();
        messageDialog.setTitle(title);
        messageDialog.setContent(message);
        messageDialog.show();
    }

    @Override
    public void onNavigationClicked() {
        if(drawerToggle.isDrawerIndicatorEnabled()) {
            drawerLayout.openDrawer(GravityCompat.START);
        } else {
            onBackPressed();
        }
    }

    @Override
    public void onNavigationViewItemSelected(MenuItem item) {
        Fragment fragment = null;
        final int id = item.getItemId();
        switch(id) {
            case R.id.navigation_menu_item_chats : {
                fragment = ConversationsFragment.newInstance();
                break;
            }
            case R.id.navigation_menu_item_participants : {
                fragment = ParticipantsFragment.newInstance();
                break;
            }
            case R.id.navigation_menu_item_preferences : {
                break;
            }
            case R.id.navigation_menu_item_categories: {
                fragment = CategoriesFragment.newInstance();
                break;
            }
        }

        if(fragment != null) {
            while(getFragmentManager().getBackStackEntryCount() > 0) {
                getFragmentManager().popBackStackImmediate();
            }
            getFragmentManager().beginTransaction().replace(R.id.activity_drawer_screen_primary, fragment).commit();
        }

        drawerLayout.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onAddFragmentInitiated(boolean addToBackStack, Fragment fragment) {
        if(addToBackStack || getFragmentManager().getBackStackEntryCount() > 0) {
            drawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            drawerToggle.setDrawerIndicatorEnabled(true);
        }
        FragmentTransaction transaction = getFragmentManager().beginTransaction().replace(R.id.activity_drawer_screen_primary, fragment);
        if(addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    public Observable<Integer> getToolbarNavigationClickEventChannel() {
        return RxToolbar.navigationClicks(toolbar);
    }

    @Override
    public Observable<MenuItem> getNavigationItemClickEventChannel() {
        return RxNavigationView.itemSelections(navigationView);
    }

    public void setTitle(@StringRes int titleRes) {
        getSupportActionBar().setTitle(titleRes);
    }

    public void setTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    public void setSubtitle(@StringRes int subtitleRes) {
        getSupportActionBar().setSubtitle(subtitleRes);
    }

    public void setSubtitle(String subtitle) {
        getSupportActionBar().setSubtitle(subtitle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == AuthorizationActivity.REQUEST_CODE_AUTHORIZATION) {
            if(resultCode == RESULT_OK) {
                Log.d("DrawerActivity", "user authentication succeeded");
                isAuthenticated = true;
                presenter.onUserAuthenticated();
                //TODO: authorization succeeded
            } else {
                Log.d("DrawerActivity", "user authentication failed");
                //TODO: authorization failed somehow
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected PresenterLoader.Info<IDrawerView> getLoaderInfo() {
        return new PresenterLoader.Info<IDrawerView>() {
            @Override
            public int getID() {
                return LOADER_DRAWER;
            }

            @Override
            public Presenter.Factory<IDrawerView> getFactory() {
                return new Presenter.Factory<IDrawerView>() {
                    @Override
                    public <P extends Presenter<IDrawerView>> P create() {
                        return (P) new DrawerPresenter();
                    }
                };
            }

            @Override
            public IDrawerView getView() {
                return DrawerActivity.this;
            }
        };
    }

    private void bindUI() {
        drawerLayout   = (DrawerLayout) findViewById(R.id.activity_drawer_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.activity_drawer_navigation_view);
        toolbar        = (Toolbar) findViewById(R.id.activity_drawer_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");

        setupDrawer();
    }

    private void setupDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        drawerLayout.addDrawerListener(drawerToggle);
    }

    private void hideAllDialogs() {
        if(errorDialog != null && errorDialog.isShowing()) {
            errorDialog.dismiss();
        }

        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if(messageDialog != null && messageDialog.isShowing()) {
            messageDialog.dismiss();
        }
    }
}
