package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.FIELD_SESSION_TOKEN;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerSessionToken {

    @SerializedName(FIELD_SESSION_TOKEN)
    private String sessionToken;

    public LayerSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public String toString() {
        return "LayerSessionToken{" +
                "sessionToken='" + sessionToken + '\'' +
                '}';
    }
}
