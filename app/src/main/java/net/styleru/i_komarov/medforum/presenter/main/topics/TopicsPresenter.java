package net.styleru.i_komarov.medforum.presenter.main.topics;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.QuestionsProxy;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.QuestionServerEntity;
import net.styleru.i_komarov.medforum.presenter.mapper.QuestionsMapper;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.topics.ITopicsView;
import net.styleru.i_komarov.medforum.view.main.topics.TopicViewEntity;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by i_komarov on 09.12.16.
 */

public class TopicsPresenter extends Presenter<ITopicsView> {

    private String categoryID = "";
    private String currentQuery = "";

    @Override
    public void onStart() {

    }

    public void init(String categoryID) {
        this.categoryID = categoryID;
    }

    @Override
    public void bindView(ITopicsView view) {
        super.bindView(view);
        subscribeOnSearchViewChangeEventChannel(view.getSearchViewChangeEventObservable().debounce(500, TimeUnit.MILLISECONDS));
        subscribeOnCreateTopicButtonClicksObservable(view.getCreateTopicButtonClicksObservable());
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onDestroy() {

    }

    public Flowable<List<TopicViewEntity>> provideDataChannel(Flowable<State> listStateChannel) {
        return listStateChannel.flatMap(
                new Function<State, Publisher<List<QuestionServerEntity>>>() {
                    @Override
                    public Publisher<List<QuestionServerEntity>> apply(State state) throws Exception {
                        return QuestionsProxy.getInstance().list(categoryID, state.getOffset(), state.getLimit())
                                .subscribeOn(Schedulers.io())
                                .filter(response -> hookHttpCode(response.code()))
                                .filter(response -> response != null && response.body() != null)
                                .map(Response::body)
                                .filter(wrapper -> wrapper != null && wrapper.getBody() != null && wrapper.getBody().size() != 0)
                                .map(ResponseWrapper::getBody);
                    }
                })
                .map(new QuestionsMapper())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void subscribeOnSearchViewChangeEventChannel(Observable<SearchViewChangeEvent> searchViewChangeEventObservable) {
        searchViewChangeEventObservable.subscribe(createSearchViewChangeEventChannelObserver());
    }

    private void subscribeOnCreateTopicButtonClicksObservable(Observable<Integer> createTopicButtonClicksObservable) {
        createTopicButtonClicksObservable.subscribe(createCreateTopicButtonClicksObserver());
    }

    private DisposableObserver<SearchViewChangeEvent> createSearchViewChangeEventChannelObserver() {
        DisposableObserver<SearchViewChangeEvent> searchViewChangeEventDisposableObserver = new DisposableObserver<SearchViewChangeEvent>() {
            @Override
            public void onNext(SearchViewChangeEvent event) {
                if(event.getType() == SearchViewChangeEvent.Type.CHANGE) {
                    currentQuery = event.getQuery();
                    view.refreshAdapter();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(searchViewChangeEventDisposableObserver);

        return searchViewChangeEventDisposableObserver;
    }

    private DisposableObserver<Integer> createCreateTopicButtonClicksObserver() {
        DisposableObserver<Integer> createTopicButtonClicksObserver = new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                view.onCreateTopicButtonClicked();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(createTopicButtonClicksObserver);

        return createTopicButtonClicksObserver;
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode == HTTP_OK) {
            return true;
        } else {
            return false;
        }
    }
}
