package net.styleru.i_komarov.medforum.model.database.mapping.questions;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 14.01.17.
 */

public class QuestionsTableConfig {

    @NonNull
    public static final String TABLE = "questions";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_SERVER_ID = "SERVER_ID";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_IMAGE_URL = "IMAGE_URL";

    @NonNull
    public static final String COLUMN_ROLE = "ROLE";

    @NonNull
    public static final String COLUMN_CATEGORY_ID = "CATEGORY_ID";

    @NonNull
    public static final String COLUMN_TITLE = "TITLE";

    @NonNull
    public static final String COLUMN_CONTENT = "CONTENT";

    @NonNull
    public static final String COLUMN_LAST_UPDATE = "LAST_UPDATE";

    public static final String COLUMN_ID_WITH_TABLE = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_SERVER_ID_WITH_TABLE = TABLE + "." + COLUMN_SERVER_ID;
    public static final String COLUMN_USER_ID_WITH_TABLE = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_FIRST_NAME_WITH_TABLE = TABLE + "." + COLUMN_FIRST_NAME;
    public static final String COLUMN_LAST_NAME_WITH_TABLE = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_IMAGE_URL_WITH_TABLE = TABLE + "." + COLUMN_IMAGE_URL;
    public static final String COLUMN_ROLE_WITH_TABLE = TABLE + "." + COLUMN_ROLE;
    public static final String COLUMN_CATEGORY_ID_WITH_TABLE = TABLE + "." + COLUMN_CATEGORY_ID;
    public static final String COLUMN_TITLE_WITH_TABLE = TABLE + "." + COLUMN_TITLE;
    public static final String COLUMN_CONTENT_WITH_TABLE = TABLE + "." + COLUMN_CONTENT;
    public static final String COLUMN_LAST_UPDATE_WITH_TABLE = TABLE + "." + COLUMN_LAST_UPDATE;

    @NonNull
    public static Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private QuestionsTableConfig() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY," +
                COLUMN_SERVER_ID + " TEXT UNIQUE," +
                COLUMN_USER_ID + " TEXT," +
                COLUMN_FIRST_NAME + " TEXT," +
                COLUMN_LAST_NAME + " TEXT," +
                COLUMN_IMAGE_URL + " TEXT," +
                COLUMN_ROLE + " TEXT," +
                COLUMN_CATEGORY_ID + " TEXT," +
                COLUMN_TITLE + " TEXT," +
                COLUMN_CONTENT + " TEXT," +
                COLUMN_LAST_UPDATE + " TEXT" +
                ");";
    }
}
