package net.styleru.i_komarov.medforum.view.main.categories;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.categories.CategoriesPresenter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.drawer.DrawerActivity;
import net.styleru.i_komarov.medforum.view.main.drawer.IDrawerView;
import net.styleru.i_komarov.medforum.view.main.topics.TopicsFragment;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;
import net.styleru.i_komarov.rxjava2_binding.search_view.main.RxSearchView;

import io.reactivex.Observable;

import static net.styleru.i_komarov.medforum.view.main.categories.CategoriesFragment.State.STATE_ADAPTER;
import static net.styleru.i_komarov.medforum.view.main.categories.CategoriesFragment.State.STATE_LIST;
import static net.styleru.i_komarov.medforum.view.main.categories.CategoriesFragment.State.listState;

/**
 * Created by i_komarov on 09.12.16.
 */

public class CategoriesFragment extends RetainPresenterFragment<ICategoriesView, CategoriesPresenter> implements ICategoriesView {

    private BindableAdapter<CategoryViewEntity> adapter;

    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView list;
    private SearchView searchView;

    public static Fragment newInstance() {
        Fragment fragment = new CategoriesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_list_swipe_refresh);
        list         = (RecyclerView) view.findViewById(R.id.fragment_list_recycler_view);
        searchView   = (SearchView) view.findViewById(R.id.fragment_list_search_view);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));

        swipeRefresh.setEnabled(true);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.refreshDataset();
                swipeRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable() {
        return RxSearchView.queryChanges(searchView);
    }

    @Override
    public void refreshAdapter() {
        adapter.refreshDataset();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(adapter != null) {
            outState.putParcelable(STATE_ADAPTER, adapter);
        }
        if(list != null && listState != null) {
            outState.putParcelable(STATE_LIST, listState);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_ADAPTER)) {
            adapter = savedInstanceState.getParcelable(STATE_ADAPTER);
        }
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_LIST)) {
            listState = savedInstanceState.getParcelable(STATE_LIST);
        }
        if(adapter == null) {
            adapter = new BindableAdapter<>(R.layout.list_item_category, CategoryViewHolder.class);
        }
        list.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DrawerActivity) getActivity()).setTitle(getActivity().getApplicationContext().getResources().getString(R.string.title_categories));
        adapter.onStart();
        adapter.subscribeToSourceChannel(presenter.provideDataChannel(adapter.getStateObservable(list)));
        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
            listState = null;
        }
        adapter.setActionListener(
                new BindableViewHolder.ActionListener<CategoryViewEntity>() {
                    @Override
                    public void clicked(int position, CategoryViewEntity item) {
                        ((IDrawerView) getActivity()).onAddFragmentInitiated(true, TopicsFragment.newInstance(item.getId()));
                    }

                    @Override
                    public void longClicked(int position, CategoryViewEntity item) {

                    }

                    @Override
                    public void swiped(int position, int direction) {

                    }
                }
        );
    }

    @Override
    public void onPause() {
        ((DrawerActivity) getActivity()).setTitle("");
        listState = list.getLayoutManager().onSaveInstanceState();
        adapter.onStop();
        super.onPause();
    }

    @Override
    protected PresenterLoader.Info<ICategoriesView> getLoaderInfo() {
        return new PresenterLoader.Info<ICategoriesView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_CATEGORIES;
            }

            @Override
            public Presenter.Factory<ICategoriesView> getFactory() {
                return new Presenter.Factory<ICategoriesView>() {
                    @Override
                    public <P extends Presenter<ICategoriesView>> P create() {
                        return (P) new CategoriesPresenter();
                    }
                };
            }

            @Override
            public ICategoriesView getView() {
                return CategoriesFragment.this;
            }
        };
    }

    static class State {

        static final String STATE_LIST = CategoriesFragment.class.getCanonicalName() + ".LIST_STATE";
        static final String STATE_ADAPTER = CategoriesFragment.class.getCanonicalName() + ".ADAPTER_STATE";

        static Parcelable listState = null;
    }
}
