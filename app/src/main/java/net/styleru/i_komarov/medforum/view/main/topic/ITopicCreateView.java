package net.styleru.i_komarov.medforum.view.main.topic;

import net.styleru.i_komarov.medforum.view.main.topics.TopicViewEntity;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 09.01.17.
 */

public interface ITopicCreateView {

    Observable<String[]> getCreateTopicButtonClicksObservable();

    void onTopicCreationSucceeded();

    void onTopicCreationFailed();
}
