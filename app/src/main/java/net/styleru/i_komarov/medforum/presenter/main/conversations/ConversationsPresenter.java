package net.styleru.i_komarov.medforum.presenter.main.conversations;

import android.util.Log;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.layer.dao.ConversationProxy;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerConversationEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerConversationPreferences;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerResponse;
import net.styleru.i_komarov.medforum.view.main.conversations.ConversationViewEntity;
import net.styleru.i_komarov.medforum.view.main.conversations.IConversationsView;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;
import static net.styleru.i_komarov.medforum.model.layer.dao.ConversationProxy.SORTING_LAST_MESSAGE;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationsPresenter extends Presenter<IConversationsView> {

    private static final int UPDATE_LIMIT_DEFAULT = 25;

    private ConversationProxy proxy;
    private String userID;

    public ConversationsPresenter() {
        this.proxy = new ConversationProxy(PreferencesManager.getInstance().loadSessionToken());
        this.userID = PreferencesManager.getInstance().loadUserID();
    }

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IConversationsView view) {
        super.bindView(view);
        view.getCreateConversationButtonClicksObservable().subscribe(createCreateConversationButtonClicksObserver());
        subscribeOnListStateEventChannel(view.getListStateChannel());
        subscribeOnRefreshEventsChannel(view.getRefreshEventsObservable());
    }

    @Override
    public void unbindView() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
        super.unbindView();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    public void createConversation(String userID) {
        if(!userID.equals(this.userID)) {
            proxy.createConversation(new LayerConversationPreferences(Arrays.asList(userID), true))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(createConversationCreationResponseObserver());
        } else {
            try {
                view.onConversationWithSelfCreateAttempt();
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void subscribeOnListStateEventChannel(Observable<LayerListState> stateChannel) {
        stateChannel.flatMap(
                new Function<LayerListState, ObservableSource<List<LayerConversationEntity>>>() {
                    @Override
                    public ObservableSource<List<LayerConversationEntity>> apply(LayerListState state) throws Exception {
                        return proxy.conversations(SORTING_LAST_MESSAGE, view.getConversationsManager().lastUri(), state.getLimit())
                                .subscribeOn(Schedulers.io())
                                .map(new Function<Response<List<LayerConversationEntity>>, List<LayerConversationEntity>>() {
                                    @Override
                                    public List<LayerConversationEntity> apply(Response<List<LayerConversationEntity>> listResponse) throws Exception {
                                        return listResponse.body();
                                    }
                                });
                    }
                })
                .map(new ConversationsMapper(userID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createConversationsLoadedEventsObserver());
    }

    private void subscribeOnRefreshEventsChannel(Observable<Integer> onRefresh) {
        onRefresh.flatMap(
                new Function<Integer, ObservableSource<List<LayerConversationEntity>>>() {
                @Override
                public ObservableSource<List<LayerConversationEntity>> apply(Integer integer) throws Exception {
                    return proxy.conversations(SORTING_LAST_MESSAGE, null, UPDATE_LIMIT_DEFAULT)
                            .subscribeOn(Schedulers.io())
                            .filter(new Predicate<Response<List<LayerConversationEntity>>>() {
                                @Override
                                public boolean test(Response<List<LayerConversationEntity>> listResponse) throws Exception {
                                    return listResponse.code() == HTTP_OK && listResponse.body() != null;
                                }
                            })
                            .concatMap(new Function<Response<List<LayerConversationEntity>>, ObservableSource<? extends LayerConversationEntity>>() {
                                @Override
                                public ObservableSource<? extends LayerConversationEntity> apply(Response<List<LayerConversationEntity>> source) throws Exception {
                                    return Observable.fromIterable(source.body());
                                }
                            })
                            .takeWhile(new Predicate<LayerConversationEntity>() {
                                @Override
                                public boolean test(LayerConversationEntity conversation) throws Exception {
                                    return !conversation.getId().equals(view.getConversationsManager().firstUri());
                                }
                            })
                            .toList()
                            .toObservable();
                }})
                .filter(new Predicate<List<LayerConversationEntity>>() {
                    @Override
                    public boolean test(List<LayerConversationEntity> layerConversationEntities) throws Exception {
                        return layerConversationEntities.size() > 0;
                    }
                })
                .map(new ConversationsMapper(userID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        new DisposableObserver<List<ConversationViewEntity>>() {
                            @Override
                            public void onNext(List<ConversationViewEntity> newConversations) {
                                view.getConversationsManager().pushTop(newConversations);
                            }

                            @Override
                            public void onError(Throwable e) {
                                e.printStackTrace();
                            }

                            @Override
                            public void onComplete() {
                                Log.d("ConversationsPresenter", "on conversations sync completed");
                            }
                        }
                );

    }

    private DisposableObserver<List<ConversationViewEntity>> createConversationsLoadedEventsObserver() {
        DisposableObserver<List<ConversationViewEntity>> conversationsLoadedEventsObserver = new DisposableObserver<List<ConversationViewEntity>>() {
            @Override
            public void onNext(List<ConversationViewEntity> items) {
                view.getConversationsManager().add(items);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(conversationsLoadedEventsObserver);

        return conversationsLoadedEventsObserver;
    }

    private DisposableObserver<Integer> createCreateConversationButtonClicksObserver() {
        DisposableObserver<Integer> createConversationButtonClicksObserver = new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer value) {
                view.startParticipantsPickerForResult();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(createConversationButtonClicksObserver);

        return createConversationButtonClicksObserver;
    }

    private DisposableObserver<Response<LayerResponse<LayerConversationEntity>>> createConversationCreationResponseObserver() {
        DisposableObserver<Response<LayerResponse<LayerConversationEntity>>> conversationCreationResponseObserver = new DisposableObserver<Response<LayerResponse<LayerConversationEntity>>>() {
            @Override
            public void onNext(Response<LayerResponse<LayerConversationEntity>> response) {
                final int code = response.code();
                switch(code) {
                    case HTTP_OK : {
                        view.onConversationAlreadyExists();
                        break;
                    }
                    case HTTP_CREATED : {
                        view.onConversationCreated();
                        Observable.just(Arrays.asList(response.body().getData()))
                                .observeOn(Schedulers.computation())
                                .map(new ConversationsMapper(userID))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(createConversationCreatedAndMappedEventObserver());
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(conversationCreationResponseObserver);

        return conversationCreationResponseObserver;
    }

    private DisposableObserver<List<ConversationViewEntity>> createConversationCreatedAndMappedEventObserver() {
        DisposableObserver<List<ConversationViewEntity>> conversationCreatedAndMappedEventObserver = new DisposableObserver<List<ConversationViewEntity>>() {
            @Override
            public void onNext(List<ConversationViewEntity> value) {
                view.updateConversationsAddNew(value);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(conversationCreatedAndMappedEventObserver);

        return conversationCreatedAndMappedEventObserver;
    }
}
