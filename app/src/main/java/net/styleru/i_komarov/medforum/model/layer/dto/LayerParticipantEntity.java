package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerParticipantEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_URL)
    private String url;
    @SerializedName(FIELD_USER_ID)
    private String userID;
    @SerializedName(FIELD_DISPLAY_NAME)
    private String displayName;
    @SerializedName(FIELD_AVATAR_URL)
    private String photoUrl;

    public LayerParticipantEntity(String id, String url, String userID, String displayName, String photoUrl) {
        this.id = id;
        this.url = url;
        this.userID = userID;
        this.displayName = displayName;
        this.photoUrl = photoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    @Override
    public String toString() {
        return "LayerParticipantEntity{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", userID='" + userID + '\'' +
                ", displayName='" + displayName + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                '}';
    }
}
