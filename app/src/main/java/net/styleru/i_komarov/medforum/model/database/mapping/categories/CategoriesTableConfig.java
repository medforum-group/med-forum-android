package net.styleru.i_komarov.medforum.model.database.mapping.categories;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 14.01.17.
 */

public class CategoriesTableConfig {

    @NonNull
    public static final String TABLE = "categories";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_SERVER_ID = "SERVER_ID";

    @NonNull
    public static final String COLUMN_TITLE = "TITLE";

    @NonNull
    public static final String COLUMN_LAST_UPDATE = "LAST_UPDATE";

    @NonNull
    public static final String COLUMN_QUESTION_COUNT = "QUESTION_COUNT";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_SERVER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SERVER_ID;
    public static final String COLUMN_TITLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_TITLE;
    public static final String COLUMN_LAST_UPDATE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_UPDATE;
    public static final String COLUMN_QUESTION_COUNT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_QUESTION_COUNT;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private CategoriesTableConfig() {
        throw new IllegalStateException("No instances please");
    }

    @NonNull
    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                    COLUMN_ID + " INTEGER NOT NULL PRIMARY KEY," +
                    COLUMN_SERVER_ID + " TEXT UNIQUE," +
                    COLUMN_TITLE + " TEXT," +
                    COLUMN_LAST_UPDATE + " TEXT," +
                    COLUMN_QUESTION_COUNT + " INTEGER" +
                ");";
    }
}
