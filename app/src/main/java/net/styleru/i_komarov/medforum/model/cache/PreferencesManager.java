package net.styleru.i_komarov.medforum.model.cache;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.concurrent.atomic.AtomicBoolean;

import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_FIRST_NAME;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_ID;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_IMAGE_URL;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_LAST_NAME;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_LAYER_ID;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_ROLE;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_SESSION_TOKEN;
import static net.styleru.i_komarov.medforum.model.cache.PreferencesManager.Const.USER_TOKEN;

/**
 * Created by i_komarov on 25.12.16.
 */

public class PreferencesManager {

    private static PreferencesManager instance;

    private SharedPreferences preferences;

    private PreferencesManager(Application app) {
        preferences = app.getSharedPreferences("user_defaults", Context.MODE_PRIVATE);
    }

    public static void onApplicationCreated(Application app) {
        instance = new PreferencesManager(app);
    }

    public static PreferencesManager getInstance() {
        if(instance != null) {
            return instance;
        } else {
            throw new RuntimeException("PreferencesManager.onApplicationCreated(Application) was not called!");
        }
    }

    public void saveSessionToken(String sessionToken) {
        preferences.edit().putString(USER_SESSION_TOKEN, sessionToken).commit();
    }

    public String loadSessionToken() {
        return preferences.getString(USER_SESSION_TOKEN, "");
    }

    public void saveLayerID(String code) {
        preferences.edit().putString(USER_LAYER_ID, code).commit();
    }

    public String loadLayerID() {
        return preferences.getString(USER_LAYER_ID, "");
    }

    public void saveUserID(String userID) {
        preferences.edit().putString(USER_ID, userID).commit();
    }

    public String loadUserID() {
        return preferences.getString(USER_ID, "");
    }

    public void saveToken(String token) {
        preferences.edit().putString(USER_TOKEN, token).commit();
    }

    public String loadToken() {
        return preferences.getString(USER_TOKEN, "");
    }

    public void saveUserRole(String role) {
        preferences.edit().putString(USER_ROLE, role).commit();
    }

    public String loadUserRole() {
        return preferences.getString(USER_ROLE, "guest");
    }

    public void saveUserFirstName(String firstName) {
        preferences.edit().putString(USER_FIRST_NAME, firstName).commit();
    }

    public String loadUserFirstName() {
        return preferences.getString(USER_FIRST_NAME, "");
    }

    public void saveUserLastName(String lastName) {
        preferences.edit().putString(USER_LAST_NAME, lastName).commit();
    }

    public String loadUserLastName() {
        return preferences.getString(USER_LAST_NAME, "");
    }

    public void saveUserImageUrl(String imageUrl) {
        preferences.edit().putString(USER_IMAGE_URL, imageUrl).commit();
    }

    public String loadUserImageUrl() {
        return preferences.getString(USER_IMAGE_URL, null);
    }

    static class Const {

        static final String USER_SESSION_TOKEN = Const.class.getCanonicalName() + ".USER_SESSION_TOKEN";

        static final String USER_LAYER_ID = Const.class.getCanonicalName() + ".USER_LAYER_ID";

        static final String USER_ID = Const.class.getCanonicalName() + ".USER_ID";

        static final String USER_TOKEN = Const.class.getCanonicalName() + ".USER_TOKEN";

        static final String USER_ROLE = Const.class.getCanonicalName() + ".USER_ROLE";

        static final String USER_FIRST_NAME = Const.class.getCanonicalName() + ".USER_FIRST_NAME";

        static final String USER_LAST_NAME = Const.class.getCanonicalName() + ".USER_LAST_NAME";

        static final String USER_IMAGE_URL = Const.class.getCanonicalName() + ".USER_IMAGE_URL";
    }
}
