package net.styleru.i_komarov.medforum.view.participants_picker;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.01.17.
 */

public interface IParticipantsPickerView {

    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable();

    public void refreshAdapter();
}
