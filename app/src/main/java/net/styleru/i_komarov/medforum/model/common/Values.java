package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 03.01.17.
 */

public class Values {

    public static final String VALUE_GUEST = "guest";
    public static final String VALUE_EXPERT = "expert";
}
