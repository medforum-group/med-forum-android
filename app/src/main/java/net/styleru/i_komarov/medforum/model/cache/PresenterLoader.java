package net.styleru.i_komarov.medforum.model.cache;

import android.content.Context;
import android.content.Loader;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;

/**
 * Created by i_komarov on 29.11.16.
 */

public class PresenterLoader<V, P extends Presenter<V>> extends Loader<P> {

    private final Presenter.Factory<V> factory;
    private P presenter;

    public PresenterLoader(Context context, Presenter.Factory<V> factory) {
        super(context);
        this.factory = factory;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        if(presenter == null) {
            forceLoad();
        } else {
            deliverResult(presenter);
        }
    }

    @Override
    public void deliverResult(P presenter) {
        this.presenter = presenter;

        if (isStarted()) {
            super.deliverResult(presenter);
        }
    }

    @Override
    protected void onForceLoad() {
        presenter = factory.create();
        deliverResult(presenter);
    }

    @Override
    protected void onReset() {
        onStopLoading();
        presenter.onDestroy();
        presenter = null;
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    public static abstract class Info<V> {

        public static final int LOADER_DRAWER = 0;
        public static final int LOADER_AUTHORIZATION = 0;
        public static final int LOADER_SEARCH = 0;
        public static final int LOADER_REGISTRATION = 0;
        public static final int LOADER_PARTICIPANTS_PICKER = 0;

        public static final int LOADER_AUTHORIZATION_ENTRY = 1;
        public static final int LOADER_REGISTRATION_1 = 1;
        public static final int LOADER_CATEGORIES = 1;
        public static final int LOADER_INFO = 1;
        public static final int LOADER_PARTICIPANTS = 1;
        public static final int LOADER_DIALOGS = 1;
        public static final int LOADER_QUESTIONS = 1;

        public static final int LOADER_QUESTION = 2;
        public static final int LOADER_DIALOG = 2;
        public static final int LOADER_TOPICS = 2;
        public static final int LOADER_PROFILE = 2;
        public static final int LOADER_AUTHORIZATION_VERIFICATION = 2;

        public static final int LOADER_DIALOG_PREFERENCES = 3;
        public static final int LOADER_QUESTION_PREFERENCES = 3;
        public static final int LOADER_DISCUSSION = 3;
        public static final int LOADER_TOPIC_CREATE = 3;

        public abstract int getID();

        public abstract Presenter.Factory<V> getFactory();

        public abstract V getView();
    }
}
