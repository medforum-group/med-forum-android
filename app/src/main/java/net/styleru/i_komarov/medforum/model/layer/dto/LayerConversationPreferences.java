package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerConversationPreferences {

    private JsonObject json;

    public LayerConversationPreferences(List<String> participants, boolean isDistinct) {
        this.json = new JsonObject();

        JsonArray participantsJson = new JsonArray();

        for(int i = 0; i < participants.size(); i++) {
            participantsJson.add(participants.get(i));
        }

        this.json.add(FIELD_PARTICIPANTS, participantsJson);
        this.json.addProperty(FIELD_DISTINCT, isDistinct);
    }

    public LayerConversationPreferences setMetadata(JsonObject metadata) {
        this.json.add(FIELD_METADATA, metadata);
        return this;
    }

    public LayerConversationPreferences setID(String id) {
        this.json.addProperty(FIELD_ID, id);
        return this;
    }

    public JsonObject build() {
        return json;
    }
}
