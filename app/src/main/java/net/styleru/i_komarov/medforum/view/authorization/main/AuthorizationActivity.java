package net.styleru.i_komarov.medforum.view.authorization.main;

import android.content.Intent;
import android.os.Bundle;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.activity.PresenterActivity;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.presenter.authorization.main.AuthorizationPresenter;
import net.styleru.i_komarov.medforum.view.authorization.entry.EntryFragment;
import net.styleru.i_komarov.medforum.view.authorization.verification.VerificationFragment;
import net.styleru.i_komarov.medforum.view.registration.main.RegistrationActivity;

/**
 * Created by i_komarov on 19.12.16.
 */

public class AuthorizationActivity extends PresenterActivity<IAuthorizationView, AuthorizationPresenter> implements IAuthorizationView {

    public static final int REQUEST_CODE_AUTHORIZATION = 8989;

    public static final String INTENT_KEY_AUTHORIZATION_RESULT = AuthorizationActivity.class.getCanonicalName() + ".AUTHORIZATION_RESULT";

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);

        if(getFragmentManager().findFragmentById(R.id.activity_authorization_screen_primary) == null) {
            getFragmentManager().beginTransaction().replace(R.id.activity_authorization_screen_primary, EntryFragment.newInstance()).commit();
        }
    }

    @Override
    public void onAuthorizationSuccess(Intent data) {
        UserServerEntity user = data.getParcelableExtra(INTENT_KEY_AUTHORIZATION_RESULT);
        if(user.getFirstName() == null || user.getLastName() == null || user.getFirstName().equals("") || user.getLastName().equals("") || user.getFirstName().equals("null") || user.getLastName().equals("null")) {
            this.onRegistrationNeeded(user);
        } else {
            PreferencesManager.getInstance().saveToken(user.getToken());
            PreferencesManager.getInstance().saveUserID(user.getId());
            PreferencesManager.getInstance().saveUserFirstName(user.getFirstName());
            PreferencesManager.getInstance().saveUserLastName(user.getLastName());
            PreferencesManager.getInstance().saveUserRole(user.getRole());
            PreferencesManager.getInstance().saveUserImageUrl(user.getImageUrl());
            setResult(RESULT_OK, data);
            finish();
        }
    }

    @Override
    public void onAuthorizationFailure(Intent data) {
        setResult(RESULT_CANCELED, data);
        finish();
    }

    @Override
    public void onRegistrationNeeded(UserServerEntity user) {
        Intent intent = new Intent(this, RegistrationActivity.class);
        intent.putExtra(INTENT_KEY_AUTHORIZATION_RESULT, user);
        startActivityForResult(intent, RegistrationActivity.REQUEST_CODE_REGISTRATION);
    }

    @Override
    public void onPhoneVerified(String phone) {
        getFragmentManager().beginTransaction()
                .replace(R.id.activity_authorization_screen_primary, VerificationFragment.newInstance(phone))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onUserChangePhoneAction() {
        getFragmentManager().popBackStack();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RegistrationActivity.REQUEST_CODE_REGISTRATION) {
            if(requestCode == RESULT_OK) {
                onAuthorizationSuccess(data);
            } else {
                //TODO: registration failed somehow
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected PresenterLoader.Info<IAuthorizationView> getLoaderInfo() {
        return new PresenterLoader.Info<IAuthorizationView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_AUTHORIZATION;
            }

            @Override
            public Presenter.Factory<IAuthorizationView> getFactory() {
                return new Presenter.Factory<IAuthorizationView>() {
                    @Override
                    public <P extends Presenter<IAuthorizationView>> P create() {
                        return (P) new AuthorizationPresenter();
                    }
                };
            }

            @Override
            public IAuthorizationView getView() {
                return AuthorizationActivity.this;
            }
        };
    }
}
