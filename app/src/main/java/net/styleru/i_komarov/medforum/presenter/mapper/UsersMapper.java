package net.styleru.i_komarov.medforum.presenter.mapper;

import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 03.01.17.
 */

public class UsersMapper implements Function<List<UserServerEntity>, List<ParticipantViewEntity>> {
    @Override
    public List<ParticipantViewEntity> apply(List<UserServerEntity> userServerEntityList) throws Exception {
        return Flowable.fromIterable(userServerEntityList)
                .map(user -> new ParticipantViewEntity(user.getId(), user.getImageUrl(), user.getFirstName(), user.getLastName()))
                .toList()
                .blockingGet();
    }
}
