package net.styleru.i_komarov.medforum.presenter.authorization.entry;

import android.util.Log;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.UsersProxy;
import net.styleru.i_komarov.medforum.view.authorization.entry.IEntryView;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 19.12.16.
 */

public class EntryPresenter extends Presenter<IEntryView> {

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IEntryView view) {
        super.bindView(view);
        subscribeToPhoneVerificationButtonClickObservable(view.getPhoneVerificationButtonClickChannel());
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onDestroy() {

    }

    private void subscribeToPhoneVerificationButtonClickObservable(Observable<String> phoneVerificationButtonClickObservable) {
        phoneVerificationButtonClickObservable
                .subscribeOn(AndroidSchedulers.mainThread())
                .filter(this::hookPhoneToVerify)
                .flatMap(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(String phone) throws Exception {
                        return UsersProxy.getInstance().code(phone)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .retry(3)
                                .filter(response -> hookHttpCode(response.code()))
                                .map(response -> phone);
                    }
                })
                .subscribe(createPhoneVerificationButtonClickObserver());
    }

    private Observer<String> createPhoneVerificationButtonClickObserver() {
        DisposableObserver<String> phoneVerificationButtonClickObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String phone) {
                view.onPhoneVerified(phone);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(phoneVerificationButtonClickObserver);

        return phoneVerificationButtonClickObserver;
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode != 200) {
            Log.d("EntryPresenter", "hookHttpCode: " + httpCode);
            view.onPhoneVerificationFailed();
            //TODO: add view.showHttpError(httpCode);
            return false;
        } else {
            return true;
        }
    }

    private boolean hookPhoneToVerify(String phone) {
        if(phone.length() == 16) {
            return true;
        } else {
            view.onPhoneVerificationFailed();
            return false;
        }
    }
}
