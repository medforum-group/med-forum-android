package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerIdentityEntity {

    @SerializedName(FIELD_IDENTITY_TOKEN)
    private String identityToken;
    @SerializedName(FIELD_APPLICATION_ID)
    private String appID;

    public LayerIdentityEntity(String identityToken, String appID) {
        this.identityToken = identityToken;
        this.appID = appID;
    }

    public String getIdentityToken() {
        return identityToken;
    }

    public void setIdentityToken(String identityToken) {
        this.identityToken = identityToken;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }
}
