package net.styleru.i_komarov.medforum.presenter.authorization.main;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.view.authorization.main.IAuthorizationView;

/**
 * Created by i_komarov on 19.12.16.
 */

public class AuthorizationPresenter extends Presenter<IAuthorizationView> {

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }
}
