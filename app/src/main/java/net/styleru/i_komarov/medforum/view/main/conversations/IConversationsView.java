package net.styleru.i_komarov.medforum.view.main.conversations;

import net.styleru.i_komarov.medforum.presenter.main.conversations.IConversationsManager;
import net.styleru.i_komarov.medforum.presenter.main.conversations.LayerListState;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 21.01.17.
 */

public interface IConversationsView {

    Observable<Integer> getCreateConversationButtonClicksObservable();

    Observable<Integer> getRefreshEventsObservable();

    Observable<LayerListState> getListStateChannel();

    void startParticipantsPickerForResult();

    void onConversationCreated();

    void onConversationAlreadyExists();

    void onConversationWithSelfCreateAttempt();

    void updateConversationsAddNew(List<ConversationViewEntity> conversations);

    IConversationsManager getConversationsManager();

    //String getLastUri();
}
