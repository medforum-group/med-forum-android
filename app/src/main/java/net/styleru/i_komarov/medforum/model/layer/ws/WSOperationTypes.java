package net.styleru.i_komarov.medforum.model.layer.ws;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSOperationTypes {

    public static final String TYPE_CREATE = "create";
    public static final String TYPE_UPDATE = "update";
    public static final String TYPE_DELETE = "delete";
}
