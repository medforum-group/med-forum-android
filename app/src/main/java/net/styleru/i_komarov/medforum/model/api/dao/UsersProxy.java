package net.styleru.i_komarov.medforum.model.api.dao;

import net.styleru.i_komarov.medforum.model.api.functional.API;
import net.styleru.i_komarov.medforum.model.common.ResultResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerIdentityToken;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 03.01.17.
 */

public class UsersProxy {

    private static UsersProxy instance;

    private API.Users api;

    private UsersProxy() {
        this.api = API.Factory.buildUsersAPI();
    }

    public static UsersProxy getInstance() {
        UsersProxy localInstance = instance;
        if(localInstance == null) {
            synchronized (UsersProxy.class) {
                localInstance = instance;
                if(localInstance == null) {
                    localInstance = instance = new UsersProxy();
                }
            }
        }

        return localInstance;
    }

    public Observable<Response<Object>> code(String phone) {
        phone = phone.replaceAll("\\(", "");
        phone = phone.replaceAll("\\+", "");
        phone = phone.replaceAll("\\)", "");
        phone = phone.replaceAll("\\-", "");

        return api.code(phone);
    }

    public Observable<Response<UserServerEntity>> auth(String phone, String code) {
        phone = phone.replaceAll("\\(", "");
        phone = phone.replaceAll("\\+", "");
        phone = phone.replaceAll("\\)", "");
        phone = phone.replaceAll("\\-", "");

        return api.auth(phone, code);
    }

    public Observable<Response<ResultResponseWrapper>> updatePersonalData(String userID, String token, String firstName, String lastName) {
        return api.updatePersonalData(userID, token, firstName, lastName);
    }

    public Observable<Response<UserServerEntity>> get(String userID) {
        return api.get(userID);
    }

    public Flowable<Response<List<UserServerEntity>>> list(int offset, int limit) {
        return api.getList(offset, limit);
    }

    public Flowable<Response<List<UserServerEntity>>> list(int offset, int limit, String filter) {
        return api.getList(offset, limit, filter);
    }

    public Observable<Response<LayerIdentityToken>> getToken(String userID, String nonce) {
        return api.getToken(userID, nonce);
    }
}
