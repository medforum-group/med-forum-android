package net.styleru.i_komarov.medforum.model.api.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.*;

/**
 * Created by i_komarov on 03.01.17.
 */

public class CategoryServerEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_NAME)
    private String title;
    @SerializedName(FIELD_LAST_UPDATE)
    private String lastUpdate;
    @SerializedName(FIELD_COUNT)
    private int count;

    public CategoryServerEntity(String id, String title, String lastUpdate, int count) {
        this.id = id;
        this.title = title;
        this.lastUpdate = lastUpdate;
        this.count = count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
