package net.styleru.i_komarov.medforum.view.main.dialog;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import net.styleru.i_komarov.medforum.R;

import io.reactivex.functions.Action;

/**
 * Created by i_komarov on 07.12.16.
 */

public class DialogBuilder {

    private final Context context;

    public DialogBuilder(Context context) {
        this.context = context;
    }

    public MaterialDialog buildErrorDialog() {
        return new MaterialDialog.Builder(context)
                .title(R.string.dialog_error_title)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .neutralText(R.string.dialog_ok)
                .onNeutral((dialog, which) -> dialog.dismiss())
                .build();
    }

    public MaterialDialog buildErrorDialog(Action onApply) {
        return new MaterialDialog.Builder(context)
                .title(R.string.dialog_error_title)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .neutralText(R.string.dialog_ok)
                .onNeutral((dialog, which) -> {
                    try {
                        onApply.run();
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                })
                .build();
    }

    public MaterialDialog buildConnectingDialog() {
        return new MaterialDialog.Builder(context)
                .title(R.string.dialog_progress_title)
                .progressIndeterminateStyle(true)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .build();
    }

    public MaterialDialog buildNotificationDialog() {
        return new MaterialDialog.Builder(context)
                .cancelable(true)
                .canceledOnTouchOutside(true)
                .build();
    }
}
