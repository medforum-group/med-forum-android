package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.FIELD_TITLE;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerMetadata {

    private JsonObject json;

    public LayerMetadata() {
        this.json = new JsonObject();
    }

    public LayerMetadata withTitle(String title) {
        this.json.addProperty(FIELD_TITLE, title);
        return this;
    }

    public JsonObject build() {
        return json;
    }
}
