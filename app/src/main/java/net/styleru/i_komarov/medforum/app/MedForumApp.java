package net.styleru.i_komarov.medforum.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.database.core.DatabaseManager;
import net.styleru.i_komarov.medforum.model.datetime.DateUtils;

/**
 * Created by i_komarov on 29.11.16.
 */

public class MedForumApp extends Application {

    private static String layerAppID;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        layerAppID = getResources().getString(R.string.layer_app_id);

        PreferencesManager.onApplicationCreated(this);
        DatabaseManager.onApplicationCreated(this);
        DateUtils.init(this);
    }

    public static String getLayerID() {
        return layerAppID;
    }
}
