package net.styleru.i_komarov.medforum.presenter.main.conversation;

import android.util.Log;

import com.google.gson.Gson;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.common.MimeTypes;
import net.styleru.i_komarov.medforum.model.layer.dao.ConversationProxy;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessageEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessagePart;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessagePreferences;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerParticipantEntity;
import net.styleru.i_komarov.medforum.model.layer.ws.WSObjectTypes;
import net.styleru.i_komarov.medforum.model.layer.ws.WSOperationTypes;
import net.styleru.i_komarov.medforum.model.layer.ws.WSResponseTypes;
import net.styleru.i_komarov.medforum.model.layer.ws.core.WSChannel;
import net.styleru.i_komarov.medforum.model.layer.ws.dto.WSChangeResponse;
import net.styleru.i_komarov.medforum.model.layer.ws.dto.WSResponse;
import net.styleru.i_komarov.medforum.model.util.UriUtils;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.conversation.ContentType;
import net.styleru.i_komarov.medforum.view.main.conversation.IConversationView;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageType;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageViewEntity;

import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by i_komarov on 22.01.17.
 */

public class ConversationPresenter extends Presenter<IConversationView> {

    private ConversationProxy proxy;

    private String conversationUri;

    private String userID;

    public ConversationPresenter(String conversationUri) {
        this.proxy = new ConversationProxy(PreferencesManager.getInstance().loadSessionToken());
        this.userID = PreferencesManager.getInstance().loadUserID();
        this.conversationUri = conversationUri;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IConversationView view) {
        super.bindView(view);
        subscribeOnListStateChannel(view.getListStateObservable());
        subscribeOnSendTextContentButtonClicksObservable(view.getSendContentButtonClicksObservable());
        view.getContentTypingEventsObservable().subscribe(createContentTypingEventsObserver());
        subscribeToLayerMessagesEventChannel(
                WSChannel.Factory.getInstance(PreferencesManager.getInstance().loadSessionToken())
                        .newWSChannelObservable(ConversationPresenter.class.getSimpleName())
                        .filter(new Predicate<WSResponse>() {
                            @Override
                            public boolean test(WSResponse wsResponse) throws Exception {
                                return wsResponse.getType().equals(WSResponseTypes.TYPE_CHANGE);
                            }
                        })
                        .map(new Function<WSResponse, WSChangeResponse>() {
                            @Override
                            public WSChangeResponse apply(WSResponse wsResponse) throws Exception {
                                return new Gson().fromJson(wsResponse.getBody(), WSChangeResponse.class);
                            }
                        })
                        .filter(new Predicate<WSChangeResponse>() {
                            @Override
                            public boolean test(WSChangeResponse wsChangeResponse) throws Exception {
                                return wsChangeResponse.getObject().getType().equals(WSObjectTypes.TYPE_MESSAGE) && wsChangeResponse.getOperation().equals(WSOperationTypes.TYPE_CREATE);
                            }
                        })
                        .map(new Function<WSChangeResponse, LayerMessageEntity>() {
                            @Override
                            public LayerMessageEntity apply(WSChangeResponse wsChangeResponse) throws Exception {
                                return new Gson().fromJson(wsChangeResponse.getData(), LayerMessageEntity.class);
                            }
                        })
                        .filter(new Predicate<LayerMessageEntity>() {
                            @Override
                            public boolean test(LayerMessageEntity message) throws Exception {
                                // check if we got an event from conversation needed and that it is not a notification about ours message
                                return UriUtils.extractEndpoint(message.getConversation().getId()).equals(conversationUri) && !message.getSender().getUserID().equals(userID);
                            }
                        })
        );
    }

    @Override
    public void unbindView() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
        super.unbindView();
    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }

    private void subscribeOnListStateChannel(Observable<State> stateChannel) {
        stateChannel.flatMap(new Function<State, ObservableSource<Response<List<LayerMessageEntity>>>>() {
                    @Override
                    public ObservableSource<Response<List<LayerMessageEntity>>> apply(State state) throws Exception {
                        return proxy.messages(conversationUri, view.getMessagesManager().lastUri(), state.getLimit())
                                .subscribeOn(Schedulers.io());
                    }
                })
                .filter(listResponse -> listResponse.code() == HTTP_OK)
                .map(Response::body)
                .map(new MessagesMapper(userID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        createLoadMessagesEventObserver()
                );
    }

    private DisposableObserver<List<MessageViewEntity>> createLoadMessagesEventObserver() {
        DisposableObserver<List<MessageViewEntity>> loadMessagesEventObserver = new DisposableObserver<List<MessageViewEntity>>() {
            @Override
            public void onNext(List<MessageViewEntity> items) {
                Log.d("ConversationPresenter", "items: " + items);
                view.getMessagesManager().add(items);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(loadMessagesEventObserver);

        return loadMessagesEventObserver;
    }

    private void subscribeOnSendTextContentButtonClicksObservable(Observable<String> sendTextContentButtonClicksObservable) {
        sendTextContentButtonClicksObservable.flatMap(
                new Function<String, ObservableSource<Response<LayerMessageEntity>>>() {
                    @Override
                    public ObservableSource<Response<LayerMessageEntity>> apply(String content) throws Exception {
                        return proxy.createMessage(conversationUri, new LayerMessagePreferences().addPart(new LayerMessagePart(null, MimeTypes.TEXT_PLAIN, content, null, null)))
                                .subscribeOn(Schedulers.io());
                    }
                })
                .filter(new Predicate<Response<LayerMessageEntity>>() {
                    @Override
                    public boolean test(Response<LayerMessageEntity> response) throws Exception {
                        return response.code() == HTTP_CREATED && response.body() != null;
                    }
                })
                .map(Response::body)
                .map(message -> {
                    try {
                        LayerParticipantEntity sender = message.getSender();
                        String name[] = sender.getDisplayName().split(" ");

                        return new MessageViewEntity(
                                ContentType.TEXT,
                                MessageType.FROM_USER,
                                message.getId(),
                                name[0],
                                name[1],
                                sender.getPhotoUrl(),
                                message.getTextMessagePart().getBody()
                        );
                    } catch(Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createSelfMessageSentEventObserver());

    }

    private void subscribeToLayerMessagesEventChannel(Observable<LayerMessageEntity> messagesEventChannel) {
        messagesEventChannel
                .map(new Function<LayerMessageEntity, List<LayerMessageEntity>>() {
                    @Override
                    public List<LayerMessageEntity> apply(LayerMessageEntity layerMessageEntity) throws Exception {
                        return Arrays.asList(layerMessageEntity);
                    }
                })
                .map(new MessagesMapper(userID))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createLayerMessageEventChannelObserver());

    }

    private DisposableObserver<MessageViewEntity> createSelfMessageSentEventObserver() {
        DisposableObserver<MessageViewEntity> selfMessageSentEventObserver = new DisposableObserver<MessageViewEntity>() {
            @Override
            public void onNext(MessageViewEntity message) {
                Log.d("ConversationPresenter", "message: " + message);
                view.getMessagesManager().addTop(message);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(selfMessageSentEventObserver);

        return selfMessageSentEventObserver;
    }

    private DisposableObserver<String> createContentTypingEventsObserver() {
        DisposableObserver<String> contentTypingEventsObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String content) {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(contentTypingEventsObserver);

        return contentTypingEventsObserver;
    }

    private DisposableObserver<List<MessageViewEntity>> createLayerMessageEventChannelObserver() {
        DisposableObserver<List<MessageViewEntity>> layerMessageEventChannelObserver = new DisposableObserver<List<MessageViewEntity>>() {
            @Override
            public void onNext(List<MessageViewEntity> message) {
                view.getMessagesManager().addTop(message);
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(layerMessageEventChannelObserver);

        return layerMessageEventChannelObserver;
    }
}
