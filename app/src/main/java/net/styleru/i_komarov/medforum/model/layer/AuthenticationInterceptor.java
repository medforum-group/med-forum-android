package net.styleru.i_komarov.medforum.model.layer;

import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.layer.dao.AuthenticationProxy;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;
import static net.styleru.i_komarov.medforum.model.common.Headers.HEADER_AUTHORIZATION;
import static net.styleru.i_komarov.medforum.model.common.Headers.HEADER_AUTHORIZATION_LAYER_FORMAT;

/**
 * Created by i_komarov on 21.01.17.
 */

public class AuthenticationInterceptor implements Interceptor {

    private String sessionToken;

    public AuthenticationInterceptor(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response response = chain.proceed(
                chain.request().newBuilder()
                        .addHeader(HEADER_AUTHORIZATION, String.format(HEADER_AUTHORIZATION_LAYER_FORMAT, sessionToken))
                        .build()
        );

        if(response.code() == HTTP_UNAUTHORIZED) {
            sessionToken = AuthenticationProxy.getInstance()
                    .authenticate(PreferencesManager.getInstance().loadUserID())
                    .blockingFirst().body()
                    .getSessionToken();

            response = chain.proceed(
                    chain.request().newBuilder()
                            .addHeader(HEADER_AUTHORIZATION, String.format(HEADER_AUTHORIZATION_LAYER_FORMAT, sessionToken))
                            .build()
            );
        }

        return response;
    }
}
