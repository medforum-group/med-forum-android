package net.styleru.i_komarov.medforum.view.main.participants;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 30.12.16.
 */

public interface IParticipantsView {

    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable();

    public void refreshAdapter();
}
