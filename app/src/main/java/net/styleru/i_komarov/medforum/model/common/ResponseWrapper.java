package net.styleru.i_komarov.medforum.model.common;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.FIELD_DESCRIPTION;

/**
 * Created by i_komarov on 03.01.17.
 */

public class ResponseWrapper<T> {

    @SerializedName(FIELD_DESCRIPTION)
    private T body;

    public ResponseWrapper(T body) {
        this.body = body;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
