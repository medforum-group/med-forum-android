package net.styleru.i_komarov.medforum.view.registration.main;

import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;

/**
 * Created by i_komarov on 06.01.17.
 */

public interface IRegistrationView {

    public void onRegistrationSucceeded(UserServerEntity user);

    public void onRegistrationFailed();
}
