package net.styleru.i_komarov.medforum.view.main.conversation;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 22.01.17.
 */

public class MessageViewEntity implements Parcelable {

    private ContentType contentType;
    private MessageType type;

    private String messageID;
    private String firstName;
    private String lastName;
    private String photoUrl;
    private String content;

    public MessageViewEntity(Parcel in) {
        byte contentTypeID = in.readByte();

        switch (contentTypeID) {
            case 1: {
                this.contentType = ContentType.TEXT;
                break;
            }
            case 2: {
                this.contentType = ContentType.IMAGE;
                break;
            }
            case 3: {
                this.contentType = ContentType.LOCATION;
                break;
            }
            case 4: {
                this.contentType = ContentType.AUDIO;
                break;
            }
            case 5: {
                this.contentType = ContentType.VIDEO;
                break;
            }
        }

        this.type = in.readByte() == (byte) 1 ? MessageType.FROM_USER : MessageType.TO_USER;
        this.messageID = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.photoUrl = in.readString();
        this.content = in.readString();
    }

    public MessageViewEntity(ContentType contentType, MessageType type, String messageID, String firstName, String lastName, String photoUrl, String content) {
        this.contentType = contentType;
        this.type = type;
        this.messageID = messageID;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photoUrl = photoUrl;
        this.content = content;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getMessageID() {
        return this.messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(
                contentType == ContentType.TEXT ? 1 :
                contentType == ContentType.IMAGE ? 2 :
                contentType == ContentType.LOCATION ? 3 :
                contentType == ContentType.AUDIO ? 4 :
                contentType == ContentType.VIDEO ? 5 :
                Byte.MIN_VALUE
        );
        dest.writeByte(type == MessageType.FROM_USER ? (byte) 1 : (byte) 0);
        dest.writeString(messageID == null ? "" : messageID);
        dest.writeString(firstName == null ? "" : firstName);
        dest.writeString(lastName == null ? "" : lastName);
        dest.writeString(photoUrl == null ? "" : photoUrl);
        dest.writeString(content == null ? "" : content);
    }

    public static final Parcelable.Creator<MessageViewEntity> CREATOR = new Creator<MessageViewEntity>() {
        @Override
        public MessageViewEntity createFromParcel(Parcel in) {
            return new MessageViewEntity(in);
        }

        @Override
        public MessageViewEntity[] newArray(int size) {
            return new MessageViewEntity[0];
        }
    };
}
