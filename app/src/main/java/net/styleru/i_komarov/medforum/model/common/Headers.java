package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 21.01.17.
 */

public class Headers {

    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_AUTHORIZATION_LAYER_FORMAT = "Layer session-token='%1$s'";
}
