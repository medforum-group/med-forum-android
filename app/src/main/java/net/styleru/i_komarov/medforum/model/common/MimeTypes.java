package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 22.01.17.
 */

/** {list in  <a href=https://ru.wikipedia.org/wiki/Список_MIME-типов#text>wikipedia</a>} */
public class MimeTypes {

    public static final String DELIMETER = "/";


    public static final String TEXT = "text";

    public static final String TEXT_PLAIN      = TEXT + DELIMETER + "plain";
    public static final String TEXT_CMD        = TEXT + DELIMETER + "cmd";
    public static final String TEXT_CSS        = TEXT + DELIMETER + "css";
    public static final String TEXT_CSV        = TEXT + DELIMETER + "csv";
    public static final String TEXT_HTML       = TEXT + DELIMETER + "html";
    public static final String TEXT_JAVASCRIPT = TEXT + DELIMETER + "javascript";
    public static final String TEXT_PHP        = TEXT + DELIMETER + "php";
    public static final String TEXT_XML        = TEXT + DELIMETER + "xml";


    public static final String IMAGE = "image";

    public static final String IMAGE_JPEG     = IMAGE + DELIMETER + "jpeg";
    public static final String IMAGE_BMP      = IMAGE + DELIMETER + "bmp";
    public static final String IMAGE_GIF      = IMAGE + DELIMETER + "gif";
    public static final String IMAGE_JPG      = IMAGE + DELIMETER + "jpg";
    public static final String IMAGE_PNG      = IMAGE + DELIMETER + "png";
    public static final String IMAGE_PJPEG    = IMAGE + DELIMETER + "pgpeg";
    public static final String IMAGE_SVG_XML  = IMAGE + DELIMETER + "svg+xml";
    public static final String IMAGE_TIFF     = IMAGE + DELIMETER + "tiff";
    public static final String IMAGE_MS_ICON  = IMAGE + DELIMETER + "vnd.microsoft.icon";
    public static final String IMAGE_WAP_WBMP = IMAGE + DELIMETER + "vnd.wap.wbmp";
    public static final String IMAGE_WEBP     = IMAGE + DELIMETER + "webp";
}
