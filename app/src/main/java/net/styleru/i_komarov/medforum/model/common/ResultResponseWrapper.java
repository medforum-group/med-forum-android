package net.styleru.i_komarov.medforum.model.common;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.FIELD_RESULT;

/**
 * Created by i_komarov on 08.01.17.
 */

public class ResultResponseWrapper {

    @SerializedName(FIELD_RESULT)
    private String result;

    public ResultResponseWrapper(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean status() {
        if(result == null && result.equals("") || result.equals("null")) {
            return false;
        } else {
            return result.equals("OK") || result.equals("ОК");
        }
    }
}
