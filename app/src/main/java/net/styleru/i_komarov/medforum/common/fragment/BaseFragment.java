package net.styleru.i_komarov.medforum.common.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.View;

/**
 * Created by i_komarov on 29.11.16.
 */

public abstract class BaseFragment extends Fragment {

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindViewComponents(view);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    protected abstract void bindViewComponents(View view);
}
