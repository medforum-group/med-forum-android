package net.styleru.i_komarov.medforum.common.activity;

import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;

/**
 * Created by i_komarov on 29.11.16.
 */

public abstract class PresenterActivity<V, P extends Presenter<V>> extends RxBindableActivity implements LoaderManager.LoaderCallbacks<P> {

    private PresenterLoader.Info<V> info = getLoaderInfo();

    protected P presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(info.getID(), null, this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onStart();
        presenter.bindView(info.getView());
    }

    public void onResumeIgnorePresenter() {
        super.onResume();
        presenter.onStart();
    }

    @Override
    public void onPause() {
        presenter.unbindView();
        presenter.onStop();
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if(presenter != null) {
            presenter.unbindView();
            presenter.onStop();
            presenter.onDestroy();
        }
        super.onDestroy();
    }

    @Override
    public Loader<P> onCreateLoader(int id, Bundle args) {
        return new PresenterLoader<>(getApplicationContext(), info.getFactory());
    }

    @Override
    public void onLoadFinished(Loader<P> loader, P presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onLoaderReset(Loader<P> loader) {
        this.presenter = null;
    }

    protected abstract PresenterLoader.Info<V> getLoaderInfo();
}
