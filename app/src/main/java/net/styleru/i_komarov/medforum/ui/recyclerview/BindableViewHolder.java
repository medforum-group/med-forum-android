package net.styleru.i_komarov.medforum.ui.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by i_komarov on 29.11.16.
 */

public class BindableViewHolder<T, I extends BindableViewHolder.ActionListener<T>> extends RecyclerView.ViewHolder {

    protected I listener;

    public BindableViewHolder(View itemView) {
        super(itemView);
    }

    public void bind(int position, T item, I listener) {
        if(listener != null) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.clicked(position, item);
                }
            });
        }
    }

    public interface ActionListener<T> {

        void clicked(int position, T item);

        void longClicked(int position, T item);

        void swiped(int position, int direction);
    }
}

