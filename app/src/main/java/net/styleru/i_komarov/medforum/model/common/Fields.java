package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 25.12.16.
 */

public class Fields {

    public static final String FIELD_DESCRIPTION = "description";
    public static final String FIELD_RESULT = "result";

    public static final String FIELD_CODE = "code";

    public static final String FIELD_LIST = "list";

    public static final String FIELD_ID = "id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_SURNAME = "surname";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_ROLE = "role";
    public static final String FIELD_IMAGE = "image";
    public static final String FIELD_TOKEN = "token";
    public static final String FIELD_NONCE = "nonce";

    public static final String FIELD_LIMIT = "offset";
    public static final String FIELD_OFFSET = "first";
    public static final String FIELD_QUERY = "query";

    public static final String FIELD_LAST_UPDATE = "lastUpdate";
    public static final String FIELD_COUNT = "Count";

    public static final String FIELD_QUESTION_USER_ID = "idUser";
    public static final String FIELD_QUESTION_RESPONSE_ID = "idCategory";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_TEXT = "text";
    public static final String FIELD_QUESTION_COUNT = "count";
    public static final String FIELD_QUESTION_QUESTION_ID = "idQuestion";

    public static final String FIELD_DATE = "date";

    public static final String FIELD_CATEGORY_ID = "categoryID";
    public static final String FIELD_QUESTION_ID = "questionID";
    public static final String FIELD_USER_ID = "userID";

}
