package net.styleru.i_komarov.medforum.presenter.main.participants;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.UsersProxy;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.presenter.mapper.UsersMapper;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.participants.IParticipantsView;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;

/**
 * Created by i_komarov on 30.12.16.
 */

public class ParticipantsPresenter extends Presenter<IParticipantsView> {

    private String currentQuery = "";

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IParticipantsView view) {
        super.bindView(view);
        subscribeOnSearchViewChangeEventChannel(view.getSearchViewChangeEventObservable().debounce(500, TimeUnit.MILLISECONDS));
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onDestroy() {

    }

    public Flowable<List<ParticipantViewEntity>> provideDataChannel(Flowable<State> listStateChannel) {
        return listStateChannel.flatMap(
                new Function<State, Publisher<List<UserServerEntity>>>() {
                    @Override
                    public Publisher<List<UserServerEntity>> apply(State state) throws Exception {
                        return UsersProxy.getInstance().list(state.getOffset(), state.getLimit())
                                .subscribeOn(Schedulers.io())
                                .filter(response -> hookHttpCode(response.code()))
                                .filter(response -> response != null && response.body() != null && response.body().size() != 0)
                                .map(Response::body);
                    }
                })
                .map(new UsersMapper())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void subscribeOnSearchViewChangeEventChannel(Observable<SearchViewChangeEvent> searchViewChangeEventObservable) {
        searchViewChangeEventObservable.subscribe(createSearchViewChangeEventChannelObserver());
    }

    private DisposableObserver<SearchViewChangeEvent> createSearchViewChangeEventChannelObserver() {
        DisposableObserver<SearchViewChangeEvent> searchViewChangeEventDisposableObserver = new DisposableObserver<SearchViewChangeEvent>() {
            @Override
            public void onNext(SearchViewChangeEvent event) {
                if(event.getType() == SearchViewChangeEvent.Type.CHANGE) {
                    currentQuery = event.getQuery();
                    view.refreshAdapter();
                } else if(event.getType() == SearchViewChangeEvent.Type.SUBMIT) {

                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(searchViewChangeEventDisposableObserver);

        return searchViewChangeEventDisposableObserver;
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode == HTTP_OK) {
            return true;
        } else {
            return false;
        }
    }
}
