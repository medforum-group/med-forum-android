package net.styleru.i_komarov.medforum.model.layer.dto;

import android.webkit.MimeTypeMap;

import com.google.gson.annotations.SerializedName;
import com.layer.sdk.messaging.MessagePart;

import net.styleru.i_komarov.medforum.model.common.MimeTypes;

import java.util.List;
import java.util.Map;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerMessageEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_URL)
    private String url;
    @SerializedName(FIELD_RECEIPTS_URL)
    private String receiptUrl;
    @SerializedName(FIELD_POSITION)
    private Long position;
    @SerializedName(FIELD_CONVERSATION)
    private LayerIdentifierUrlPair conversation;
    @SerializedName(FIELD_PARTS)
    private List<LayerMessagePart> parts;
    @SerializedName(FIELD_SENT_AT)
    private String sentAt;
    @SerializedName(FIELD_SENDER)
    private LayerParticipantEntity sender;
    @SerializedName(FIELD_IS_UNREAD)
    private Boolean isUnread;
    @SerializedName(FIELD_RECEPIENT_STATUS)
    private Map<String, String> recipientStatuses;

    public LayerMessageEntity(String id, String url, String receiptUrl, Long position, LayerIdentifierUrlPair conversation, List<LayerMessagePart> parts, String sentAt, LayerParticipantEntity sender, Boolean isUnread, Map<String, String> recipientStatuses) {
        this.id = id;
        this.url = url;
        this.receiptUrl = receiptUrl;
        this.position = position;
        this.conversation = conversation;
        this.parts = parts;
        this.sentAt = sentAt;
        this.sender = sender;
        this.isUnread = isUnread;
        this.recipientStatuses = recipientStatuses;
    }

    public String getPreview() {
        LayerMessagePart textPart = getTextMessagePart();
        return textPart == null ? "" : textPart.getBody();
    }

    public LayerMessagePart getTextMessagePart() {
        for(LayerMessagePart part : parts) {
            if(part.getMimeType().equals(MimeTypes.TEXT_PLAIN)) {
                return part;
            }
        }

        return null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getReceiptUrl() {
        return receiptUrl;
    }

    public void setReceiptUrl(String receiptUrl) {
        this.receiptUrl = receiptUrl;
    }

    public Long getPosition() {
        return position;
    }

    public void setPosition(Long position) {
        this.position = position;
    }

    public LayerIdentifierUrlPair getConversation() {
        return conversation;
    }

    public void setConversation(LayerIdentifierUrlPair conversation) {
        this.conversation = conversation;
    }

    public List<LayerMessagePart> getParts() {
        return parts;
    }

    public void setParts(List<LayerMessagePart> parts) {
        this.parts = parts;
    }

    public String getSentAt() {
        return sentAt;
    }

    public void setSentAt(String sentAt) {
        this.sentAt = sentAt;
    }

    public LayerParticipantEntity getSender() {
        return sender;
    }

    public void setSender(LayerParticipantEntity sender) {
        this.sender = sender;
    }

    public Boolean getUnread() {
        return isUnread;
    }

    public void setUnread(Boolean unread) {
        isUnread = unread;
    }

    public Map<String, String> getRecipientStatuses() {
        return recipientStatuses;
    }

    public void setRecipientStatuses(Map<String, String> recipientStatuses) {
        this.recipientStatuses = recipientStatuses;
    }

    @Override
    public String toString() {
        return "LayerMessageEntity{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", receiptUrl='" + receiptUrl + '\'' +
                ", position=" + position +
                ", conversation=" + conversation +
                ", parts=" + parts +
                ", sentAt='" + sentAt + '\'' +
                ", sender=" + sender +
                ", isUnread=" + isUnread +
                ", recipientStatuses=" + recipientStatuses +
                '}';
    }
}
