package net.styleru.i_komarov.medforum.presenter.notifications;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

/**
 * Created by i_komarov on 11.01.17.
 */

public class FirebaseCloudMessagesReceiver extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);

        if(message == null) {
            return;
        }

        if(message.getData() != null) {
            Map<String, String> data = message.getData();
            //TODO: use below parser for parsing data
            /**{@link net.styleru.i_komarov.medforum.model.notifications.FCMNotificationParser}*/
        }

        RemoteMessage.Notification notification = message.getNotification();
        //TODO: complete this
    }
}
