package net.styleru.i_komarov.medforum.model.database.mapping.answers;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

/**
 * Created by i_komarov on 15.01.17.
 */

public class AnswersTableConfig {

    @NonNull
    public static final String TABLE = "answers";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_SERVER_ID = "SERVER_ID";

    @NonNull
    public static final String COLUMN_QUESTION_ID = "QUESTION_ID";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_IMAGE_URL = "IMAGE_URL";

    @NonNull
    public static final String COLUMN_ROLE = "ROLE";

    @NonNull
    public static final String COLUMN_DATE = "DATE";

    @NonNull
    public static final String COLUMN_CONTENT = "CONTENT";


    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_SERVER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SERVER_ID;
    public static final String COLUMN_QUESTION_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_QUESTION_ID;
    public static final String COLUMN_USER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_FIRST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_FIRST_NAME;
    public static final String COLUMN_LAST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_IMAGE_URL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_IMAGE_URL;
    public static final String COLUMN_ROLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ROLE;
    public static final String COLUMN_DATE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_DATE;
    public static final String COLUMN_CONTENT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CONTENT;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private AnswersTableConfig() {
        throw new IllegalStateException("No instances please");
    }

    public static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE +
                "(" +
                    COLUMN_ID + " INTEGER NON NULL PRIMARY KEY," +
                    COLUMN_SERVER_ID + " TEXT," +
                    COLUMN_QUESTION_ID + " TEXT," +
                    COLUMN_USER_ID + " TEXT," +
                    COLUMN_FIRST_NAME + " TEXT," +
                    COLUMN_LAST_NAME + " TEXT," +
                    COLUMN_IMAGE_URL + " TEXT," +
                    COLUMN_ROLE + " TEXT," +
                    COLUMN_DATE + " TEXT," +
                    COLUMN_CONTENT + " TEXT" +
                ");";

    }
}
