package net.styleru.i_komarov.medforum.model.api.dao;

import net.styleru.i_komarov.medforum.model.api.functional.API;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.common.ResultResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.AnswerServerEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 03.01.17.
 */

public class AnswersProxy {

    private static AnswersProxy instance;

    private API.Answers api;

    private AnswersProxy() {
        this.api = API.Factory.buildAnswersAPI();
    }

    public static AnswersProxy getInstance() {
        AnswersProxy localInstance = instance;
        if(localInstance == null) {
            synchronized (AnswersProxy.class) {
                localInstance = instance;
                if(localInstance == null) {
                    localInstance = instance = new AnswersProxy();
                }
            }
        }

        return localInstance;
    }

    public Flowable<Response<ResponseWrapper<List<AnswerServerEntity>>>> list(String questionID, int offset, int limit) {
        return api.getList(questionID, offset, limit);
    }

    public Observable<Response<ResultResponseWrapper>> reply(String userID, String questionID, String content) {
        return api.reply(userID, questionID, content);
    }
}
