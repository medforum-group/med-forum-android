package net.styleru.i_komarov.medforum.presenter.main.conversation;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.conversation.ContentType;
import net.styleru.i_komarov.medforum.view.main.conversation.IMessagesManager;
import net.styleru.i_komarov.medforum.view.main.conversation.MyMessageTextViewHolder;
import net.styleru.i_komarov.medforum.view.main.conversation.TheirMessageTextViewHolder;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageType;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageViewEntity;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.BiPredicate;

/**
 * Created by i_komarov on 22.01.17.
 */

public class MessagesAdapter extends RecyclerView.Adapter<BindableViewHolder<MessageViewEntity, BindableViewHolder.ActionListener<MessageViewEntity>>> implements IMessagesManager, Parcelable {

    private static final int TYPE_TEXT_FROM_USER = 0;
    private static final int TYPE_TEXT_TO_USER = 1;
    private static final int TYPE_IMAGE_FROM_USER = 2;
    private static final int TYPE_IMAGE_TO_USER = 3;
    private static final int TYPE_LOCATION_FROM_USER = 4;
    private static final int TYPE_LOCATION_TO_USER = 5;
    private static final int TYPE_AUDIO_FROM_USER = 6;
    private static final int TYPE_AUDIO_TO_USER = 7;
    private static final int TYPE_VIDEO_FROM_USER = 8;
    private static final int TYPE_VIDEO_TO_USER = 9;

    private String oldestMessageUri;

    private int limit = 30;

    private List<MessageViewEntity> items;
    private BindableViewHolder.ActionListener<MessageViewEntity> listener;

    public MessagesAdapter() {
        this.items = new ArrayList<>();
    }

    public MessagesAdapter(Parcel in) {
        in.readList(this.items, List.class.getClassLoader());
        this.oldestMessageUri = in.readString();
    }

    @Override
    public BindableViewHolder<MessageViewEntity, BindableViewHolder.ActionListener<MessageViewEntity>> onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_TEXT_FROM_USER) {
            return new MyMessageTextViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_my, parent, false));
        } else if(viewType == TYPE_TEXT_TO_USER) {
            return new TheirMessageTextViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_message_their, parent, false));
        } else {
            throw new UnsupportedOperationException("Other content types are not supported yet");
        }
    }

    @Override
    public void onBindViewHolder(BindableViewHolder<MessageViewEntity, BindableViewHolder.ActionListener<MessageViewEntity>> holder, int position) {
        holder.bind(position, items.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    @Override
    public void clear() {
        final int itemCount = this.items.size();
        this.items.clear();
        notifyItemRangeRemoved(0, itemCount);
    }

    @Override
    public void add(MessageViewEntity item) {
        final int insertPosition = this.items.size();
        this.items.add(item);
        notifyItemInserted(insertPosition);
        this.oldestMessageUri = item.getMessageID();
    }

    @Override
    public void add(List<MessageViewEntity> items) {
        if(items.size() > 0) {
            this.oldestMessageUri = items.get(items.size() - 1).getMessageID();
            final int startPosition = this.items.size();
            this.items.addAll(items);
            notifyItemRangeInserted(startPosition, items.size());
        }
    }

    @Override
    public void addTop(MessageViewEntity item) {
        this.items.add(0, item);
        notifyItemInserted(0);
    }

    @Override
    public void addTop(List<MessageViewEntity> items) {
        if (items.size() > 0) {
            final int itemCount = items.size();
            this.items.addAll(0, items);
            notifyItemRangeInserted(0, itemCount);
        }
    }

    @Override
    public String lastUri() {
        return oldestMessageUri;
    }

    @Override
    public int getItemViewType(int position) {
        MessageViewEntity message = items.get(position);
        if(message.getContentType() == ContentType.TEXT) {
            return message.getType() == MessageType.FROM_USER ? TYPE_TEXT_FROM_USER : TYPE_TEXT_TO_USER;
        } else if(message.getContentType() == ContentType.IMAGE) {
            return message.getType() == MessageType.FROM_USER ? TYPE_IMAGE_FROM_USER : TYPE_IMAGE_TO_USER;
        } else if(message.getContentType() == ContentType.LOCATION) {
            return message.getType() == MessageType.FROM_USER ? TYPE_LOCATION_FROM_USER : TYPE_LOCATION_TO_USER;
        } else if(message.getContentType() == ContentType.AUDIO) {
            return message.getType() == MessageType.FROM_USER ? TYPE_AUDIO_FROM_USER : TYPE_AUDIO_TO_USER;
        } else if(message.getContentType() == ContentType.VIDEO) {
            return message.getType() == MessageType.FROM_USER ? TYPE_VIDEO_FROM_USER : TYPE_VIDEO_TO_USER;
        } else throw new IllegalStateException("Unknown message content type: " + message.getContentType());
    }

    public State getState() {
        return new State(items.size(), limit);
    }

    public Observable<State> createListStateObservable(RecyclerView list) {
        return Observable.create(
                new ObservableOnSubscribe<State>() {
                    @Override
                    public void subscribe(ObservableEmitter<State> stateEmitter) throws Exception {
                        list.setOnScrollListener(buildPaginationListener(stateEmitter));
                    }
                })
                .distinctUntilChanged(
                        new BiPredicate<State, State>() {
                            @Override
                            public boolean test(State state, State state2) throws Exception {
                                return state.getOffset() == state2.getOffset();
                            }
                        }
                );
    }

    private RecyclerView.OnScrollListener buildPaginationListener(final ObservableEmitter<State> stateEmitter) {
        stateEmitter.onNext(new State(0, limit));

        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                State state = getState();

                int position = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                int updatePosition = getItemCount() - 1 - (state.getLimit() / 2);

                if(updatePosition <= position) {
                    stateEmitter.onNext(state);
                }
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(items);
        dest.writeString(oldestMessageUri == null ? "" : oldestMessageUri);
    }

    public static final Parcelable.Creator<MessagesAdapter> CREATOR = new Creator<MessagesAdapter>() {
        @Override
        public MessagesAdapter createFromParcel(Parcel in) {
            return new MessagesAdapter(in);
        }

        @Override
        public MessagesAdapter[] newArray(int size) {
            return new MessagesAdapter[0];
        }
    };
}
