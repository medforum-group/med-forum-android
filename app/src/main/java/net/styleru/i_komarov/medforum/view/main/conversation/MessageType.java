package net.styleru.i_komarov.medforum.view.main.conversation;

/**
 * Created by i_komarov on 22.01.17.
 */

public enum MessageType {
    FROM_USER,
    TO_USER
}
