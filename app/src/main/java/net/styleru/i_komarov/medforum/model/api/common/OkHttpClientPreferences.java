package net.styleru.i_komarov.medforum.model.api.common;

/**
 * Created by i_komarov on 03.01.17.
 */

public class OkHttpClientPreferences {

    private int connectTimeout = 120;
    private int readTimeout = 30;
    private int writeTimeout = 30;

    public OkHttpClientPreferences() {

    }

    public OkHttpClientPreferences(int writeTimeout) {
        this.writeTimeout = writeTimeout;
    }

    public OkHttpClientPreferences(int writeTimeout, int readTimeout) {
        this.writeTimeout = writeTimeout;
        this.readTimeout = readTimeout;
    }

    public OkHttpClientPreferences(int writeTimeout, int readTimeout, int timeout) {
        this.writeTimeout = writeTimeout;
        this.readTimeout = readTimeout;
        this.connectTimeout = timeout;
    }


    public int getConnectTimeout() {
        return connectTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public int getWriteTimeout() {
        return writeTimeout;
    }
}
