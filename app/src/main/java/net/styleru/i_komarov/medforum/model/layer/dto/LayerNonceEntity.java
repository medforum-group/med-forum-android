package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.FIELD_NONCE;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerNonceEntity {

    @SerializedName(FIELD_NONCE)
    private String nonce;

    public LayerNonceEntity(String nonce) {
        this.nonce = nonce;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }
}
