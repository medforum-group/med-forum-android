package net.styleru.i_komarov.medforum.view.main.conversation;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;

/**
 * Created by i_komarov on 22.01.17.
 */

public class MyMessageTextViewHolder extends BindableViewHolder<MessageViewEntity, BindableViewHolder.ActionListener<MessageViewEntity>> {

    private AppCompatTextView contentHolder;

    public MyMessageTextViewHolder(View itemView) {
        super(itemView);
        contentHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_message_my_content_holder);
    }

    @Override
    public void bind(int position, MessageViewEntity item, BindableViewHolder.ActionListener<MessageViewEntity> listener) {
        super.bind(position, item, listener);

        contentHolder.setText(item.getContent());
    }
}
