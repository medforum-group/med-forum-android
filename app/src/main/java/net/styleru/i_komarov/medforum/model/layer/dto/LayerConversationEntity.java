package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerConversationEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_URL)
    private String url;
    @SerializedName(FIELD_MESSAGES_URL)
    private String messagesUrl;
    @SerializedName(FIELD_CREATED_AT)
    private String createdAt;
    @SerializedName(FIELD_LAST_MESSAGE)
    private LayerMessageEntity lastMessage;
    @SerializedName(FIELD_PARTICIPANTS)
    private List<LayerParticipantEntity> participants;
    @SerializedName(FIELD_DISTINCT)
    private Boolean isDistinct;
    @SerializedName(FIELD_UNREAD_MESSAGES_COUNT)
    private Long unreadCount;

    public LayerConversationEntity(String id, String url, String messagesUrl, String createdAt, LayerMessageEntity lastMessage, List<LayerParticipantEntity> participants, Boolean isDistinct, Long unreadCount) {
        this.id = id;
        this.url = url;
        this.messagesUrl = messagesUrl;
        this.createdAt = createdAt;
        this.lastMessage = lastMessage;
        this.participants = participants;
        this.isDistinct = isDistinct;
        this.unreadCount = unreadCount;
    }

    public String getPreview() {
        return lastMessage == null ? "" : lastMessage.getPreview();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMessagesUrl() {
        return messagesUrl;
    }

    public void setMessagesUrl(String messagesUrl) {
        this.messagesUrl = messagesUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public LayerMessageEntity getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LayerMessageEntity lastMessage) {
        this.lastMessage = lastMessage;
    }

    public List<LayerParticipantEntity> getParticipants() {
        return participants;
    }

    public void setParticipants(List<LayerParticipantEntity> participants) {
        this.participants = participants;
    }

    public Boolean getDistinct() {
        return isDistinct;
    }

    public void setDistinct(Boolean distinct) {
        isDistinct = distinct;
    }

    public Long getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(Long unreadCount) {
        this.unreadCount = unreadCount;
    }

    @Override
    public String toString() {
        return "LayerConversationEntity{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", messagesUrl='" + messagesUrl + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", lastMessage=" + lastMessage +
                ", participants=" + participants +
                ", isDistinct=" + isDistinct +
                ", unreadCount=" + unreadCount +
                '}';
    }
}
