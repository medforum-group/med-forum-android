package net.styleru.i_komarov.medforum.presenter.registration.main;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.view.registration.main.IRegistrationView;

/**
 * Created by i_komarov on 06.01.17.
 */

public class RegistrationPresenter extends Presenter<IRegistrationView> {

    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onDestroy() {

    }


}
