package net.styleru.i_komarov.medforum.view.main.conversations;

import android.os.Parcel;
import android.os.Parcelable;

import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationViewEntity implements Parcelable {

    private String uri;
    private Long unreadMessagesCount;
    private String preview;
    private String lastUpdate;
    private List<ParticipantViewEntity> participants;

    public ConversationViewEntity(Parcel in) {
        this.uri = in.readString();
        this.unreadMessagesCount = in.readLong();
        this.preview = in.readString();
        this.lastUpdate = in.readString();
        in.readList(participants, List.class.getClassLoader());
    }

    public ConversationViewEntity(String uri, String preview, String lastUpdate, Long unreadMessagesCount, List<ParticipantViewEntity> participants) {
        this.uri = uri;
        this.preview = preview;
        this.lastUpdate = lastUpdate;
        this.unreadMessagesCount = unreadMessagesCount == null ? 0L : unreadMessagesCount;
        this.participants = participants;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getUnreadMessagesCount() {
        return unreadMessagesCount;
    }

    public void setUnreadMessagesCount(Long unreadMessagesCount) {
        this.unreadMessagesCount = unreadMessagesCount;
    }

    public List<ParticipantViewEntity> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ParticipantViewEntity> participants) {
        this.participants = participants;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(uri);
        dest.writeLong(unreadMessagesCount == null ? 0L : unreadMessagesCount);
        dest.writeString(preview);
        dest.writeString(lastUpdate);
        dest.writeList(participants);
    }

    public static final Parcelable.Creator<ConversationViewEntity> CREATOR = new Creator<ConversationViewEntity>() {
        @Override
        public ConversationViewEntity createFromParcel(Parcel in) {
            return new ConversationViewEntity(in);
        }

        @Override
        public ConversationViewEntity[] newArray(int size) {
            return new ConversationViewEntity[0];
        }
    };
}
