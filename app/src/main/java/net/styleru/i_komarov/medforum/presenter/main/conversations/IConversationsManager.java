package net.styleru.i_komarov.medforum.presenter.main.conversations;

import net.styleru.i_komarov.medforum.view.main.conversations.ConversationViewEntity;

import java.util.List;

/**
 * Created by i_komarov on 23.01.17.
 */

public interface IConversationsManager {

    void add(ConversationViewEntity item);

    void add(List<ConversationViewEntity> items);

    void addTop(ConversationViewEntity item);

    void addTop(List<ConversationViewEntity> items);

    void pushTop(ConversationViewEntity item);

    void pushTop(List<ConversationViewEntity> items);

    String lastUri();

    String firstUri();
}
