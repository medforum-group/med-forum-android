package net.styleru.i_komarov.medforum.common.view;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 29.11.16.
 */

public interface IConnectionInteract {

    void showConnectingDialog();

    void hideConnectingDialog();;

    void showConnectionError();

    void showConenctionError(@StringRes int message);

    void showConnectionError(String message);
}
