package net.styleru.i_komarov.medforum.model.database.core;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 14.01.17.
 */

public class DatabaseConfig {

    @NonNull
    public static final String DATABASE_NAME = "cache_persistent";

    @NonNull
    public static final int SCHEMA_VERSION = 1;
}
