package net.styleru.i_komarov.medforum.model.layer.ws.core;

/**
 * Created by i_komarov on 23.01.17.
 */

public interface IWSListener {

    void onMessage(String message);
}
