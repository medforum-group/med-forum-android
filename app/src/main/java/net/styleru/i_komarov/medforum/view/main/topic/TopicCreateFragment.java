package net.styleru.i_komarov.medforum.view.main.topic;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.topics.TopicCreatePresenter;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 09.01.17.
 */

public class TopicCreateFragment extends RetainPresenterFragment<ITopicCreateView, TopicCreatePresenter> implements ITopicCreateView {

    private static final String STATE_CATEGORY_ID = TopicCreateFragment.class.getCanonicalName() + ".CATEGORY_ID";

    private AppCompatEditText topicTitleHolder;
    private AppCompatEditText topicContentHolder;
    private AppCompatButton   createTopicButton;

    private String categoryID;

    public static Fragment newInstance(String categoryID) {
        Bundle args = new Bundle();
        args.putString(STATE_CATEGORY_ID, categoryID);
        Fragment fragment = new TopicCreateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null && getArguments().containsKey(STATE_CATEGORY_ID)) {
            categoryID = getArguments().getString(STATE_CATEGORY_ID);
        } else if(savedInstanceState != null && savedInstanceState.containsKey(STATE_CATEGORY_ID)) {
            categoryID = savedInstanceState.getString(STATE_CATEGORY_ID);
        } else {
            throw new RuntimeException("categoryID was not found neither in arguments nor in savedInsatnceState");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_topic_add, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        topicTitleHolder   = (AppCompatEditText) view.findViewById(R.id.fragment_topic_add_title_holder);
        topicContentHolder = (AppCompatEditText) view.findViewById(R.id.fragment_topic_add_content_holder);
        createTopicButton  = (AppCompatButton) view.findViewById(R.id.fragment_topic_add_create_question_button);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.init(categoryID);
    }

    @Override
    public Observable<String[]> getCreateTopicButtonClicksObservable() {
        return RxView.clicks(createTopicButton)
                .map(value -> new String[]{
                        topicTitleHolder.getText().toString(),
                        topicContentHolder.getText().toString()
                });
    }

    @Override
    public void onTopicCreationSucceeded() {
        getActivity().onBackPressed();
    }

    @Override
    public void onTopicCreationFailed() {
        //TODO: how, god damn it, this could be triggered ?!
    }

    @Override
    protected PresenterLoader.Info<ITopicCreateView> getLoaderInfo() {
        return new PresenterLoader.Info<ITopicCreateView>() {
            @Override
            public int getID() {
                return LOADER_TOPIC_CREATE;
            }

            @Override
            public Presenter.Factory<ITopicCreateView> getFactory() {
                return new Presenter.Factory<ITopicCreateView>() {
                    @Override
                    public <P extends Presenter<ITopicCreateView>> P create() {
                        return (P) new TopicCreatePresenter();
                    }
                };
            }

            @Override
            public ITopicCreateView getView() {
                return TopicCreateFragment.this;
            }
        };
    }
}
