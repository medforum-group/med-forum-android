package net.styleru.i_komarov.medforum.view.main.profile;

import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 08.01.17.
 */

public interface IProfileView {

    public void onSignOutSucceeded();

    public void onSignOutFailed();

    public void displayUserProfile(ParticipantViewEntity participant);

    public Observable<Integer> getSignOutButtonClicksObservable();
}
