package net.styleru.i_komarov.medforum.model.layer.dto;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.layer.sdk.messaging.MessagePart;

import java.util.ArrayList;
import java.util.List;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerMessagePreferences {

    @SerializedName(FIELD_PARTS)
    private List<LayerMessagePart> parts;

    public LayerMessagePreferences() {
        this.parts = new ArrayList<>();
    }

    public LayerMessagePreferences(@NonNull List<LayerMessagePart> parts) {
        this.parts = parts;
    }

    public LayerMessagePreferences addPart(@NonNull LayerMessagePart part) {
        this.parts.add(part);
        return this;
    }
}
