package net.styleru.i_komarov.medforum.presenter.main.discussion;

import android.util.Log;

import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.api.dao.AnswersProxy;
import net.styleru.i_komarov.medforum.model.api.dao.QuestionsProxy;
import net.styleru.i_komarov.medforum.model.cache.PreferencesManager;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.AnswerServerEntity;
import net.styleru.i_komarov.medforum.presenter.mapper.AnswersMapper;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.topic.CommentViewEntity;
import net.styleru.i_komarov.medforum.view.main.topic.DiscussionViewEntity;
import net.styleru.i_komarov.medforum.view.main.topic.IDiscussionView;

import org.reactivestreams.Publisher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_OK;
import static net.styleru.i_komarov.medforum.model.common.Values.VALUE_EXPERT;

/**
 * Created by i_komarov on 10.12.16.
 */

public class DiscussionPresenter extends Presenter<IDiscussionView> {

    private String categoryID;
    private String questionID;

    public void init(String categoryID, String questionID) {
        this.categoryID = categoryID;
        this.questionID = questionID;
    }

    public void loadDiscussionHeader() {
        QuestionsProxy.getInstance().get(questionID)
                .subscribeOn(Schedulers.io())
                .map(Response::body)
                .map(ResponseWrapper::getBody)
                .filter(list -> list != null && list.size() != 0)
                .map(list -> list.get(0))
                .map(question -> new DiscussionViewEntity(question.getId(), question.getTitle(), question.getContent(), question.getCount(), question.getImageUrl(), question.getFirstName(), question.getLastName(), question.getRole() != null && question.getRole().equals(VALUE_EXPERT)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createDiscussionContentSubscriber());
    }

    @Override
    public void onStart() {

    }

    @Override
    public void bindView(IDiscussionView view) {
        super.bindView(view);
        subscribeToSendCommentButtonClicksObservable(view.getSendButtonClicksObservable());
        subscribeToCommentInputTextChangeEventsObservable(view.getCommentInputChangeEventObservable());
    }

    @Override
    public void unbindView() {
        super.unbindView();
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onStop() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    @Override
    public void onDestroy() {
        disposeAllUIObservers();
        disposeAllUISubscribers();
    }

    public Flowable<List<CommentViewEntity>> provideDataChannel(Flowable<State> listStateChannel) {
        return listStateChannel.flatMap(
                new Function<State, Publisher<List<AnswerServerEntity>>>() {
                    @Override
                    public Publisher<List<AnswerServerEntity>> apply(State state) throws Exception {
                        return AnswersProxy.getInstance().list(questionID, state.getOffset(), state.getLimit())
                                .subscribeOn(Schedulers.io())
                                .filter(response -> hookHttpCode(response.code()))
                                .filter(response -> response != null && response.body() != null)
                                .map(Response::body)
                                .filter(wrapper -> wrapper != null && wrapper.getBody() != null && wrapper.getBody().size() != 0)
                                .map(ResponseWrapper::getBody);
                    }
                })
                .map(new AnswersMapper())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void subscribeToSendCommentButtonClicksObservable(Observable<String> sendCommentButtonClicksObservable) {
        sendCommentButtonClicksObservable
                .flatMap(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(String content) throws Exception {
                        return AnswersProxy.getInstance().reply(PreferencesManager.getInstance().loadUserID(), questionID, content)
                                .subscribeOn(Schedulers.io())
                                .filter(response -> hookHttpCode(response.code()))
                                .map(response -> content)
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                })
                .subscribe(createSendCommentButtonClicksObserver());
    }

    private void subscribeToCommentInputTextChangeEventsObservable(Observable<String> commentInputTextChangeEventsObservable) {
        commentInputTextChangeEventsObservable.subscribe(createCommentInputTextChangeEventObserver());
    }

    private DisposableObserver<DiscussionViewEntity> createDiscussionContentSubscriber() {
        DisposableObserver<DiscussionViewEntity> discussionContentSubscriber = new DisposableObserver<DiscussionViewEntity>() {
            @Override
            public void onNext(DiscussionViewEntity discussionViewEntity) {
                view.displayDiscussionHeader(discussionViewEntity);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(discussionContentSubscriber);

        return discussionContentSubscriber;
    }

    private DisposableObserver<String> createSendCommentButtonClicksObserver() {
        DisposableObserver<String> sendCommentButtonClicksObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String content) {
                view.onCommentAdded(new CommentViewEntity("1", content, null, PreferencesManager.getInstance().loadUserImageUrl(), PreferencesManager.getInstance().loadUserFirstName(), PreferencesManager.getInstance().loadUserLastName(), PreferencesManager.getInstance().loadUserRole().equals("expert")));
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(sendCommentButtonClicksObserver);

        return sendCommentButtonClicksObserver;
    }

    private DisposableObserver<String> createCommentInputTextChangeEventObserver() {
        DisposableObserver<String> commentInputTextChangeEventObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String currentText) {
                //TODO: maybe attach some suggestions neurons-net here ?)
                Log.d("DiscussionPresenter", "comment text is now: " + currentText);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };

        addObserver(commentInputTextChangeEventObserver);

        return commentInputTextChangeEventObserver;
    }

    private boolean hookHttpCode(int httpCode) {
        if(httpCode == HTTP_OK) {
            return true;
        } else  {
            return false;
        }
    }
}
