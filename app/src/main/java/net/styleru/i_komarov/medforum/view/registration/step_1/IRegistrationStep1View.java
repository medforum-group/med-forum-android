package net.styleru.i_komarov.medforum.view.registration.step_1;

import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 06.01.17.
 */

public interface IRegistrationStep1View {

    public Observable<UserServerEntity> getRegisterButtonClicksObservable();

    public Observable<Integer> getAttachPhotoButtonClickObservable();

    public void onPhotoPickerStartRequest();

    public void onRegistrationSucceeded(UserServerEntity user);

    public void onRegistrationFailed();
}
