package net.styleru.i_komarov.medforum.ui.recyclerview;

/**
 * Created by i_komarov on 29.11.16.
 */

public class State {

    private int offset;
    private int limit;

    public State(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}

