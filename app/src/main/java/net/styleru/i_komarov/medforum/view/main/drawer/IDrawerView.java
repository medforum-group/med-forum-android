package net.styleru.i_komarov.medforum.view.main.drawer;

import android.app.Fragment;
import android.content.Intent;
import android.view.MenuItem;

import net.styleru.i_komarov.medforum.common.view.IConnectionInteract;
import net.styleru.i_komarov.medforum.common.view.IErrorInteract;
import net.styleru.i_komarov.medforum.common.view.IMessageInteract;
import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public interface IDrawerView extends IConnectionInteract, IErrorInteract, IMessageInteract {

    public boolean isAuthenticated();

    public void onNavigationClicked();

    public void onNavigationViewItemSelected(MenuItem item);

    public void onAddFragmentInitiated(boolean addToBackStack, Fragment fragment);

    public void startActivity(Intent intent);

    public Observable<Integer> getToolbarNavigationClickEventChannel();

    public Observable<MenuItem> getNavigationItemClickEventChannel();
}
