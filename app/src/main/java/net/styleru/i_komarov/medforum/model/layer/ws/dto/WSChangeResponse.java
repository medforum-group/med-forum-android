package net.styleru.i_komarov.medforum.model.layer.ws.dto;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import net.styleru.i_komarov.medforum.model.layer.ws.dto.LayerObject;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 23.01.17.
 */

public class WSChangeResponse {

    @SerializedName(FIELD_OPERATION)
    private String operation;
    @SerializedName(FIELD_OBJECT)
    private LayerObject object;
    @SerializedName(FIELD_DATA)
    private JsonObject data;

    public WSChangeResponse(String operation, LayerObject object, JsonObject data) {
        this.operation = operation;
        this.object = object;
        this.data = data;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public LayerObject getObject() {
        return object;
    }

    public void setObject(LayerObject object) {
        this.object = object;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "WSChangeResponse{" +
                "operation='" + operation + '\'' +
                ", object=" + object +
                ", data=" + data +
                '}';
    }
}
