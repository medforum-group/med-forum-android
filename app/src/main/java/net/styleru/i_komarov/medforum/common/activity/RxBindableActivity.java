package net.styleru.i_komarov.medforum.common.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by i_komarov on 29.11.16.
 */

public abstract class RxBindableActivity extends AppCompatActivity {

    private LinkedList<Disposable> viewDisposableList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewDisposableList = new LinkedList<>();
    }

    public void addDisposable(Disposable disposable) {
        this.viewDisposableList.addLast(disposable);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(viewDisposableList.size() != 0) {
            for(Disposable disposable : viewDisposableList) {
                if(!disposable.isDisposed()) {
                    disposable.dispose();
                }
            }
        }
    }
}
