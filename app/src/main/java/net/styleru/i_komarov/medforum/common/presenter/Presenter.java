package net.styleru.i_komarov.medforum.common.presenter;

import java.util.LinkedList;
import java.util.Observer;

import io.reactivex.observers.DisposableObserver;
import io.reactivex.subscribers.DisposableSubscriber;

/**
 * Created by i_komarov on 29.11.16.
 */

public abstract class Presenter<V> implements IPresenter<V> {

    protected V view;

    private LinkedList<DisposableObserver> viewEventObserverList;
    private LinkedList<DisposableSubscriber> viewEventSubscriberList;

    public Presenter() {
        viewEventObserverList = new LinkedList<>();
        viewEventSubscriberList = new LinkedList<>();
    }

    @Override
    public void bindView(V view) {
        this.view = view;
    }

    @Override
    public void unbindView() {
        this.view = null;
    }

    protected void addObserver(DisposableObserver observer) {
        this.viewEventObserverList.add(observer);
    }

    protected void removeObserver(DisposableObserver observer) {
        if(!observer.isDisposed()) {
            observer.dispose();
        }
        this.viewEventObserverList.remove(observer);
    }

    protected void addSubscriber(DisposableSubscriber subscriber) {
        this.viewEventSubscriberList.addLast(subscriber);
    }

    protected void removeSubscriber(DisposableSubscriber subscriber) {
        if(!subscriber.isDisposed()) {
            subscriber.dispose();
        }
        this.viewEventSubscriberList.remove(subscriber);
    }

    protected void disposeAllUIObservers() {
        if(this.viewEventObserverList != null && this.viewEventObserverList.size() > 0) {
            for(DisposableObserver observer : this.viewEventObserverList) {
                if(!observer.isDisposed()) {
                    observer.dispose();
                }
            }

            this.viewEventObserverList.clear();
        }
    }

    protected void disposeAllUISubscribers() {
        if(this.viewEventSubscriberList != null && this.viewEventSubscriberList.size() > 0) {
            for(DisposableSubscriber subscriber : this.viewEventSubscriberList) {
                if(!subscriber.isDisposed()) {
                    subscriber.dispose();
                }
            }

            this.viewEventSubscriberList.clear();
        }
    }

    public interface Factory<V> {

        public <P extends Presenter<V>> P create();
    }
}
