package net.styleru.i_komarov.medforum.view.main.conversations;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.model.util.UriUtils;
import net.styleru.i_komarov.medforum.presenter.main.conversations.ConversationsAdapter;
import net.styleru.i_komarov.medforum.presenter.main.conversations.ConversationsPresenter;
import net.styleru.i_komarov.medforum.presenter.main.conversations.IConversationsManager;
import net.styleru.i_komarov.medforum.presenter.main.conversations.LayerListState;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.conversation.ConversationFragment;
import net.styleru.i_komarov.medforum.view.main.drawer.DrawerActivity;
import net.styleru.i_komarov.medforum.view.participants_picker.ParticipantsPickerActivity;
import net.styleru.i_komarov.rxjava2_binding.swipe_refresh_layout.main.RxSwipeRefreshLayout;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

import static android.app.Activity.RESULT_OK;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationsFragment extends RetainPresenterFragment<IConversationsView, ConversationsPresenter> implements IConversationsView {

    private static final int REQUEST_PARTICIPANT = 9917;

    private static final String KEY_STATE_ADAPTER = ConversationsFragment.class.getCanonicalName() + ".STATE_ADAPTER";
    private static final String KEY_STATE_LIST = ConversationsFragment.class.getCanonicalName() + ".STATE_LIST";

    private FloatingActionButton createConversationButton;
    private SwipeRefreshLayout swipeRefresh;
    private RecyclerView list;

    private ConversationsAdapter adapter;

    private Parcelable listState;


    private View root;

    public static Fragment newInstance() {
        Fragment fragment = new ConversationsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversations, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        this.root = view;
        this.createConversationButton = (FloatingActionButton) view.findViewById(R.id.fragment_conversations_create_conversation);
        this.swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.fragment_conversations_swipe_refresh);
        this.list = (RecyclerView) view.findViewById(R.id.fragment_conversations_list);
        this.list.setLayoutManager(new LinearLayoutManager(view.getContext()));
    }

    @Override
    public Observable<Integer> getCreateConversationButtonClicksObservable() {
        return RxView.clicks(createConversationButton);
    }

    @Override
    public Observable<Integer> getRefreshEventsObservable() {
        return RxSwipeRefreshLayout.refreshes(swipeRefresh)
                .doOnNext(new Consumer<Integer>() {
                    @Override
                    public void accept(Integer integer) throws Exception {
                        swipeRefresh.setRefreshing(false);
                    }
                });
    }

    @Override
    public Observable<LayerListState> getListStateChannel() {
        return adapter.createListStateObservable(list);
    }

    @Override
    public void startParticipantsPickerForResult() {
        startActivityForResult(new Intent(getActivity(), ParticipantsPickerActivity.class), REQUEST_PARTICIPANT);
    }

    @Override
    public void onConversationCreated() {
        Toast.makeText(root.getContext(),
                getActivity().getApplicationContext().getString(R.string.snackbar_message_conversation_created),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onConversationAlreadyExists() {
        Toast.makeText(
                root.getContext(),
                getActivity().getApplicationContext().getString(R.string.snackbar_message_conversation_already_exists),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onConversationWithSelfCreateAttempt() {
        Toast.makeText(
                root.getContext(),
                getActivity().getApplicationContext().getString(R.string.snackbar_message_conversation_with_self_restricted),
                Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void updateConversationsAddNew(List<ConversationViewEntity> conversations) {
        adapter.addTop(conversations);
    }

    @Override
    public IConversationsManager getConversationsManager() {
        return adapter;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PARTICIPANT) {
            if(resultCode == RESULT_OK) {
                presenter.createConversation(data.getExtras().getString(ParticipantsPickerActivity.RESULT_PARTICIPANT_ID));
            } else {
                //TODO: participant pick somehow failed or what ?!
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(adapter != null) {
            outState.putParcelable(KEY_STATE_ADAPTER, adapter);
        }
        if(listState != null) {
            outState.putParcelable(KEY_STATE_LIST, listState);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey(KEY_STATE_ADAPTER)) {
                adapter = savedInstanceState.getParcelable(KEY_STATE_ADAPTER);
            }
            if(savedInstanceState.containsKey(KEY_STATE_LIST)) {
                listState = savedInstanceState.getParcelable(KEY_STATE_LIST);
            }
        }

        if(adapter == null) {
            adapter = new ConversationsAdapter();
        }

        list.setAdapter(adapter);

        adapter.setListener(new BindableViewHolder.ActionListener<ConversationViewEntity>() {
            @Override
            public void clicked(int position, ConversationViewEntity item) {
                ((DrawerActivity) getActivity()).onAddFragmentInitiated(true, ConversationFragment.newInstance(UriUtils.extractEndpoint(item.getUri())));
            }

            @Override
            public void longClicked(int position, ConversationViewEntity item) {

            }

            @Override
            public void swiped(int position, int direction) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DrawerActivity) getActivity()).setTitle(R.string.navigation_menu_item_chats);
        ((DrawerActivity) getActivity()).setSubtitle("");
        if(list.getAdapter() == null) {
            if(adapter == null) {
                adapter = new ConversationsAdapter();
            }

            list.setAdapter(adapter);
        }

        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onPause() {
        ((DrawerActivity) getActivity()).setTitle("");
        ((DrawerActivity) getActivity()).setSubtitle("");
        list.setOnScrollListener(null);
        list.setAdapter(null);
        listState = list.getLayoutManager().onSaveInstanceState();
        super.onPause();
    }

    @Override
    protected PresenterLoader.Info<IConversationsView> getLoaderInfo() {
        return new PresenterLoader.Info<IConversationsView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_DIALOGS;
            }

            @Override
            public Presenter.Factory<IConversationsView> getFactory() {
                return new Presenter.Factory<IConversationsView>() {
                    @Override
                    public <P extends Presenter<IConversationsView>> P create() {
                        return (P) new ConversationsPresenter();
                    }
                };
            }

            @Override
            public IConversationsView getView() {
                return ConversationsFragment.this;
            }
        };
    }
}
