package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 03.01.17.
 */

public class BaseUrls {

    public static final String BASE_URL_MEDFORUM = "http://78.155.219.201:3000/diabetics/";
    public static final String BASE_URL_LAYER = "https://api.layer.com/";
}
