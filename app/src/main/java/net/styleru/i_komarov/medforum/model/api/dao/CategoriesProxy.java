package net.styleru.i_komarov.medforum.model.api.dao;

import net.styleru.i_komarov.medforum.model.api.functional.API;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.CategoryServerEntity;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.Response;

/**
 * Created by i_komarov on 03.01.17.
 */

public class CategoriesProxy {

    private static CategoriesProxy instance;

    private API.Categories api;

    private CategoriesProxy() {
        this.api = API.Factory.buildCategoriesAPI();
    }

    public static CategoriesProxy getInstance() {
        CategoriesProxy localInstance = instance;
        if(localInstance == null) {
            synchronized (CategoriesProxy.class) {
                localInstance = instance;
                if(localInstance == null) {
                    localInstance = instance = new CategoriesProxy();
                }
            }
        }

        return localInstance;
    }

    public Flowable<Response<ResponseWrapper<List<CategoryServerEntity>>>> list(int offset, int limit) {
        return api.getList(offset, limit);
    }

    public Flowable<Response<ResponseWrapper<List<CategoryServerEntity>>>> list(int offset, int limit, String filter) {
        return api.getList(offset, limit, filter);
    }
}
