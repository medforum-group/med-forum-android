package net.styleru.i_komarov.medforum.model.api.dao;

import net.styleru.i_komarov.medforum.model.api.functional.API;
import net.styleru.i_komarov.medforum.model.common.ResponseWrapper;
import net.styleru.i_komarov.medforum.model.common.ResultResponseWrapper;
import net.styleru.i_komarov.medforum.model.api.dto.QuestionServerEntity;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 03.01.17.
 */

public class QuestionsProxy {

    private static QuestionsProxy instance;

    private API.Questions api;

    private QuestionsProxy() {
        this.api = API.Factory.buildQuestionsAPI();
    }

    public static QuestionsProxy getInstance() {
        QuestionsProxy localInstance = instance;
        if(localInstance == null) {
            synchronized (QuestionsProxy.class) {
                localInstance = instance;
                if(localInstance == null) {
                    localInstance = instance = new QuestionsProxy();
                }
            }
        }

        return localInstance;
    }

    public Observable<Response<ResponseWrapper<List<QuestionServerEntity>>>> get(String questionID) {
        return api.get(questionID);
    }

    public Flowable<Response<ResponseWrapper<List<QuestionServerEntity>>>> list(String categoryID, int offset, int limit) {
        return api.getList(categoryID, offset, limit);
    }

    public Flowable<Response<ResponseWrapper<List<QuestionServerEntity>>>> list(String categoryID, int offset, int limit, String filter) {
        return api.getList(categoryID, offset, limit, filter);
    }

    public Observable<Response<ResultResponseWrapper>> create(String userID, String categoryID, String title, String content) {
        return api.create(userID, categoryID, title, content);
    }
}
