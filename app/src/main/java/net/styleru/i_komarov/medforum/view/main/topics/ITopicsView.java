package net.styleru.i_komarov.medforum.view.main.topics;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 09.12.16.
 */

public interface ITopicsView {

    public Observable<SearchViewChangeEvent> getSearchViewChangeEventObservable();

    public Observable<Integer> getCreateTopicButtonClicksObservable();

    public void onTopicCreated(TopicViewEntity topic);

    public void refreshAdapter();

    public void onCreateTopicButtonClicked();
}

