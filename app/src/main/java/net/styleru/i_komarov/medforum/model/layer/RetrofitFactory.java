package net.styleru.i_komarov.medforum.model.layer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.i_komarov.medforum.model.api.common.OkHttpClientPreferences;

import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static net.styleru.i_komarov.medforum.app.Versioning.DEBUG;

/**
 * Created by i_komarov on 21.01.17.
 */

public class RetrofitFactory {

    private static final int connectTimeout = 60;
    private static final int readTimeout = 15;
    private static final int writeTimeout = 15;

    private static OkHttpClientPreferences preferences = new OkHttpClientPreferences();

    public static Retrofit buildWithInterceptor(String baseUrl, Interceptor interceptor) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(buildClient(interceptor))
                .build();
    }

    public static Retrofit build(String baseUrl) {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();

        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(buildClient())
                .build();
    }

    private static OkHttpClient buildClient(Interceptor... interceptors) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(preferences.getConnectTimeout(), TimeUnit.SECONDS)
                .writeTimeout(preferences.getWriteTimeout(), TimeUnit.SECONDS)
                .readTimeout(preferences.getConnectTimeout(), TimeUnit.SECONDS);
        if(DEBUG) {
            builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }

        if(interceptors != null && interceptors.length > 0) {
            for (int i = 0; i < interceptors.length; i++) {
                builder.addInterceptor(interceptors[i]);
            }
        }

        return builder.build();
    }
}
