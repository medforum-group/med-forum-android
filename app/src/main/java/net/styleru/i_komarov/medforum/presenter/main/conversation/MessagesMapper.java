package net.styleru.i_komarov.medforum.presenter.main.conversation;

import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessageEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessagePart;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerParticipantEntity;
import net.styleru.i_komarov.medforum.view.main.conversation.ContentType;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageType;
import net.styleru.i_komarov.medforum.view.main.conversation.MessageViewEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.01.17.
 */

public class MessagesMapper implements Function<List<LayerMessageEntity>, List<MessageViewEntity>> {

    private String userID;

    public MessagesMapper(String userID) {
        this.userID = userID;
    }

    @Override
    public List<MessageViewEntity> apply(List<LayerMessageEntity> layerMessageEntities) throws Exception {
        return Observable.fromIterable(layerMessageEntities)
                .map(new Function<LayerMessageEntity, MessageViewEntity>() {
                    @Override
                    public MessageViewEntity apply(LayerMessageEntity msg) throws Exception {
                        LayerMessagePart textPart = msg.getTextMessagePart();

                        LayerParticipantEntity sender = msg.getSender();
                        String[] name;
                        String firstName = "";
                        String lastName = "";
                        String photoUrl = "";

                        if(sender != null) {
                            if(sender.getDisplayName() != null) {
                                name = sender.getDisplayName().split(" ");

                                firstName = name[0];
                                lastName = name[1];
                            }

                            photoUrl = sender.getPhotoUrl();
                        }

                        return new MessageViewEntity(
                                ContentType.TEXT,
                                msg.getSender().getUserID().equals(userID) ? MessageType.FROM_USER : MessageType.TO_USER, msg.getId(),
                                firstName,
                                lastName,
                                photoUrl,
                                textPart.getBody()
                        );
                    }
                })
                .toList()
                .blockingGet();
    }
}
