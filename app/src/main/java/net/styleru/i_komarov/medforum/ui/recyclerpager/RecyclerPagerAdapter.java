package net.styleru.i_komarov.medforum.ui.recyclerpager;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.LayoutRes;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by henrytao on 11/13/15.
 */
public class RecyclerPagerAdapter<T> extends PagerAdapter {

    private static final String STATE = RecyclerPagerAdapter.class.getCanonicalName();

    private SparseArray<RecycleCache> mRecycleTypeCaches = new SparseArray<>();

    private SparseArray<Parcelable> mSavedStates = new SparseArray<>();

    @LayoutRes
    private final int layoutRes;
    private final Class<? extends ViewHolder> vhClass;

    private List<ViewHolder> staticPool;

    public boolean useRecycling = true;

    public void setUseRecycling(boolean useRecycling) {
        this.useRecycling = useRecycling;
    }

    private List<T> items;

    public RecyclerPagerAdapter(int layoutRes, Class<? extends ViewHolder> vhClass) {
        this.layoutRes = layoutRes;
        this.vhClass = vhClass;
        this.items = new ArrayList<>();
        this.staticPool = new ArrayList<>();
    }

    @Override
    public void destroyItem(ViewGroup parent, int position, Object object) {
        if (object instanceof ViewHolder) {
            if(useRecycling) {
                ((ViewHolder) object).detach(parent);
            }
        }
    }

    public int getItemCount() {
        return this.items.size();
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(position, items.get(position));
    }

    public void add(T item) {
        this.items.add(item);
        notifyDataSetChanged();
    }

    public void remove(T item) {
        this.items.remove(item);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        this.items.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.items.clear();
        notifyDataSetChanged();
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            return vhClass.getConstructor(View.class).newInstance(LayoutInflater.from(parent.getContext()).inflate(layoutRes, parent, false));
        } catch (InstantiationException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getCount() {
        return getItemCount();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object instantiateItem(ViewGroup parent, int position) {
        if(useRecycling) {
            int viewType = getItemViewType(position);
            if (mRecycleTypeCaches.get(viewType) == null) {
                mRecycleTypeCaches.put(viewType, new RecycleCache(this));
            }
            ViewHolder viewHolder = mRecycleTypeCaches.get(viewType).getFreeViewHolder(parent, viewType);
            viewHolder.attach(parent, position);
            onBindViewHolder(viewHolder, position);
            viewHolder.onRestoreInstanceState(mSavedStates.get(getItemId(position)));
            return viewHolder;
        } else {
            if(staticPool.size() > position) {
                Log.d("RecyclerPagerAdapter", "vh from static pool returned");
                return staticPool.get(position);
            } else {
                Log.d("RecyclerPagerAdapter", "vh from static pool was not found, one was created at: " + position);
                ViewHolder viewHolder = onCreateViewHolder(parent, position);
                staticPool.add(viewHolder);
                viewHolder.attach(parent, position);
                onBindViewHolder(viewHolder, position);
                viewHolder.onRestoreInstanceState(mSavedStates.get(getItemId(position)));
                return viewHolder;
            }
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object instanceof ViewHolder && ((ViewHolder) object).itemView == view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        for (ViewHolder viewHolder : getAttachedViewHolders()) {
            onNotifyItemChanged(viewHolder);
        }
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            bundle.setClassLoader(loader);
            SparseArray<Parcelable> ss = bundle.containsKey(STATE) ? bundle.getSparseParcelableArray(STATE) : null;
            mSavedStates = ss != null ? ss : new SparseArray<Parcelable>();
        }
        super.restoreState(state, loader);
    }

    @Override
    public Parcelable saveState() {
        Bundle bundle = new Bundle();
        for (ViewHolder viewHolder : getAttachedViewHolders()) {
            mSavedStates.put(getItemId(viewHolder.mPosition), viewHolder.onSaveInstanceState());
        }
        bundle.putSparseParcelableArray(STATE, mSavedStates);
        return bundle;
    }

    public int getItemId(int position) {
        return position;
    }

    public int getItemViewType(int position) {
        return 0;
    }

    protected void onNotifyItemChanged(ViewHolder viewHolder) {

    }

    private List<ViewHolder> getAttachedViewHolders() {
        List<ViewHolder> attachedViewHolders = new ArrayList<>();
        int n = mRecycleTypeCaches.size();
        for (int i = 0; i < n; i++) {
            for (ViewHolder viewHolder : mRecycleTypeCaches.get(mRecycleTypeCaches.keyAt(i)).mCaches) {
                if (viewHolder.mIsAttached) {
                    attachedViewHolders.add(viewHolder);
                }
            }
        }
        return attachedViewHolders;
    }

    private static class RecycleCache {

        private final RecyclerPagerAdapter mAdapter;

        private final List<ViewHolder> mCaches;

        RecycleCache(RecyclerPagerAdapter adapter) {
            mAdapter = adapter;
            mCaches = new ArrayList<>();
        }

        ViewHolder getFreeViewHolder(ViewGroup parent, int viewType) {
            int i = 0;
            ViewHolder viewHolder;
            for (int n = mCaches.size(); i < n; i++) {
                viewHolder = mCaches.get(i);
                if (!viewHolder.mIsAttached) {
                    return viewHolder;
                }
            }
            viewHolder = mAdapter.onCreateViewHolder(parent, viewType);
            mCaches.add(viewHolder);
            return viewHolder;
        }
    }

    public static abstract class ViewHolder<T> {

        private static final String STATE = ViewHolder.class.getSimpleName();

        public final View itemView;

        private boolean mIsAttached;

        private int mPosition;

        public ViewHolder(View itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemView should not be null");
            }
            this.itemView = itemView;
        }

        public abstract void bind(int position, T item);

        private void attach(ViewGroup parent, int position) {
            mIsAttached = true;
            mPosition = position;
            parent.addView(itemView);
        }

        private void detach(ViewGroup parent) {
            parent.removeView(itemView);
            mIsAttached = false;
        }

        protected void onRestoreInstanceState(Parcelable state) {
            if (state instanceof Bundle) {
                Bundle bundle = (Bundle) state;
                SparseArray<Parcelable> ss = bundle.containsKey(STATE) ? bundle.getSparseParcelableArray(STATE) : null;
                if (ss != null) {
                    itemView.restoreHierarchyState(ss);
                }
            }
        }

        protected Parcelable onSaveInstanceState() {
            SparseArray<Parcelable> state = new SparseArray<>();
            itemView.saveHierarchyState(state);
            Bundle bundle = new Bundle();
            bundle.putSparseParcelableArray(STATE, state);
            return bundle;
        }
    }
}
