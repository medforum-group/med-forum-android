package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 03.01.17.
 */

public class Endpoints {

    public static final String ENDPOINT_USERS = "users";
    public static final String ENDPOINT_CATEGORIES = "categories";
    public static final String ENDPOINT_QUESTIONS = "questions";
    public static final String ENDPOINT_QUESTION = "question";
    public static final String ENDPOINT_ANSWERS = "answers";
    public static final String ENDPOINT_ANSWER = "answer";
    public static final String ENDPOINT_CODE = "getcode";
    public static final String ENDPOINT_AUTHORIZE = "authorise";
    public static final String ENDPOINT_REGISTRATION = "registration";
    public static final String ENDPOINT_TOKEN = "token";
}
