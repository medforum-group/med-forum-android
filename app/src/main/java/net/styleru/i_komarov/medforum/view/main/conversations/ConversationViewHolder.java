package net.styleru.i_komarov.medforum.view.main.conversations;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.ui.recyclerview.BindableViewHolder;
import net.styleru.i_komarov.medforum.view.main.participants.ParticipantViewEntity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationViewHolder extends BindableViewHolder<ConversationViewEntity, BindableViewHolder.ActionListener<ConversationViewEntity>> {

    //private CircleImageView avatarHolder;
    private AppCompatTextView titleHolder;
    private AppCompatTextView lastMessageHolder;
    private AppCompatTextView timeHolder;

    public ConversationViewHolder(View view) {
        super(view);

        //avatarHolder = (CircleImageView) view.findViewById(R.id.list_item_conversation_avatar);
        titleHolder = (AppCompatTextView) view.findViewById(R.id.list_item_conversation_title);
        lastMessageHolder = (AppCompatTextView) view.findViewById(R.id.list_item_conversation_last_message);
        timeHolder = (AppCompatTextView) view.findViewById(R.id.list_item_conversation_time);
    }

    @Override
    public void bind(int position, ConversationViewEntity item, ActionListener<ConversationViewEntity> listener) {
        super.bind(position, item, listener);

        //avatarHolder.setImageDrawable(null);
        titleHolder.setText(getTitle(item.getParticipants()));
        timeHolder.setText(parseDate(item.getLastUpdate()));
        lastMessageHolder.setText(item.getPreview());
    }

    private String parseDate(String date) {
        try {
            Date dto = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dto);
            return calendar.get(Calendar.DAY_OF_MONTH) + "." + (calendar.get(Calendar.MONTH) + 1) + "." + calendar.get(Calendar.YEAR) + " в " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
        } catch (Exception e) {
            e.printStackTrace();
            return date;
        }
    }

    private String getTitle(List<ParticipantViewEntity> participants) {
        String title = "";
        for(int i = 0; i < participants.size(); i++) {
            if(i != 0) {
                title += ", ";
            }

            title += participants.get(i).getFirstName() + " " + participants.get(i).getLastName();
        }

        return title;
    }
}
