package net.styleru.i_komarov.medforum.model.layer.repository;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessageEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessagePreferences;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static net.styleru.i_komarov.medforum.model.common.LayerEndpoints.*;
import static net.styleru.i_komarov.medforum.model.common.LayerPaths.*;
import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public interface MessagesRepository {

    @Headers("Accept: application/vnd.layer+json; version=2.0")
    @GET(ENDPOINT_CONVERSATIONS + "/{" + PATH_CONVERSATION_URI + "}/" + ENDPOINT_MESSAGES)
    Observable<Response<List<LayerMessageEntity>>> list(
            @Path(PATH_CONVERSATION_URI) String conversationUri,
            @Query(FIELD_PAGE_SIZE) int amount
    );

    @Headers("Accept: application/vnd.layer+json; version=2.0")
    @GET(ENDPOINT_CONVERSATIONS + "/{" + PATH_CONVERSATION_URI + "}/" + ENDPOINT_MESSAGES)
    Observable<Response<List<LayerMessageEntity>>> list(
            @Path(PATH_CONVERSATION_URI) String conversationUri,
            @Query(FIELD_FROM_ID) String fromID,
            @Query(FIELD_PAGE_SIZE) int amount
    );

    @Headers("Accept: application/vnd.layer+json; version=2.0")
    @POST(ENDPOINT_CONVERSATIONS + "/{" + PATH_CONVERSATION_URI + "}/" + ENDPOINT_MESSAGES)
    Observable<Response<LayerMessageEntity>> create(
            @Path(PATH_CONVERSATION_URI) String conversationUri,
            @Body LayerMessagePreferences message
    );
}
