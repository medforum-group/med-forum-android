package net.styleru.i_komarov.medforum.view.main.conversation;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.main.conversation.ConversationPresenter;
import net.styleru.i_komarov.medforum.presenter.main.conversation.MessagesAdapter;
import net.styleru.i_komarov.medforum.ui.recyclerview.State;
import net.styleru.i_komarov.medforum.view.main.drawer.DrawerActivity;
import net.styleru.i_komarov.rxjava2_binding.text_input.main.RxEditText;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by i_komarov on 22.01.17.
 */

public class ConversationFragment extends RetainPresenterFragment<IConversationView, ConversationPresenter> implements IConversationView {

    private static final String STATE_CONVERSATION_URI = ConversationFragment.class.getCanonicalName() + ".STATE_CONVERSATION_URI";
    private static final String STATE_ADAPTER = ConversationFragment.class.getCanonicalName() + ".STATE_ADAPTER";
    private static final String STATE_LIST = ConversationFragment.class.getCanonicalName() + ".STATE_LIST";

    private RecyclerView list;
    private SwipeRefreshLayout refresh;
    private AppCompatEditText messageInput;
    private AppCompatImageButton sendButton;

    private Parcelable listState;

    private MessagesAdapter adapter;

    private String conversationUri;

    public static Fragment newInstance(String conversationUri) {
        Fragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putString(STATE_CONVERSATION_URI, conversationUri);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.containsKey(STATE_CONVERSATION_URI)) {
            this.conversationUri = savedInstanceState.getString(STATE_CONVERSATION_URI);
        } else if(getArguments() != null && getArguments().containsKey(STATE_CONVERSATION_URI)) {
            this.conversationUri = getArguments().getString(STATE_CONVERSATION_URI);
        } else {
            throw new IllegalStateException("conversation uri was not found neither in arguments nor in saved state");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conversation, parent, false);
    }

    @Override
    protected void bindViewComponents(View view) {
        list         = (RecyclerView) view.findViewById(R.id.fragment_conversation_list);
        refresh      = (SwipeRefreshLayout) view.findViewById(R.id.fragment_conversation_swipe_refresh);
        messageInput = (AppCompatEditText) view.findViewById(R.id.fragment_conversation_content_input_holder);
        sendButton   = (AppCompatImageButton) view.findViewById(R.id.fragment_conversation_send_content_button);

        refresh.setEnabled(false);

        refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(refresh != null) {
                    refresh.setRefreshing(false);
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        layoutManager.setReverseLayout(true);
        list.setLayoutManager(layoutManager);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((DrawerActivity) getActivity()).setTitle(R.string.navigation_menu_item_chats);
        ((DrawerActivity) getActivity()).setSubtitle("");
        if(listState != null) {
            list.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onPause() {
        ((DrawerActivity) getActivity()).setTitle("");
        ((DrawerActivity) getActivity()).setSubtitle("");
        listState = list.getLayoutManager().onSaveInstanceState();
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(listState != null) {
            outState.putParcelable(STATE_LIST, listState);
        }
        if(adapter != null) {
            outState.putParcelable(STATE_ADAPTER, adapter);
        }
        if(conversationUri != null) {
            outState.putString(STATE_CONVERSATION_URI, conversationUri);
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            if(savedInstanceState.containsKey(STATE_LIST)) {
                listState = savedInstanceState.getParcelable(STATE_LIST);
            }
            if(savedInstanceState.containsKey(STATE_ADAPTER)) {
                adapter = savedInstanceState.getParcelable(STATE_ADAPTER);
            }
        }

        if(adapter == null) {
            adapter = new MessagesAdapter();
        }

        list.setAdapter(adapter);
    }

    @Override
    public IMessagesManager getMessagesManager() {
        return adapter;
    }

    @Override
    public Observable<String> getSendContentButtonClicksObservable() {
        return RxView.clicks(sendButton)
                .map(integer -> messageInput.getText().toString())
                .doOnNext(integer -> {
                    messageInput.setText("");
                    ((InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(messageInput.getWindowToken(), 0);
                });
    }

    @Override
    public Observable<State> getListStateObservable() {
        return adapter.createListStateObservable(list);
    }

    @Override
    public Observable<String> getContentTypingEventsObservable() {
        return RxEditText.textChanges(messageInput)
                .doOnNext(charSequence -> sendButton.setEnabled(charSequence.length() != 0))
                .map(CharSequence::toString);
    }

    @Override
    protected PresenterLoader.Info<IConversationView> getLoaderInfo() {
        return new PresenterLoader.Info<IConversationView>() {
            @Override
            public int getID() {
                return LOADER_DIALOG;
            }

            @Override
            public Presenter.Factory<IConversationView> getFactory() {
                return new Presenter.Factory<IConversationView>() {
                    @Override
                    public <P extends Presenter<IConversationView>> P create() {
                        return (P) new ConversationPresenter(conversationUri);
                    }
                };
            }

            @Override
            public IConversationView getView() {
                return ConversationFragment.this;
            }
        };
    }
}
