package net.styleru.i_komarov.medforum.model.layer.dao;

import net.styleru.i_komarov.medforum.model.layer.AuthenticationInterceptor;
import net.styleru.i_komarov.medforum.model.layer.RetrofitFactory;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerConversationEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerConversationPreferences;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessageEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerMessagePreferences;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerResponse;
import net.styleru.i_komarov.medforum.model.layer.repository.ConversationRepository;
import net.styleru.i_komarov.medforum.model.layer.repository.MessagesRepository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.Retrofit;

import static net.styleru.i_komarov.medforum.model.common.BaseUrls.BASE_URL_LAYER;

/**
 * Created by i_komarov on 21.01.17.
 */

public class ConversationProxy {

    public static final String SORTING_LAST_MESSAGE = "last_message";

    private static ConversationProxy instance;

    private ConversationRepository conversations;
    private MessagesRepository messages;

    public ConversationProxy(String accessToken) {
        Retrofit retrofit = RetrofitFactory.buildWithInterceptor(BASE_URL_LAYER, new AuthenticationInterceptor(accessToken));
        this.conversations = retrofit.create(ConversationRepository.class);
        this.messages = retrofit.create(MessagesRepository.class);
    }

    public Observable<Response<List<LayerConversationEntity>>> conversations(String sortBy, String fromUri, int amount) {
        if(fromUri != null) {
            return conversations.list(sortBy, fromUri, amount);
        } else {
            return conversations.list(sortBy, amount);
        }
    }

    public Observable<Response<List<LayerMessageEntity>>> messages(String conversationUri, String fromUri, int amount) {
        if(fromUri == null) {
            return messages.list(conversationUri, amount);
        } else {
            return messages.list(conversationUri, fromUri, amount);
        }
    }

    public Observable<Response<LayerResponse<LayerConversationEntity>>> createConversation(LayerConversationPreferences preferences) {
        return conversations.create(preferences.build());
    }

    public Observable<Response<LayerMessageEntity>> createMessage(String conversationUri, LayerMessagePreferences preferences) {
        return messages.create(conversationUri, preferences);
    }
}
