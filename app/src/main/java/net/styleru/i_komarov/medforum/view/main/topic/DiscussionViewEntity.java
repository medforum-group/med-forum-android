package net.styleru.i_komarov.medforum.view.main.topic;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 10.12.16.
 */

public class DiscussionViewEntity implements Parcelable {

    private String id;
    private String title;
    private String content;
    private int answersCount;
    private String authorImage;
    private String authorFirstName;
    private String authorLastName;
    private Boolean isCompleted;

    public DiscussionViewEntity(Parcel in) {
        this.answersCount = in.readInt();
        this.authorFirstName = in.readString();
        this.authorImage = in.readString();
        this.authorLastName = in.readString();
        this.content = in.readString();
        this.id = in.readString();
        this.isCompleted = in.readByte() == (byte) 1;
        this.title = in.readString();
    }

    public DiscussionViewEntity(String id, String title, String content, int answersCount, String authorImage, String authorFirstName, String authorLastName, Boolean isCompleted) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.answersCount = answersCount;
        this.authorImage = authorImage;
        this.authorFirstName = authorFirstName;
        this.authorLastName = authorLastName;
        this.isCompleted = isCompleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(int answersCount) {
        this.answersCount = answersCount;
    }

    public String getAuthorImage() {
        return authorImage;
    }

    public void setAuthorImage(String authorImage) {
        this.authorImage = authorImage;
    }

    public String getAuthorFirstName() {
        return authorFirstName;
    }

    public void setAuthorFirstName(String authorFirstName) {
        this.authorFirstName = authorFirstName;
    }

    public String getAuthorLastName() {
        return authorLastName;
    }

    public void setAuthorLastName(String authorLastName) {
        this.authorLastName = authorLastName;
    }

    public Boolean getCompleted() {
        return isCompleted;
    }

    public void setCompleted(Boolean completed) {
        isCompleted = completed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(answersCount);
        dest.writeString(authorFirstName);
        dest.writeString(authorImage);
        dest.writeString(authorLastName);
        dest.writeString(content);
        dest.writeString(id);
        dest.writeByte(isCompleted ? (byte) 1 : (byte) 0);
        dest.writeString(title);
    }

    public static Parcelable.Creator<DiscussionViewEntity> CREATOR = new Creator<DiscussionViewEntity>() {
        @Override
        public DiscussionViewEntity createFromParcel(Parcel in) {
            return new DiscussionViewEntity(in);
        }

        @Override
        public DiscussionViewEntity[] newArray(int size) {
            return new DiscussionViewEntity[0];
        }
    };

    @Override
    public String toString() {
        return "DiscussionViewEntity{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", answersCount=" + answersCount +
                ", authorImage='" + authorImage + '\'' +
                ", authorFirstName='" + authorFirstName + '\'' +
                ", authorLastName='" + authorLastName + '\'' +
                ", isCompleted=" + isCompleted +
                '}';
    }
}
