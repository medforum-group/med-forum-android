package net.styleru.i_komarov.medforum.model.api.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.*;

/**
 * Created by i_komarov on 03.01.17.
 */

public class AnswerServerEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_QUESTION_USER_ID)
    private String userID;
    @SerializedName(FIELD_QUESTION_QUESTION_ID)
    private String questionID;
    @SerializedName(FIELD_DATE)
    private String date;
    @SerializedName(FIELD_TEXT)
    private String content;
    @SerializedName(FIELD_NAME)
    private String firstName;
    @SerializedName(FIELD_SURNAME)
    private String lastName;
    @SerializedName(FIELD_IMAGE)
    private String imageUrl;
    @SerializedName(FIELD_ROLE)
    private String role;

    public AnswerServerEntity(String id, String userID, String questionID, String date, String content, String firstName, String lastName, String imageUrl, String role) {
        this.id = id;
        this.userID = userID;
        this.questionID = questionID;
        this.date = date;
        this.content = content;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
