package net.styleru.i_komarov.medforum.view.authorization.entry;

import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.fragment.RetainPresenterFragment;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.presenter.authorization.entry.EntryPresenter;
import net.styleru.i_komarov.medforum.ui.edittext_pattern.PatternedTextWatcher;
import net.styleru.i_komarov.medforum.view.authorization.main.IAuthorizationView;
import net.styleru.i_komarov.rxjava2_binding.view.main.RxView;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.12.16.
 */

public class EntryFragment extends RetainPresenterFragment<IEntryView, EntryPresenter> implements IEntryView {

    private AppCompatEditText phoneInputHolder;
    private AppCompatButton applyPhoneButton;

    public static EntryFragment newInstance() {
        EntryFragment fragment = new EntryFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_authorization_entry, parent, false);
    }

    @Override
    public Observable<String> getPhoneVerificationButtonClickChannel() {
        return RxView.clicks(applyPhoneButton)
                .doOnNext(value -> {
                    phoneInputHolder.setEnabled(false);
                    applyPhoneButton.setEnabled(false);
                })
                .map(value -> phoneInputHolder.getText().toString());
    }

    @Override
    public void onPhoneVerified(String phone) {
        phoneInputHolder.setText("");
        phoneInputHolder.setEnabled(true);
        applyPhoneButton.setEnabled(true);
        ((IAuthorizationView) getActivity()).onPhoneVerified(phone);
    }

    @Override
    public void onPhoneVerificationFailed() {
        phoneInputHolder.setText("");
        phoneInputHolder.setEnabled(true);
        applyPhoneButton.setEnabled(true);
        phoneInputHolder.setError(getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_entry_error_phone_verification_failed));
    }

    @Override
    protected void bindViewComponents(View view) {
        phoneInputHolder = (AppCompatEditText) view.findViewById(R.id.fragment_authorization_entry_enter_phone_field);
        applyPhoneButton = (AppCompatButton) view.findViewById(R.id.fragment_authorization_entry_continue_button);
        phoneInputHolder.addTextChangedListener(new PatternedTextWatcher(getActivity().getApplicationContext().getResources().getString(R.string.fragment_authorization_entry_phone_pattern)));
    }

    @Override
    protected PresenterLoader.Info<IEntryView> getLoaderInfo() {
        return new PresenterLoader.Info<IEntryView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_AUTHORIZATION_ENTRY;
            }

            @Override
            public Presenter.Factory<IEntryView> getFactory() {
                return new Presenter.Factory<IEntryView>() {
                    @Override
                    public <P extends Presenter<IEntryView>> P create() {
                        return (P) new EntryPresenter();
                    }
                };
            }

            @Override
            public IEntryView getView() {
                return EntryFragment.this;
            }
        };
    }
}
