package net.styleru.i_komarov.medforum.model.api.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.Fields.*;
import static net.styleru.i_komarov.medforum.model.common.Values.VALUE_EXPERT;

/**
 * Created by i_komarov on 25.12.16.
 */

public class UserServerEntity implements Parcelable {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_PHONE)
    private String phone;
    @SerializedName(FIELD_NAME)
    private String firstName;
    @SerializedName(FIELD_SURNAME)
    private String lastName;
    @SerializedName(FIELD_IMAGE)
    private String imageUrl;
    @SerializedName(FIELD_ROLE)
    private String role;
    @SerializedName(FIELD_TOKEN)
    private String token;

    public UserServerEntity(Parcel in) {
        id = in.readString();
        phone = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        imageUrl = in.readString();
        role = in.readString();
        token = in.readString();
    }

    public UserServerEntity(String id, String phone, String firstName, String lastName, String imageUrl, String role, String token) {
        this.id = id;
        this.phone = phone;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageUrl = imageUrl;
        this.role = role;
        this.token = token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isExpert() {
        return role.equals(VALUE_EXPERT);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "UserServerEntity{" +
                "id='" + id + '\'' +
                ", phone='" + phone + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", role='" + role + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(phone);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imageUrl);
        dest.writeString(role);
        dest.writeString(token);
    }

    public static final Parcelable.Creator<UserServerEntity> CREATOR = new Creator<UserServerEntity>() {
        @Override
        public UserServerEntity createFromParcel(Parcel in) {
            return new UserServerEntity(in);
        }

        @Override
        public UserServerEntity[] newArray(int size) {
            return new UserServerEntity[0];
        }
    };
}
