package net.styleru.i_komarov.medforum.model.common;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerFields {

    public static final String FIELD_NONCE = "nonce";
    public static final String FIELD_IDENTITY_TOKEN = "identity_token";
    public static final String FIELD_SESSION_TOKEN = "session_token";
    public static final String FIELD_APPLICATION_ID = "app_id";

    public static final String FIELD_DATA = "data";

    public static final String FIELD_PARTICIPANTS = "participants";
    public static final String FIELD_DISTINCT = "distinct";
    public static final String FIELD_METADATA = "metadata";
    public static final String FIELD_ID = "id";
    public static final String FIELD_CODE = "code";
    public static final String FIELD_MESSAGE = "message";

    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_LAST_MESSAGE = "last_message";
    public static final String FIELD_UNREAD_MESSAGES_COUNT = "unread_messages_count";

    public static final String FIELD_URL = "url";
    public static final String FIELD_MESSAGES_URL = "messages_url";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_DISPLAY_NAME = "display_name";
    public static final String FIELD_AVATAR_URL = "avatar_url";

    public static final String FIELD_PAGE_SIZE = "page_size";
    public static final String FIELD_FROM_ID = "from_id";
    public static final String FIELD_SORT_BY = "sort_by";

    public static final String FIELD_DOWNLOAD_URL = "download_url";
    public static final String FIELD_EXPIRATION = "expiration";
    public static final String FIELD_REFRESH_URl = "refresh_url";
    public static final String FIELD_SIZE = "size";

    public static final String FIELD_MIME_TYPE = "mime_type";
    public static final String FIELD_BODY = "body";
    public static final String FIELD_ENCODING = "encoding";
    public static final String FIELD_CONTENT = "content";

    public static final String FIELD_RECEPIENT_STATUS = "recepient_status";
    public static final String FIELD_IS_UNREAD = "is_unread";
    public static final String FIELD_SENDER = "sender";
    public static final String FIELD_SENT_AT = "sent_at";
    public static final String FIELD_PARTS = "parts";
    public static final String FIELD_CONVERSATION = "conversation";
    public static final String FIELD_POSITION = "position";
    public static final String FIELD_RECEIPTS_URL = "receipts_url";

    public static final String FIELD_TITLE = "title";

    public static final String FIELD_TYPE = "type";
    public static final String FIELD_TIMESTAMP = "timestamp";
    public static final String FIELD_COUNTER = "counter";

    public static final String FIELD_OPERATION = "operation";
    public static final String FIELD_OBJECT = "object";
}
