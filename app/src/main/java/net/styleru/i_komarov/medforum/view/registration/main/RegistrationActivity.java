package net.styleru.i_komarov.medforum.view.registration.main;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

import net.styleru.i_komarov.medforum.R;
import net.styleru.i_komarov.medforum.common.activity.PresenterActivity;
import net.styleru.i_komarov.medforum.common.presenter.Presenter;
import net.styleru.i_komarov.medforum.model.cache.PresenterLoader;
import net.styleru.i_komarov.medforum.model.api.dto.UserServerEntity;
import net.styleru.i_komarov.medforum.presenter.registration.main.RegistrationPresenter;
import net.styleru.i_komarov.medforum.view.registration.step_1.RegistrationStep1Fragment;

import static net.styleru.i_komarov.medforum.view.authorization.main.AuthorizationActivity.INTENT_KEY_AUTHORIZATION_RESULT;

/**
 * Created by i_komarov on 06.01.17.
 */

public class RegistrationActivity extends PresenterActivity<IRegistrationView, RegistrationPresenter> implements IRegistrationView {

    public static int REQUEST_CODE_REGISTRATION = 8990;

    private Fragment fragment;

    private UserServerEntity currentUser = null;

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        if(getIntent() != null && getIntent().hasExtra(INTENT_KEY_AUTHORIZATION_RESULT)) {
            currentUser = getIntent().getParcelableExtra(INTENT_KEY_AUTHORIZATION_RESULT);
        } else if(savedInstanceState != null && savedInstanceState.containsKey(INTENT_KEY_AUTHORIZATION_RESULT)) {
            currentUser = savedInstanceState.getParcelable(INTENT_KEY_AUTHORIZATION_RESULT);
        } else {
            throw new RuntimeException("User was not found neither in incoming intent nor in saved state!");
        }

        if(getFragmentManager().findFragmentById(R.id.activity_registration_screen_primary) == null) {
            fragment = RegistrationStep1Fragment.newInstance(currentUser);
            getFragmentManager().beginTransaction().replace(R.id.activity_registration_screen_primary, fragment).commit();
        } else {
            fragment = getFragmentManager().findFragmentById(R.id.activity_authorization_screen_primary);
        }
    }

    @Override
    public void onRegistrationSucceeded(UserServerEntity user) {
        Intent data = new Intent();
        data.putExtra(INTENT_KEY_AUTHORIZATION_RESULT, user);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onRegistrationFailed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(INTENT_KEY_AUTHORIZATION_RESULT, currentUser);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected PresenterLoader.Info<IRegistrationView> getLoaderInfo() {
        return new PresenterLoader.Info<IRegistrationView>() {
            @Override
            public int getID() {
                return PresenterLoader.Info.LOADER_REGISTRATION;
            }

            @Override
            public Presenter.Factory<IRegistrationView> getFactory() {
                return new Presenter.Factory<IRegistrationView>() {
                    @Override
                    public <P extends Presenter<IRegistrationView>> P create() {
                        return (P) new RegistrationPresenter();
                    }
                };
            }

            @Override
            public IRegistrationView getView() {
                return RegistrationActivity.this;
            }
        };
    }
}
