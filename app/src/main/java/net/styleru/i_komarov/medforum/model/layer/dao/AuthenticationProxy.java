package net.styleru.i_komarov.medforum.model.layer.dao;

import net.styleru.i_komarov.medforum.app.MedForumApp;
import net.styleru.i_komarov.medforum.model.api.dao.UsersProxy;
import net.styleru.i_komarov.medforum.model.layer.RetrofitFactory;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerIdentityEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerIdentityToken;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerNonceEntity;
import net.styleru.i_komarov.medforum.model.layer.dto.LayerSessionToken;
import net.styleru.i_komarov.medforum.model.layer.repository.AuthenticationRepository;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

import static net.styleru.i_komarov.medforum.model.common.BaseUrls.BASE_URL_LAYER;

/**
 * Created by i_komarov on 21.01.17.
 */

public class AuthenticationProxy {

    private static AuthenticationProxy instance;

    private AuthenticationRepository repository;

    private AuthenticationProxy() {
        repository = RetrofitFactory.build(BASE_URL_LAYER).create(AuthenticationRepository.class);
    }

    public static AuthenticationProxy getInstance() {
        AuthenticationProxy localInstance = instance;
        if(localInstance == null) {
            synchronized (AuthenticationProxy.class) {
                localInstance = instance;
                if(localInstance == null) {
                    localInstance = instance = new AuthenticationProxy();
                }
            }
        }

        return localInstance;
    }

    public Observable<Response<LayerSessionToken>> authenticate(String userID) {
        return getNonce().subscribeOn(Schedulers.io())
                .flatMap(new Function<Response<LayerNonceEntity>, ObservableSource<LayerIdentityEntity>>() {
                    @Override
                    public ObservableSource<LayerIdentityEntity> apply(Response<LayerNonceEntity> response) throws Exception {
                        return getIdentityToken(userID, response.body().getNonce())
                                .subscribeOn(Schedulers.io());
                    }
                })
                .flatMap(new Function<LayerIdentityEntity, ObservableSource<Response<LayerSessionToken>>>() {
                    @Override
                    public ObservableSource<Response<LayerSessionToken>> apply(LayerIdentityEntity layerIdentityEntity) throws Exception {
                        return getSessionToken(layerIdentityEntity)
                                .subscribeOn(Schedulers.io());
                    }
                });
    }

    private Observable<Response<LayerNonceEntity>> getNonce() {
        return repository.getNonce();
    }

    private Observable<LayerIdentityEntity> getIdentityToken(String userID, String nonce) {
        return UsersProxy.getInstance().getToken(userID, nonce)
                .map(new Function<Response<LayerIdentityToken>, LayerIdentityEntity>() {
                    @Override
                    public LayerIdentityEntity apply(Response<LayerIdentityToken> response) throws Exception {
                        return new LayerIdentityEntity(response.body().getToken(), MedForumApp.getLayerID());
                    }
                });
    }

    private Observable<Response<LayerSessionToken>> getSessionToken(LayerIdentityEntity identity) {
        return repository.getToken(identity);
    }
}
