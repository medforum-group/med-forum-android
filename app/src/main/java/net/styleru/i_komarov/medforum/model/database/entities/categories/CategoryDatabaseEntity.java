package net.styleru.i_komarov.medforum.model.database.entities.categories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.i_komarov.medforum.model.database.mapping.categories.CategoriesTableConfig;

/**
 * Created by i_komarov on 14.01.17.
 */

@StorIOSQLiteType(table = CategoriesTableConfig.TABLE)
public class CategoryDatabaseEntity {

    @Nullable
    @StorIOSQLiteColumn(name = CategoriesTableConfig.COLUMN_ID, key = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = CategoriesTableConfig.COLUMN_SERVER_ID)
    String serverID;

    @NonNull
    @StorIOSQLiteColumn(name = CategoriesTableConfig.COLUMN_TITLE)
    String title;

    @NonNull
    @StorIOSQLiteColumn(name = CategoriesTableConfig.COLUMN_LAST_UPDATE)
    String lastUpdate;

    @NonNull
    @StorIOSQLiteColumn(name = CategoriesTableConfig.COLUMN_QUESTION_COUNT)
    Integer questionCount;

    CategoryDatabaseEntity() {

    }

    @NonNull
    public static CategoryDatabaseEntity newInstance(String serverID, String title, String lastUpdate, Integer questionCount) {
        CategoryDatabaseEntity entity = new CategoryDatabaseEntity();
        entity.serverID = serverID;
        entity.title = title;
        entity.lastUpdate = lastUpdate;
        entity.questionCount = questionCount;
        return entity;
    }

    @NonNull
    public static CategoryDatabaseEntity newInstance(Long id, String serverID, String title, String lastUpdate, Integer questionCount) {
        CategoryDatabaseEntity entity = new CategoryDatabaseEntity();
        entity.id = id;
        entity.serverID = serverID;
        entity.title = title;
        entity.lastUpdate = lastUpdate;
        entity.questionCount = questionCount;
        return entity;
    }

    @Nullable
    public Long getId() {
        return id;
    }

    public void setId(@Nullable Long id) {
        this.id = id;
    }

    @NonNull
    public String getServerID() {
        return serverID;
    }

    public void setServerID(@NonNull String serverID) {
        this.serverID = serverID;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    @NonNull
    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(@NonNull String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @NonNull
    public Integer getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(@NonNull Integer questionCount) {
        this.questionCount = questionCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CategoryDatabaseEntity that = (CategoryDatabaseEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!serverID.equals(that.serverID)) return false;
        if (!title.equals(that.title)) return false;
        if (!lastUpdate.equals(that.lastUpdate)) return false;
        return questionCount.equals(that.questionCount);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + serverID.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + lastUpdate.hashCode();
        result = 31 * result + questionCount.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CategoryDatabaseEntity{" +
                "id=" + id +
                ", serverID='" + serverID + '\'' +
                ", title='" + title + '\'' +
                ", lastUpdate='" + lastUpdate + '\'' +
                ", questionCount=" + questionCount +
                '}';
    }
}
