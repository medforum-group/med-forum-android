package net.styleru.i_komarov.medforum.view.authorization.entry;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.12.16.
 */

public interface IEntryView {

    public Observable<String> getPhoneVerificationButtonClickChannel();

    public void onPhoneVerified(String phone);

    public void onPhoneVerificationFailed();
}
