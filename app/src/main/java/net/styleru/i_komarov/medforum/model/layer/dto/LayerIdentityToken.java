package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.i_komarov.medforum.model.common.Fields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerIdentityToken {

    @SerializedName(FIELD_TOKEN)
    private String token;
    @SerializedName(FIELD_LIST)
    private List<String> userIDs;

    public LayerIdentityToken(String token, List<String> userIDs) {
        this.token = token;
        this.userIDs = userIDs;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getUserIDs() {
        return userIDs;
    }

    public void setUserIDs(List<String> userIDs) {
        this.userIDs = userIDs;
    }
}
