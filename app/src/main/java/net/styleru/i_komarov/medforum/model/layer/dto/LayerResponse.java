package net.styleru.i_komarov.medforum.model.layer.dto;

import com.google.gson.annotations.SerializedName;

import static net.styleru.i_komarov.medforum.model.common.LayerFields.*;

/**
 * Created by i_komarov on 21.01.17.
 */

public class LayerResponse<T> {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_CODE)
    private String code;
    @SerializedName(FIELD_MESSAGE)
    private String message;
    @SerializedName(FIELD_URL)
    private String url;
    @SerializedName(FIELD_DATA)
    private T data;

    public LayerResponse(String id, String code, String message, String url, T data) {
        this.id = id;
        this.code = code;
        this.message = message;
        this.url = url;
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "LayerResponse{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", url='" + url + '\'' +
                ", data=" + data +
                '}';
    }
}
