package net.styleru.i_komarov.rxjava2_binding.toolbar.on_subscribe;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ToolbarMenuItemClickOnSubscribe implements ObservableOnSubscribe<MenuItem> {

    private final Toolbar view;

    public ToolbarMenuItemClickOnSubscribe(Toolbar view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<MenuItem> emitter) throws Exception {

        verifyMainThread();

        Toolbar.OnMenuItemClickListener listener = new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(item);
                    return true;
                } else {
                    return false;
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnMenuItemClickListener(null);
                    }
                }
        );

        view.setOnMenuItemClickListener(listener);
    }
}
