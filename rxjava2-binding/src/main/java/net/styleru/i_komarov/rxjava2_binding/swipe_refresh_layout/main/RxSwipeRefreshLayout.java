package net.styleru.i_komarov.rxjava2_binding.swipe_refresh_layout.main;

import android.support.v4.widget.SwipeRefreshLayout;

import net.styleru.i_komarov.rxjava2_binding.swipe_refresh_layout.on_subscribe.RxSwipeRefreshLayoutRefreshEventsOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 23.01.17.
 */

public class RxSwipeRefreshLayout {

    public static Observable<Integer> refreshes(SwipeRefreshLayout view) {
        return Observable.create(new RxSwipeRefreshLayoutRefreshEventsOnSubscribe(view));
    }
}
