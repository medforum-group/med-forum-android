package net.styleru.i_komarov.rxjava2_binding.toolbar.on_subscribe;

import android.support.v7.widget.Toolbar;
import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ToolbarNavigationClickOnSubscribe implements ObservableOnSubscribe<Integer> {

    private final Toolbar view;

    public ToolbarNavigationClickOnSubscribe(Toolbar view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<Integer> emitter) throws Exception {

        verifyMainThread();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(0);
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setNavigationOnClickListener(null);
                    }
                }
        );

        view.setNavigationOnClickListener(listener);
    }
}
