package net.styleru.i_komarov.rxjava2_binding.view.on_subscribe;

import android.view.DragEvent;
import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ViewDragOnSubscribe implements ObservableOnSubscribe<DragEvent> {

    private final View view;

    public ViewDragOnSubscribe(View view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<DragEvent> emitter) throws Exception {

        verifyMainThread();

        View.OnDragListener listener = new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent event) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(event);
                    return true;
                } else {
                    return false;
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnDragListener(null);
                    }
                }
        );

        view.setOnDragListener(listener);
    }
}
