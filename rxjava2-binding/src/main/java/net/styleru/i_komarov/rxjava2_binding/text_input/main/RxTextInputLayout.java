package net.styleru.i_komarov.rxjava2_binding.text_input.main;

import android.support.design.widget.TextInputLayout;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxTextInputLayout {

    public static Consumer<CharSequence> error(final TextInputLayout view) {
        return new Consumer<CharSequence>() {
            @Override
            public void accept(CharSequence error) throws Exception {
                view.setError(error);
            }
        };
    }

    public static Consumer<Integer> errorRes(final TextInputLayout view) {
        return new Consumer<Integer>() {
            @Override
            public void accept(Integer errorRes) throws Exception {
                view.setError(view.getContext().getResources().getText(errorRes));
            }
        };
    }

    public static Consumer<CharSequence> hint(final TextInputLayout view) {
        return new Consumer<CharSequence>() {
            @Override
            public void accept(CharSequence hint) throws Exception {
                view.setHint(hint);
            }
        };
    }

    public static Consumer<Integer> hintRes(final TextInputLayout view) {
        return new Consumer<Integer>() {
            @Override
            public void accept(Integer hintRes) throws Exception {
                view.setHint(view.getContext().getText(hintRes));
            }
        };
    }
}
