package net.styleru.i_komarov.rxjava2_binding.view.on_subscribe;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.View;

import net.styleru.i_komarov.rxjava2_binding.view.event.ScrollChangeEvent;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ViewScrollChangeOnSubscribe implements ObservableOnSubscribe<ScrollChangeEvent> {

    private final View view;

    public ViewScrollChangeOnSubscribe(View view) {
        this.view = view;
    }

    @Override
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void subscribe(final ObservableEmitter<ScrollChangeEvent> emitter) throws Exception {

        verifyMainThread();

        View.OnScrollChangeListener listener = new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(new ScrollChangeEvent(scrollX, scrollY, oldScrollX, oldScrollY));
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnScrollChangeListener(null);
                    }
                }
        );

        view.setOnScrollChangeListener(listener);
    }
}
