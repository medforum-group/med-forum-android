package net.styleru.i_komarov.rxjava2_binding.text_input.main;

import android.widget.EditText;

import net.styleru.i_komarov.rxjava2_binding.text_input.on_subscribe.EditTextTextChangeOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxEditText {

    public static <T extends EditText> Observable<CharSequence> textChanges(T view) {
        return Observable.create(new EditTextTextChangeOnSubscribe(view));
    }
}
