package net.styleru.i_komarov.rxjava2_binding.toolbar.main;

import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import net.styleru.i_komarov.rxjava2_binding.toolbar.on_subscribe.ToolbarMenuItemClickOnSubscribe;
import net.styleru.i_komarov.rxjava2_binding.toolbar.on_subscribe.ToolbarNavigationClickOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxToolbar {

    public static Observable<MenuItem> menuItemClicks(Toolbar view) {
        return Observable.create(new ToolbarMenuItemClickOnSubscribe(view));
    }

    public static Observable<Integer> navigationClicks(Toolbar view) {
        return Observable.create(new ToolbarNavigationClickOnSubscribe(view));
    }
}
