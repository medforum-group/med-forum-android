package net.styleru.i_komarov.rxjava2_binding.view.on_subscribe;

import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ViewClickOnSubscribe implements ObservableOnSubscribe<Integer> {

    private final View view;

    public ViewClickOnSubscribe(View view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<Integer> emitter) throws Exception {

        verifyMainThread();

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(0);
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnClickListener(null);
                    }
                }
        );

        view.setOnClickListener(listener);
    }
}
