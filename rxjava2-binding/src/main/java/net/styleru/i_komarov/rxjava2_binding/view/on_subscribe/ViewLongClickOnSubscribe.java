package net.styleru.i_komarov.rxjava2_binding.view.on_subscribe;

import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ViewLongClickOnSubscribe implements ObservableOnSubscribe<Integer> {

    private final View view;

    public ViewLongClickOnSubscribe(View view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<Integer> emitter) throws Exception {

        verifyMainThread();

        View.OnLongClickListener listener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(0);
                    return true;
                } else {
                    return false;
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnLongClickListener(null);
                    }
                }
        );

        view.setOnLongClickListener(listener);
    }
}
