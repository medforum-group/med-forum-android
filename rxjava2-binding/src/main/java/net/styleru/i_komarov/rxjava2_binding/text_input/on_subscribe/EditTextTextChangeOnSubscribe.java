package net.styleru.i_komarov.rxjava2_binding.text_input.on_subscribe;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class EditTextTextChangeOnSubscribe implements ObservableOnSubscribe<CharSequence> {

    private final EditText view;

    public EditTextTextChangeOnSubscribe(EditText view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<CharSequence> emitter) throws Exception {

        verifyMainThread();

        final TextWatcher listener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(s);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //ignore
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.removeTextChangedListener(listener);
                    }
                }
        );

        view.addTextChangedListener(listener);
    }
}
