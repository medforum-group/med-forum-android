package net.styleru.i_komarov.rxjava2_binding.navigation_view.on_subscribe;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.util.Log;
import android.view.MenuItem;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class NavigationViewItemSelectionOnSubscribe implements ObservableOnSubscribe<MenuItem> {

    private final NavigationView view;

    public NavigationViewItemSelectionOnSubscribe(NavigationView view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<MenuItem> emitter) throws Exception {

        verifyMainThread();

        NavigationView.OnNavigationItemSelectedListener listener = new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(item);
                    return true;
                } else {
                    return false;
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setNavigationItemSelectedListener(null);
                    }
                }
        );

        view.setNavigationItemSelectedListener(listener);
    }
}
