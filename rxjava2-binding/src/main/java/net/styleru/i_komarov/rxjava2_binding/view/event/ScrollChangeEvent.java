package net.styleru.i_komarov.rxjava2_binding.view.event;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ScrollChangeEvent {

    private int scrollX;
    private int scrollY;
    private int oldScrollX;
    private int oldScrollY;

    public ScrollChangeEvent(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        this.scrollX = scrollX;
        this.scrollY = scrollY;
        this.oldScrollX = oldScrollX;
        this.oldScrollY = oldScrollY;
    }

    public int getScrollX() {
        return scrollX;
    }

    public void setScrollX(int scrollX) {
        this.scrollX = scrollX;
    }

    public int getScrollY() {
        return scrollY;
    }

    public void setScrollY(int scrollY) {
        this.scrollY = scrollY;
    }

    public int getOldScrollX() {
        return oldScrollX;
    }

    public void setOldScrollX(int oldScrollX) {
        this.oldScrollX = oldScrollX;
    }

    public int getOldScrollY() {
        return oldScrollY;
    }

    public void setOldScrollY(int oldScrollY) {
        this.oldScrollY = oldScrollY;
    }
}
