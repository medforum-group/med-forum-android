package net.styleru.i_komarov.rxjava2_binding.view.on_subscribe;

import android.view.MotionEvent;
import android.view.View;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class ViewTouchOnSubscribe implements ObservableOnSubscribe<MotionEvent> {

    private final View view;

    public ViewTouchOnSubscribe(View view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<MotionEvent> emitter) throws Exception {

        verifyMainThread();

        View.OnTouchListener listener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(event);
                    return true;
                } else {
                    return false;
                }
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnTouchListener(null);
                    }
                }
        );

        view.setOnTouchListener(listener);
    }
}
