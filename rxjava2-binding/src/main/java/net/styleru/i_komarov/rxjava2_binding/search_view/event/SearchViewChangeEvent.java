package net.styleru.i_komarov.rxjava2_binding.search_view.event;

/**
 * Created by i_komarov on 25.12.16.
 */

public class SearchViewChangeEvent {

    private Type type;
    private String query;

    public SearchViewChangeEvent(Type type, String query) {
        this.type = type;
        this.query = query;
    }

    public Type getType() {
        return type;
    }

    public String getQuery() {
        return query;
    }

    public enum Type {
        CHANGE,
        SUBMIT
    }
}
