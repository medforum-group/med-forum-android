package net.styleru.i_komarov.rxjava2_binding.search_view.main;

import android.support.v7.widget.SearchView;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;
import net.styleru.i_komarov.rxjava2_binding.search_view.on_subscribe.SearchViewQueryChangeOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxSearchView {

    public static Observable<SearchViewChangeEvent> queryChanges(SearchView view) {
        return Observable.create(new SearchViewQueryChangeOnSubscribe(view));
    }
}
