package net.styleru.i_komarov.rxjava2_binding.navigation_view.main;

import android.support.design.widget.NavigationView;
import android.view.MenuItem;

import net.styleru.i_komarov.rxjava2_binding.navigation_view.on_subscribe.NavigationViewItemSelectionOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxNavigationView {

    public static Observable<MenuItem> itemSelections(NavigationView view) {
        return Observable.create(new NavigationViewItemSelectionOnSubscribe(view));
    }
}
