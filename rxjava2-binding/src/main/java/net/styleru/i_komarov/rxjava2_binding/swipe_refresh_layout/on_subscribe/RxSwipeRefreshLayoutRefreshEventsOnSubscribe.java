package net.styleru.i_komarov.rxjava2_binding.swipe_refresh_layout.on_subscribe;

import android.support.v4.widget.SwipeRefreshLayout;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 23.01.17.
 */

public class RxSwipeRefreshLayoutRefreshEventsOnSubscribe implements ObservableOnSubscribe<Integer> {

    private final SwipeRefreshLayout swipeRefreshLayout;

    public RxSwipeRefreshLayoutRefreshEventsOnSubscribe(SwipeRefreshLayout swipeRefreshLayout) {
        this.swipeRefreshLayout = swipeRefreshLayout;
    }

    @Override
    public void subscribe(final ObservableEmitter<Integer> emitter) throws Exception {
        verifyMainThread();

        SwipeRefreshLayout.OnRefreshListener listener = new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!emitter.isDisposed()) {
                    emitter.onNext(0);
                }
            }
        };

        emitter.setDisposable(new MainThreadDisposable() {
            @Override
            protected void onDispose() {
                swipeRefreshLayout.setOnRefreshListener(null);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(listener);
    }
}
