package net.styleru.i_komarov.rxjava2_binding.view.main;

import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;

import net.styleru.i_komarov.rxjava2_binding.view.event.ScrollChangeEvent;
import net.styleru.i_komarov.rxjava2_binding.view.on_subscribe.ViewClickOnSubscribe;
import net.styleru.i_komarov.rxjava2_binding.view.on_subscribe.ViewDragOnSubscribe;
import net.styleru.i_komarov.rxjava2_binding.view.on_subscribe.ViewLongClickOnSubscribe;
import net.styleru.i_komarov.rxjava2_binding.view.on_subscribe.ViewScrollChangeOnSubscribe;
import net.styleru.i_komarov.rxjava2_binding.view.on_subscribe.ViewTouchOnSubscribe;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 07.12.16.
 */

public class RxView {

    public static Observable<Integer> clicks(View view) {
        return Observable.create(new ViewClickOnSubscribe(view));
    }

    public static Observable<Integer> longClicks(View view) {
        return Observable.create(new ViewLongClickOnSubscribe(view));
    }

    public static Observable<DragEvent> drags(View view) {
        return Observable.create(new ViewDragOnSubscribe(view));
    }

    public static Observable<MotionEvent> touches(View view) {
        return Observable.create(new ViewTouchOnSubscribe(view));
    }

    public static Observable<ScrollChangeEvent> scrollChanges(View view) {
        return Observable.create(new ViewScrollChangeOnSubscribe(view));
    }
}
