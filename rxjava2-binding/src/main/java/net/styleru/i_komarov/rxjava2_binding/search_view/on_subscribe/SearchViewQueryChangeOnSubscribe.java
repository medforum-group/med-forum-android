package net.styleru.i_komarov.rxjava2_binding.search_view.on_subscribe;

import android.support.v7.widget.SearchView;

import net.styleru.i_komarov.rxjava2_binding.search_view.event.SearchViewChangeEvent;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.MainThreadDisposable;

import static io.reactivex.android.MainThreadDisposable.verifyMainThread;

/**
 * Created by i_komarov on 07.12.16.
 */

public class SearchViewQueryChangeOnSubscribe implements ObservableOnSubscribe<SearchViewChangeEvent> {

    private final SearchView view;

    public SearchViewQueryChangeOnSubscribe(SearchView view) {
        this.view = view;
    }

    @Override
    public void subscribe(final ObservableEmitter<SearchViewChangeEvent> emitter) throws Exception {

        verifyMainThread();

        SearchView.OnQueryTextListener listener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(new SearchViewChangeEvent(SearchViewChangeEvent.Type.SUBMIT, query));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if(!emitter.isDisposed()) {
                    emitter.onNext(new SearchViewChangeEvent(SearchViewChangeEvent.Type.CHANGE, newText));
                }
                return false;
            }
        };

        emitter.setDisposable(
                new MainThreadDisposable() {
                    @Override
                    protected void onDispose() {
                        view.setOnQueryTextListener(null);
                    }
                }
        );

        view.setOnQueryTextListener(listener);
    }
}
